/* Delets the given user from the data base.
 * Gets in the email of the user you want to delete.
 */
function deleteUser(user) {
	var r = confirm("Are you sure you  want to delete\n" + user + "?");
	if (r == true) {
		$.ajax({
			type : "POST",
			url : location.protocol + '//' + location.host + "/user/" + user
					+ "/delete",
			dataType : "json",
			success : function(data) {
				window.location.href = "/administration";
			},
			error : function(request, status, error) {
				alert("ERROR");
			}
		});
	}
}

/*
 * Updates the role of the user. Gets in the value (new role) and setting (what
 * to update. Ignored n this method).
 */
function updateRole(value, settings) {
	var origvalue = this.revert;
	var success = false;

	if (origvalue == value) {
		return origvalue;
	}

	var user = this.parentNode.parentNode.id;
	$.ajax({
		async : false,
		type : "POST",
		url : location.protocol + '//' + location.host + "/user/" + user
				+ "/update",
		dataType : "json",
		data : {
			"field" : "role",
			"value" : value
		},
		error : function(request, status, error) {
			value = origvalue;
			alert("ERROR");
		}
	});
	return value;
}

/*
 * Updates the user. Gets in the value (new value) and setting (which field to
 * change). Changes the user in the database and on the screen.
 */
function updateUser(value, settings) {

	var origvalue = this.revert;
	var success = false;

	if (origvalue == value) {
		return origvalue;
	}
	var field = this.id.substring(this.id.indexOf(" ") + 1, this.id.length);
	var user = this.parentNode.parentNode.id;
	$.ajax({
		async : false,
		type : "POST",
		url : location.protocol + '//' + location.host + "/user/" + user
				+ "/update",
		dataType : "json",
		data : {
			"field" : field,
			"value" : value
		},
		error : function(request, status, error) {
			value = origvalue;
			alert("ERROR");
		}
	});
	return value;
}
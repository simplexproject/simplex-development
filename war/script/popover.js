var keepPopover;
function initializePopovers() {
	delay = function() {
		keepPopover = setTimeout(function() {
			$('.popover').hide();
		}, 400);
	};
	$('a[rel="popover"]').popover({
		trigger : 'manual'
	}).mouseenter(function(e) {
		var $popover = $('.popover');
		(keepPopover) && clearTimeout(keepPopover);
		($popover.length) && $popover.remove();
		$(this).popover('show');
	}).mouseleave(function(e) {
		delay();
		$('.popover').mouseenter(function(e) {
			clearTimeout(keepPopover);
		}).mouseleave(function(e) {
			delay();
		});
	});
}
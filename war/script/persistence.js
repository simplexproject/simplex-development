function handlePersist(divId, lessonID) {
	var inputs = document.getElementById(divId).getElementsByTagName("input");
	var textArea = document.getElementById(divId).getElementsByTagName(
			"textarea");

	var length = inputs.length + textArea.length;
	var properties = new Array(length);
	var values = new Array(length);

	var i = 0;
	for (; i < inputs.length; i++) {
		properties[i] = inputs[i].id;
		values[i] = inputs[i].value;
	}

	for (j = 0; j < textArea.length; j++) {
		properties[i] = textArea[j].id;
		values[i] = textArea[j].value;
		i++;
	}

	$.ajax({
		type : "POST",
		url : "/lesson/add/1/"+lessonID+"/"+properties+"/"+values,
		success : function(data) {
			for (k = 1; k < inputs.length; k++) {
				inputs[k].value = "";
			}
			for (k = 0; k < textArea.length; k++) {
				textArea[k].value = "";
			}
			// alert("done");
			$('#existingItems').load('addLessonContent.jsp').fadeIn("slow");
		},
		//data : "properties=" + properties + "&values=" + values
	});
}

function hideMe(ele, text, linkId) {
	ele.style.display = "none";
	text.innerHTML = linkId;
}

function toggle(linkId, displayDiv) {
	var ele = document.getElementById(displayDiv);
	var text = document.getElementById(linkId);
	if (ele.style.display == "block") {
		hideMe(ele, text, linkId);
	} else {
		var divs = document.getElementsByTagName("div");
		for (i = 0; i < divs.length; i++) {
			if (divs[i].style.display == "block") {
				return;
			}
		}
		ele.style.display = "block";
		text.innerHTML = "hide";
	}
}

function subElem(linkId, displayDiv, lessonID) {
	handlePersist(displayDiv);
	var ele = document.getElementById(displayDiv);
	var text = document.getElementById(linkId);
	hideMe(ele, text, linkId, lessonID);

}

function subPartOne() {
	var value = document.getElementById("name").value+","+document.getElementById("description").value;
	$.ajax({
		type : "POST",
		url : "/lesson/add/0/null/name,description/"+value,
		success : function(data) {
			$('#content').load('createLessonAddElements.jsp').fadeIn("slow");
		}
//		data : "name=" + document.getElementById("name").value + "&description="
//				+ document.getElementById("description").value
	});
}
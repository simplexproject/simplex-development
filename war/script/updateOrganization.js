/* Delets the given organization from the data base.
 * Gets in the id of the organization you want to delete.
 */
function deleteOrg(orgID) {
	var r = confirm("Are you sure you  want to delete\n" + orgID + "?");
	if (r == true) {
		$.ajax({
			type : "POST",
			url : location.protocol + '//' + location.host+"/org/" + orgID + "/delete",
			dataType : "json",
			success : function() {
				window.location.href = "/administration";
			},
			error : function(request, status, error) {
				alert("ERROR");
			}
		});
	}
}

/*
 * Updates the organization. Gets in the value (new value) and setting (which field to
 * change). Changes the user in the database and on the screen.
 */
function updateOrganization(value, settings) {

	var origvalue = this.revert;
	var success = false;

	if (origvalue == value) {
		return origvalue;
	}
	var field = this.id.substring(this.id.indexOf(" ") + 1, this.id.length);
	var org = this.parentNode.parentNode.id;
	$.ajax({
		async : false,
		type : "POST",
		url : location.protocol + '//' + location.host+"/org/" + org + "/update",
		dataType : "json",
		data : {
			"field" : field,
			"value" : value
		},
		error : function(request, status, error) {
			value = origvalue;
			alert("ERROR");
		}
	});
	return value;
}
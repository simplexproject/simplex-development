/*
 * This file controls notifications. It is imported in header and then can be used anywhere.
 * If you want to show a notification, call show(Success/Fail/Waiting)Notification with the text.
 */

function showSuccessNotification(str){
	$("#notification-area-lol").attr("class","alert alert-success fade in");
	setUpNotification(str);
}
function showFailNotification(str){
	$("#notification-area-lol").attr("class","alert alert-danger fade in");
	setUpNotification(str);
}
function showWaitingNotification(str){
	$("#notification-area-lol").attr("class","alert alert-info fade in");
	setUpNotification(str);
}

function fadeOutNotification(){
	$("#notification-area-lol").fadeOut('slow');
}

function fadeInNotification(){
	$("#notification-area-lol").fadeIn('normal');
}

function setUpNotification(str){
	$("#notification-area-lol").attr("style","text-align:center;");
	$("#notification-area-lol").text(str);
	$("#notification-area-lol").hide();
	fadeInNotification();
	setTimeout(fadeOutNotification,5000);
}
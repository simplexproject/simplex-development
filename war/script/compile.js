//Old file - from the daus of MEETag

/* attach a submit handler to the form */
$("#editorForm").submit(
		function(event) {

			/* stop form from submitting normally */
			event.preventDefault();

			/* get some values from elements on the page: */
			var $form = $(this);
			var $editor = $form.find('textarea[name="editor"]').val();
			var $mode = $form.find('select[name="mode"]').val();
			var $theme = $form.find('select[name="theme"]').val();
			var $gutter = $form.find('select[name="gutter"]').val();
			var $fontSize = $form.find('select[name="fontSize"]').val();
			var $softWarp = $form.find('select[name="softWrap"]').val();
			var $showPrintMargin = $form.find('select[name="showPrintMargin"]')
					.val();
			var $useSoftTabs = $form.find('select[name="useSoftTabs"]').val();
			var $showInvisible = $form.find('select[name="showInvisible"]')
					.val();
			var $url = $form.attr('action');

			/* Send the data using post and put the results in a div */
			$.post($url, {
				editor : $editor,
				mode : $mode,
				theme : $theme,
				gutter : $gutter
			}, function(data) {
				$("#result").empty().append(
						"<p>" + $editor + $mode + $theme + $gutter + "</p>");
			});
		});
function injectLesson( callback) {
	var baseUrl = "/ace/build/textarea/src/";

	var load = window.__ace_loader__ = function(path, module, callback) {
		var head = document.getElementsByTagName('head')[0];
		var s = document.createElement('script');

		s.src = baseUrl + path;
		head.appendChild(s);

		s.onload = function() {
			window.__ace_shadowed__.require([ module ], callback);
		};
	};

	load('ace-bookmarklet.js', "ace/ext/textarea", function() {
		var ace = window.__ace_shadowed__;
		ace.options.mode = "java";
		var Event = ace.require("ace/lib/event");
		var areas = document.querySelectorAll("textarea.aceEditor");
		for ( var i = 0; i < areas.length; i++) {
			Event.addListener(areas[i], "click", function(e) {
				if (e.detail == 3) {
					ace.transformTextarea(e.target, load);
				}
			});
		}
		callback();
	});
}
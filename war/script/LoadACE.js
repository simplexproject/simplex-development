document.write('<script type="text/javascript" src="/ace/build/src/ace.js"></script>');
//Themes
document.write('<script type="text/javascript" src="/ace/build/src/theme-chrome.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-clouds.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-clouds_midnight.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-cobalt.js" type="text/javascript"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-crimson_editor.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-dawn.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-dreamweaver.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-eclipse.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-idle_fingers.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-kr_theme.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-merbivore.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-merbivore_soft.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-mono_industrial.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-monokai.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-pastel_on_dark.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-solarized_dark.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-solarized_light.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-textmate.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-twilight.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-tomorrow.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-tomorrow_night.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-tomorrow_night_blue.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-tomorrow_night_bright.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-tomorrow_night_eighties.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/theme-vibrant_ink.js"></script>');
//Languages 
document.write('<script type="text/javascript" src="/ace/build/src/mode-c_cpp.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-clojure.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-coffee.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-coldfusion.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-csharp.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-css.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-groovy.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-haxe.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-html.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-java.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-javascript.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-json.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-latex.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-lua.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-markdown.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-ocaml.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-perl.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-pgsql.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-php.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-powershell.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-python.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-ruby.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-scad.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-scala.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-scss.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-sql.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-svg.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-textile.js"></script>');
document.write('<script type="text/javascript" src="/ace/build/src/mode-xml.js"></script>');

function afterSending() {
	$("#contactForm")[0].innerHTML = "<h3>Thank you!</h3><br/><h4>Your message has been sent.</h4>";
}

function sendContactUs() {

	var fullName = $("#contactName")[0].value;
	var email = $("#contactEmail")[0].value;
	var query = $("#contactQuery")[0].value;
	var message = $("#contactMessage")[0].value;
	$.ajax({
		type : "POST",
		url : location.protocol + '//' + location.host + "/contact/send",
		dataType : "json",
		data : {
			"fullName" : fullName,
			"email" : email,
			"query" : query,
			"message" : message
		},
		error : afterSending
	}).done(afterSending);
}
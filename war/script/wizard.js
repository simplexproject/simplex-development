$(document).ready(function() {
	$("#myModal").modal({
		show : false
	});
});

$(document).ready(getCourses);

function getCourses() {
	$
			.ajax({
				type : "GET",
				url : "/creator/getCourses",
				success : function(data) {
					if ($.parseJSON(data) == "") {
						$("#exCoursesPanel").hide();
						$("#noCourses").show();
					} else {
						$("#noCourses").hide();
						$("#exCoursesPanel").show();
						var cArray = $.parseJSON(data);
						var pCourses;
						var $course;
						var cURL;
						var cNameTrimmed;
						document.getElementById("existingCourses").innerHTML = "";
						for (i = cArray.length - 1; i >= 0; i--) {
							pCourses = document
									.getElementById("existingCourses").innerHTML;
							$course = cArray[i];
							cURL = $course.id;
							if ($course.name.length > 14) {
								cNameTrimmed = $course.name.substr(0, 14)
										+ "...";
							} else {
								cNameTrimmed = $course.name;
							}
							document.getElementById("existingCourses").innerHTML = pCourses
									+ '<li class="span2"><a rel="popover" data-original-title="'
									+ $course.name
									+ '" data-content="'
									+ $course.description
									+ '" data-placement="bottom" href="/course/'
									+ cURL
									+ '" target="_blank"><section class="courseView"><span class="cNameSpan">'
									+ cNameTrimmed
									+ '</span><img class="courseImg" src="'
									+ $course.avatar + '"/></section></a></li>';
						}
						initializePopovers();
					}
				},
				error : function() {
					alert("Cannot reach DB.");
				}
			});
}

$("#addButton")
		.hover(
				function() {
					var PcDesc = document.getElementById("cDesI").value;
					var Pavatar;
					var cNameFull = document.getElementById("cNaI").value;
					var PcNameTrimmed;
					var PtestImg = new Image();
					var pass;
					PtestImg.src = document.getElementById("cAvI").value;
					if (cNameFull.length > 14) {
						PcNameTrimmed = cNameFull.substr(0, 14) + "...";
					} else {
						PcNameTrimmed = cNameFull;
					}
					if (document.getElementById("cAvI").value == ""
							|| (PtestImg.width < 100 || PtestImg.height < 100)) {
						Pavatar = "/images/logo.jpg";
					} else {
						if (PtestImg.width == PtestImg.height) {
							Pavatar = document.getElementById("cAvI").value;
						} else {
							Pavatar = "/images/logo.jpg";
						}
					}
					if (cNameFull == "" || PcDesc == "") {
						pass = 0;
					} else if (cNameFull.length > 40 || PcDesc.length > 200) {
						pass = 0;
					} else {
						pass = 1;
					}

					if (pass == 0) {
						$("#addButton")[0].setAttribute("data-content", "");
						$("#addButton")[0].setAttribute("data-dismiss", "");
					} else if (pass == 1) {
						$("#addButton")[0]
								.setAttribute(
										"data-content",
										'<ul class="thumbnails" id="popoverPreviewUl"><li class="span2"><a href="javascript:void(0);" target="_blank"><section id="PcoursePreviewer" class="courseView" style=""><span class="cNameSpan">'
												+ PcNameTrimmed
												+ '</span><img class="courseImg" src="'
												+ Pavatar
												+ '"/></section></a></li></ul>');
						$("#addButton")[0]
								.setAttribute("data-dismiss", "modal");
					}
				});

$("#addButton")
		.click(
				function(event) {
					event.preventDefault();
					var $form = $("#addCourseForm"), cName = $form.find(
							'input[name="cName"]').val(), cDesc = $form.find(
							'textarea[name="cDesc"]').val(), url = $form
							.attr('action'), cAvatar = $form.find(
							'input[name="cAvatar"]').val();
					var testImg = new Image();
					var cPass;

					testImg.src = cAvatar;

					if ($form.find('input[name="cAvatar"]').val() == ""
							|| (testImg.width < 100 || testImg.height < 100)) {
						cAvatar = "/images/logo.jpg";
					} else {
						if (testImg.width == testImg.height) {
							cAvatar = $form.find('input[name="cAvatar"]').val();
						} else {
							cAvatar = "/images/logo.jpg";
						}
					}

					if (cName == "" || cDesc == "") {
						if (cName == "") {
							alert("You cannot add a Course without a name.");
						} else {
							alert("Please give your Course a brief description.");
						}
						cPass = 0;
					} else if (cName.length > 40 || cDesc.length > 200) {
						if (cName.length > 40) {
							alert("The name is over 40 characters, please revise it and try again. Current count: "
									+ cName.length + " characters.");
						} else {
							alert("The description is over 200 characters, please revise it and try again. Current count: "
									+ cDesc.length + " characters.");
						}
						cPass = 0;
					} else {
						cPass = 1;
					}

					if (cPass == 1) {
						$
								.ajax({
									type : "POST",
									url : url,
									data : {
										"courseName" : cName,
										"courseDesc" : cDesc,
										"courseAvatar" : cAvatar,
									},
									success : function(data) {
										var responseObject = $.parseJSON(data);
										if (responseObject) {
											getCourses();
										} else {
											alert("Failed to add Course, please try again later.");
										}
									},
									error : function() {
										alert("An error has occured; Database unreachable, please check your internet connection.");
									}
								});
					} else {
						alert("Your Course was not added. Please follow instructions and try again.");
					}
				});

$("#cNaI")
		.focus(
				function() {
					document.getElementById("help-info").style.display = "inline-block";
					document.getElementById("help-info").innerHTML = "Please give your new Course an appropriate and informative name that suits its content.<br /><br />Note: Name is limited to 40 characters.";
				});

$("#cDesI")
		.focus(
				function() {
					document.getElementById("help-info").style.display = "inline-block";
					document.getElementById("help-info").innerHTML = "Please write an appropriate and informative description for your new Course that suits its content.<br /><br />Note: Description is limited to 200 characters.";
				});

$("#cAvI")
		.focus(
				function() {
					document.getElementById("help-info").style.display = "inline-block";
					document.getElementById("help-info").innerHTML = "You can provide a link here to an avatar image for your new Course. It will be set to the default avatar if left blank or if the provided link (if any) does not point to a valid image. <br /><br />Note: Only even images are counted as valid (1:1 width:height; width = height).";
				});
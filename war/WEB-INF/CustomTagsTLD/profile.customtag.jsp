<section class="container">
	<section id="highlights" style="width: 100%;">
		<table id="basicANDsocial">
			<tr>
				<td valign="top">
					<section id="basicView" class="well"
						style="background-color: white;">
						<table>
							<tr>
								<td valign="top">
									<section id="profile_pic">
										<img src="${User.avatar}" class="thumbnail" onclick="test()"
											style="max-width: 215px; max-height: 215px;" /> <br>
									</section>
								</td>
								<td valign="top">
									<h3 style="text-align: center;">${User.firstName}
										${User.lastName}</h3>
									<section id="tabs&content"
										style="margin-left: 20px; margin-top: 20px;">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#basic" data-toggle="tab"><i
													class="icon-user"></i> Basic Information</a></li>
											<li><a href="#statistics" data-toggle="tab"><i
													class="icon-tasks"></i> ${User.firstName}'s Statistics</a></li>
										</ul>
										<section class="tab-content span5">
											<section class="tab-pane fade in active" id="basic">
												<table class="table table-bordered table-striped">
													<tbody>
														<tr>
															<td>Name</td>
															<td><p class="click" id="first_name"
																	style="display: inline">${User.firstName}</p>
																<p class="click" style="display: inline">${User.lastName}</p></td>
														</tr>
														<tr>
															<td>Birthdate</td>
															<td><p class="click" id="birthdate"
																	style="display: inline">${User.birthDate}</p></td>
														</tr>
														<tr>
															<td>Gender</td>
															<td><p class="click" id="gender"
																	style="display: inline">${User.gender}</p></td>
														</tr>
													</tbody>
												</table>
											</section>
											<section class="tab-pane fade" id="statistics">
												<p class="click" style="display: inline">${User.firstName}'s
													statistics should load here.</p>
											</section>
										</section>
									</section>
								</td>
							</tr>
						</table>
					</section>
				</td>
				<td valign="top">
					<section id="social" class="well"
						style="background-color: white; margin-left: 10px;">
						<table style="text-align: center;">
							<tr>
								<td>
									<h3>${User.firstName}'s Social Links</h3>
								</td>
							</tr>
							<tr>
								<td style="padding-top: 20px; padding-bottom: 20px;"><img
									style="width: 150px;" src="images/facebook-logo-large.png"
									onclick="window.location=('https://www.facebook.com/dialog/oauth?client_id=323904147673897&redirect_uri=http://localhost:8888/login/facebook&scope=email,user_birthday&response_type=code');"></img></td>
							</tr>
							<tr>
								<td style="padding-bottom: 20px;"><img
									style="width: 150px;" src="images/google-logo-large.png"
									onclick="window.location=('https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=937715082662.apps.googleusercontent.com&redirect_uri=http://localhost:8888/login/google&scope=https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/plus.me');"></img></td>
							</tr>
							<tr>
								<td><img style="width: 150px;"
									src="images/twitter-logo-large.png"
									onclick="window.location=('https://api.twitter.com/oauth/request_token?oauth_callback=http://localhost:8888/login/twitter');"></img></td>
							</tr>
						</table>
					</section>
				</td>
			</tr>
		</table>
	</section>
	<section class="well" style="background-color: white;">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#userCourses" data-toggle="tab"><i
					class="icon-file"></i> ${User.firstName}'s Courses</a></li>
			<li><a href="#userLessons" data-toggle="tab"><i
					class="icon-file"></i> ${User.firstName}'s Lessons</a></li>
		</ul>
		<section class="tab-content">
			<section class="tab-pane fade in active" id="userCourses">
				<p style="display: inline">Etsy mixtape wayfarers, ethical wes
					anderson tofu before they sold out mcsweeney's organic lomo retro
					fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify
					pitchfork tattooed craft beer, iphone skateboard locavore carles
					etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony.
					Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi
					whatever gluten-free, carles pitchfork biodiesel fixie etsy retro
					mlkshk vice blog. Scenester cred you probably haven't heard of
					them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu
					synth chambray yr.</p>
			</section>
			<section id="userLessons" class="tab-pane fade">
				<p style="display: inline">Trust fund seitan letterpress, keytar
					raw denim keffiyeh etsy art party before they sold out master
					cleanse gluten-free squid scenester freegan cosby sweater. Fanny
					pack portland seitan DIY, art party locavore wolf cliche high life
					echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi
					before they sold out farm-to-table VHS viral locavore cosby
					sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh
					craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh
					echo park vegan.</p>
			</section>
		</section>
	</section>
</section>
<script type="text/javascript">
	$(document).ready(function() {
		var highlightsHeight = ($("#highlights").height() - 40);
		var containerWidth = $(".container").width();
		$("#basicANDsocial").css("width", containerWidth);
		$("#basicView").css("height", highlightsHeight);
		$("#social").css("height", highlightsHeight);
	});
</script>
<%@ page import="java.util.ArrayList"%>
<section>
	<section class="well">
		<h3><%=request.getAttribute("title")%></h3>
		<h6 style="margin-left: 20px;"><%=request.getAttribute("question")%></h6>
		<%
			ArrayList<String> choices = (ArrayList<String>) request
				.getAttribute("choices");
		%>
		<br />
		<section id="${id}" style="display: none;">
			<ul class="nav nav-list">
				<%
					for (String str : choices) {
				%>
				<li><a href="#"><%=str%> </a></li>
				<%
					}
				%>
			</ul>
		</section>
	</section>
</section>
<script type="text/javascript">
	function slideChoices(id) {
		$("#" + id).slideToggle("slow");
	}
</script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<link rel="stylesheet" type="text/css" href="css/dialog.css">
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<div class="dialog">

		<div class="dialog-content">

			<h2>
				Signup<sub>x</sub>
			</h2>
			<br />
			<h6>The journey begins here.</h6>


			<form style="position: center" action="/signup/regular" method="post">
				<input id="firstName" name="firstName" type="text" class="input0s"
					placeholder="First Name"> <br /> <input id="lastName"
					name="lastName" type="text" class="input0s" placeholder="Last Name">
				<br /> <input id="email" name="email" type="text" class="input0s"
					placeholder="Email"> <br /> <input id="password"
					name="password" type="password" class="input"
					placeholder="Password"><br /> <input
					id="passwordConfirmation" name="passwordConf" type="password"
					class="input" placeholder="Confirm Password"> <br /> <br />

				<div>
					<button type="submit" class="btn btn-primary">Sign up</button>
					<button type="reset" class="btn">Cancel</button>
				</div>
			</form>

		</div>

		<div class="dialog-footer">

			<div class="dialog-footer-left">
				<a href="/">Go home</a>
			</div>
			<div class="dialog-footer-right">
				<a href="/login"> Already using SIMPLE<sub>x</sub>?
				</a>
			</div>

		</div>

	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<script type="text/javascript">
		$('#betaNotification').hide();
	</script>
	<section class="row">

		<section style="margin-left: 300px;" class="span4">
			<img src="images/we-will-be-back-soon.jpg"></img>
		</section>
		<section style="margin-top: 50px;" class="span7">

			<h1 style="font: lighter;">
				:) We're busy updating the SIMPLE<sub>x</sub> platform for you and
				will be back soon.
			</h1>
		</section>

	</section>
	<%@ include file="footer.jsp"%>
</body>
</html>
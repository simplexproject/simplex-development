<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
<style>
@import "css/popover.css";

@import "css/about.css";
</style>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container"
		style="margin-top: 1.61803398875%; margin-bottom: 1%;">
		<section class="span7">
			<iframe width="560px" height="315px"
				src="http://www.youtube.com/embed/T2ID4uB3CTg?controls=1&theme=light&modestbranding=1&rel=0&showinfo=0"
				frameborder="0" style="box-shadow: -2px 2px 8px rgba(0, 0, 0, 0.3);"></iframe>
		</section>
		<section class="span4">
			<h2>Mission</h2>
			<p>
				We envision a world where any person from any background will be
				able to learn how to program in every state-of-the-art programming
				language available out there. <br /> Our mission is to create a
				platform that facilitates studying programming interactively, for
				beginners with no technical background as well as experienced
				programmers, while building and nurturing an active community that
				develops the content collaboratively.
			</p>
		</section>
	</section>
	<section class="container" id="infoPanel">
		<hr>
		<section class="row" style="margin-bottom: 20px;">
			<section class="span5 pull-left">
				<h2>About Us</h2>
				<p>
					We're 8 young entrepreneurs from Jerusalem area, coming from very
					diverse backgrounds. However, what brings us together is that we
					are all former students of <a href="http://meet.mit.edu/"
						target="_blank">MEET - Middle East Education through
						Technology</a>. Being students at MEET, we have all been motivated to
					dream and think out of the box - to find solutions to real life
					problems and help the global community.
				</p>
			</section>
			<section id="teamPanel" class="span6 pull-right">
				<h2>The Team</h2>
				<p>
					Want to know how a computer geek who's also a passionate
					entrepreneur looks like? <br /> Interested to see what we have
					done before in our lives? <br /> Are you wondering if we are real
					people? With real habits?
				</p>
				<br /> <a href="javascript:void(0);"><span
					class="label btn-large" data-toggle="collapse"
					data-target="#devPanel"><strong>MEET the SIMPLE<sub>x</sub>
							team!
					</strong></span></a> &nbsp;&nbsp;&nbsp; <a href="javascript:void(0);"><span
					class="label btn-large" data-toggle="collapse"
					data-target="#supPanel">Supporters</span></a>
			</section>
		</section>
		<section id="devPanel" class="collapse">
			<%@ include file="developers.jsp"%>
		</section>
		<section id="supPanel" class="collapse">
			<%@ include file="supporters.jsp"%>
			<section style="height: 240px; background-color: transparent;"></section>
		</section>
	</section>
	<!-- Including footer -->
	<%@ include file="footer.jsp"%>
	<!-- #End footer -->
</body>
<script type="text/javascript" src="/script/popover.js"></script>
<script type="text/javascript">
	//Setting the About navBar tab to class='active' to visually illuminate it to indicate it as the current active page
	$(document).ready($('#about').addClass('active'));
	//Hiding notification area
	$("#notification-area").hide();
	//Hiding headerPadding
	$("#headerPadding").hide();
	//Initializing popovers when document is ready
	$(document).ready(initializePopovers);
</script>
</html>
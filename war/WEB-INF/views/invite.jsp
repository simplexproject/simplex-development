<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
<link rel="stylesheet" type="text/css" href="css/dialog.css">
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<script type="text/javascript">
		$('#betaNotification').hide();
	</script>
	<section class="container" style="max-width: 967px;">
		<section class="span6">
			<h1 style="margin-left: 10px;">
				SIMPLE<sub>x</sub>
			</h1>
			<legend
				style="margin-left: 11px; font-size: 12px; margin-top: -15px;">Programming
				Made Simple</legend>
			<h4 style="margin-left: 11px;">We're in Early Access Beta</h4>
			<p style="margin-left: 11px;">
				Just leave us your name and e-mail address to request an invitation
				to try out SIMPLE<sub>x</sub> <br />Once we are ready we'll send
				you an invite, and you'll be able to test our platform before anyone
				else.<br /> We're counting on your feedback and participation to
				make SIMPLE<sub>x</sub> best for you and the whole SIMPLE<sub>x</sub>
				community.
			</p>
		</section>
		<section class="dialog pull-right" style="max-width: 300px; margin-left: -300px;">
			<section class="dialog-content">
				<legend>Sign Up Now!</legend>
				<form
					action="http://thesimplex.us5.list-manage.com/subscribe/post?u=6289324bec691e60e8cda0dfb&amp;id=cbce62e360"
					method="post" style="width: 750px;">
					<input type="text" class="input" placeholder="Name" name="NAME">
					<br /> <input name="EMAIL" type="text" class="input"
						placeholder="Email"> <br /> <br />
					<button type="submit" class="btn btn-primary">Request an
						invitation</button>
				</form>
			</section>
		</section>
	</section>
	<%@ include file="footer.jsp"%>
</body>
</html>
<%@page import="edu.mit.meet.meetag.entities.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>

<body>
<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container">

		<section class="hero-unit">
			<h1>Messages</h1>
			<br />

			<ul class="nav nav-tabs">
				<li><a href="#"><i class="icon-edit"></i> Compose</a></li>
				<li><a href="#"><i class="icon-inbox"></i> Inbox</a></li>
				<li><a href="#"><i class="icon-envelope"></i> Sent</a></li>
			</ul>

			<%
				//User user = (User) session.getAttribute("User");
				//MailBox userMailBox = user.getMailbox();
				//ArrayList<Message> inbox = userMailBox.getInbox();
			%>

			<table class="table table-striped">
				<thead>
					<tr>
						<th><span class="badge">#</span></th>
						<th>From</th>
						<th>Subject</th>
						<th>Message</th>
					</tr>
				</thead>
				<tbody>
					<%
						//for (Message msg : inbox) {
					%>
					<tr>
						<td><span class="badge"></span></td>
						<td>
							<%
								//msg.getSender();
							%>
						</td>
						<td>
							<%
							//	msg.getTitle();
							%>
						</td>
						<td>
							<%
						//		msg.getContent();
							%>
						</td>
					</tr>
					<%
					//	}
					%>

				</tbody>
			</table>
		</section>
	</section>
	<%@ include file="footer.jsp"%>
</body>
</html>
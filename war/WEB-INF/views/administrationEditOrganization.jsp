<%@page import="edu.mit.meet.meetag.entities.Organization"%>
<%@page import="edu.mit.meet.meetag.entities.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="edu.mit.meet.meetag.connectors.DAO"%>


<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Managers</th>
			<th>Account Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<%
			int currentOrg = 1;
			String name, managers;
			ArrayList<Organization> orgs = DAO.getDAO().getOrganizations(0, 10);
			for (Organization org : orgs) {
				name = currentOrg + " name";
				managers = currentOrg + " managers";
		%>
		<tr id="<%=org.getId()%>">
			<td><%=currentOrg%></td>
			<td>
				<p class="click_org" id="<%=name%>" style="display: inline"><%=org.getName()%></p>
			</td>
			<td>
				<p id="<%=managers%>" style="display: inline"><%=org.getManager()%></p>
			</td>
			<td>
				<%
					if (org.isVerified()) {
				%> <span class="badge badge-success">Verified</span> <%
 	} else {
 %> <span class="badge badge-warning">Not Verified</span> <%
 	}
 %>
			</td>
			<td><a href="#"><i class="icon-trash"
					onclick="deleteOrg('<%=org.getId()%>')"></i></a> <a data-toggle="modal"
				href="#myModal<%=currentOrg%>"><i class="icon-pencil"></i></a></td>
		</tr>




		<section class="modal hide" id="myModal<%=currentOrg%>">
			<section class="modal-header">
				<button type="button" class="close" data-dismiss="modal">�</button>
				<h3>Modal header</h3>
			</section>
			<section class="modal-body">
				<%
					for (String user : org.getUsers()) {
				%>
				<p><%=user%></p>
				<%
					}
				%>
				<p>One fine body</p>
			</section>
			<section class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a> <a href="#"
					class="btn btn-primary">Save changes</a>
			</section>
		</section>





		<%
			currentOrg++;
			}
		%>
	</tbody>
</table>
<section class="pagination">
	<ul>
		<li><a href="#">Prev</a></li>
		<li><a href="#">Next</a></li>
	</ul>
</section>
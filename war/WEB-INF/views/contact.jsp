<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<script>
	!function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (!d.getElementById(id)) {
			js = d.createElement(s);
			js.id = id;
			js.src = "//platform.twitter.com/widgets.js";
			fjs.parentNode.insertBefore(js, fjs);
		}
	}(document, "script", "twitter-wjs");
</script>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container" style="margin-top: 1.61803398875%;">
		<section class="span5">

			<form class="form-horizontal">
				<legend>Contact Us</legend>
				<div class="control-group">
					<p>Use the form below to send us your contact us. We will get
						back to you as soon as possible.</p>
				</div>
				<div class="control-group">
					<input type="text" class="input span4" placeholder="Full Name"
						id="contactName">
				</div>
				<div class="control-group">
					<input type="text" class="input span4" placeholder="Email"
						id="contactEmail">
				</div>
				<div class="control-group">
					<select class="select span4" id="contactQuery">
						<option>General</option>
						<option>Submit A Lesson</option>
						<option>Get Involved</option>
						<option>Submit A Bug</option>
					</select>
				</div>
				<div class="control-group">
					<textarea rows="3" class="input span4" placeholder="Message"
						id="contactMessage" style="max-height: 200px;"></textarea>
				</div>
				<div class="control-group">
					<button type="button" class="btn btn-primary span1"
						onclick="sendContactUs();">Send</button>
					<button type="Reset" class="btn span1">Reset</button>

				</div>
			</form>
		</section>
		<section class="span5">
			<legend>Socialize</legend>
			<div class="control-group">
				<p>Use the following social links to connect with us. Stay tuned
					for more updates.</p>

				<ul class="unstyled contact-types">
					<li><a href="https://twitter.com/simplexproject"
						target="_blank"><img src="/images/social/twitter.png" /></a> <a
						href="https://www.facebook.com/SIMPLEx.project" target="_blank"><img
							src="/images/social/facebook.png" /></a> <a
						href="https://plus.google.com/u/1/b/114523489619719975580/114523489619719975580/posts"
						target="_blank"><img src="/images/social/gplus2.png" /></a> <a
						href="http://blog.thesimplex.co" target="_blank"><img
							src="/images/social/blogger.png" /></a></li>
				</ul>
			</div>
			<br /> <br /> <br />
			<legend>Tweet Tweet!</legend>
			<div class="control-group">
				<iframe allowtransparency="true" frameborder="0" scrolling="no"
					src="http://platform.twitter.com/widgets/tweet_button.1337330192.html#_=1338790122448&amp;count=none&amp;id=twitter-widget-2&amp;lang=en&amp;original_referer=http%3A%2F%2Fsoundready.fm%2Fcontact&amp;related=dannyhertz&amp;screen_name=simplexproject&amp;size=l&amp;text=I%20think%20that...&amp;type=mention"
					class="twitter-mention-button twitter-count-none"
					style="width: 205px; height: 28px;" title="Twitter Tweet Button"></iframe>
			</div>
		</section>
	</section>
	<%@ include file="footer.jsp"%>
</body>
<script type="text/javascript">
	$(document).ready($('#contact').addClass('active'));
</script>
</html>
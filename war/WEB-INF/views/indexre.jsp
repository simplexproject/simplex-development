<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>

<link rel="stylesheet" type="text/css" href="/css/index_revamp.css" />

<!-- Importing jQuery library to enable panning effect -->
<script src="/script/jquery.spritely-0.1.js"></script>

<!-- Importing local page-specific JavaScript -->
<script src="/script/indexre.js"></script>
</head>
<body>
	<section id="mainSpan" class="fullSpan">
		<section id="mid">
			<section id="in">
				<img src="/images/logo_tr.png">
				<span id="glyph" class="glyphicon glyphicon-chevron-down"></span>
			</section>
		</section>
	</section>
	<section id="featuresSpan" class="fullSpan"></section>
	<section id="aboutSpan" class="fullSpan"></section>
</body>
</html>
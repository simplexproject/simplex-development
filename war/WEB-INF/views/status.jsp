<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<style type="text/css" media="all">
@import "css/status.css";
</style>
</head>
<body class="noproblems good">
	<div id="header">
		<div class="inner">
			<h2 id="logo">
				<a class="brand" href="/">SIMPLE<sub>x</sub>
				</a>
			</h2>
			<p class="misc">
				This page will <strong>automatically refresh</strong> (refreshing in
				<span id="time-to-update">4</span> seconds)
			</p>
		</div>
		<!-- .inner -->
	</div>
	<!-- /#header -->
	<div id="statusbar">
		<div class="pageheader_wrap">
			<div class="inner">
				<p>
					As of <span class="last-updated">June 16, 2012 @ 12:00AM PST</span>
				</p>
				<h1>All systems go.</h1>
				<span class=""></span>
			</div>
		</div>
	</div>
	<!-- /#statusbar -->
	<div id="realtime" class="clearfix">
		<div class="inner clearfix">
			<div id="simplex" class="service"
				title="SIMPLE<sub>x</sub>: Fully operational.">
				<span class="status up"></span> SIMPLE<sub>x</sub>
			</div>
			<div id="ideone" class="service" title="Ideone: Fully operational.">
				<span class="status up"></span> Ideone
			</div>
			<div id="gae" class="service" title="GAE: Fully operational.">
				<span class="status up"></span> Google App Engine
			</div>
			<div class="spinner" style="display: none;">
				<img src="images/spinner.gif" width="18" height="18">
			</div>
		</div>
	</div>
	<div id="content">
		<div class="main">
			<h3 class="good">Today</h3>
			<p>All systems operational</p>
			<h3 class="good">June 15, 2012</h3>
			<p>All systems operational, 24 hour uptime</p>
			<h3 class="good">June 14, 2012</h3>
			<p>All systems operational, 24 hour uptime</p>
			<h3 class="good">June 13, 2012</h3>
			<p>All systems operational, 24 hour uptime</p>
			<h3 class="good">June 12, 2012</h3>
			<p>All systems operational, 24 hour uptime</p>
			<h3 class="good">June 11, 2012</h3>
			<p>All systems operational, 24 hour uptime</p>
			<h3 class="good">June 10, 2012</h3>
			<p>All systems operational, 24 hour uptime</p>
		</div>
		<!-- /.main -->

		<div class="sidebar">

			<div class="module">
				<div class="inner">
					<h2>What does this page mean?</h2>
					<p>
						We monitor the status of the <strong>SIMPLE<sub>x</sub></strong>
						and the <strong>Ideone</strong> platform operations for <a
							href="http://thesimplex.co">SIMPLE<sub>x</sub></a>. If there are
						any interruptions in the services, a note will be posted here.
					</p>
					<p>
						If you are experiencing problems connecting to SIMPLE<sub>x</sub>
						and do not see a notice posted, please email <a
							href="mailto:support@thesimplex.co">support@thesimplex.co</a>
					</p>
				</div>
			</div>
			<!-- /.module -->

			<div class="module">
				<div class="inner">
					<h2 class="twitter">
						Instant updates <a href="http://twitter.com/simplexproject">@simplexproject</a>
					</h2>
					<div id="tweets">
						<div class="tweet">
							<p>
								It's almost party time! Join us tonight at 8pm to celebrate our
								GitHub for Windows! <a href="https://t.co/6ZGDPu3d">https://t.co/6ZGDPu3d</a>
							</p>
							<p class="meta">
								<a href="http://twitter.com/github/statuses/213745250483699700"
									title="view tweet on twitter">about 9 hours ago</a>
							</p>
						</div>
						<div class="tweet">
							<p>
								The GitHub Data Challenge: and the winners are... <a
									href="https://t.co/21He25GV">https://t.co/21He25GV</a>
							</p>
							<p class="meta">
								<a href="http://twitter.com/github/statuses/212653786945175550"
									title="view tweet on twitter">3 days ago</a>
							</p>
						</div>
						<div class="tweet">
							<p>
								Chad Humphries is a GitHubber! <a href="https://t.co/02u0Zkhk">https://t.co/02u0Zkhk</a>
							</p>
							<p class="meta">
								<a href="http://twitter.com/github/statuses/212616707578134530"
									title="view tweet on twitter">3 days ago</a>
							</p>
						</div>
						<div class="tweet">
							<p>
								API v1 and v2 have reached their End of Life <a
									href="https://t.co/cLXkDAFY">https://t.co/cLXkDAFY</a>
							</p>
							<p class="meta">
								<a href="http://twitter.com/github/statuses/212578113094299650"
									title="view tweet on twitter">3 days ago</a>
							</p>
						</div>
						<div class="tweet">
							<p>
								Slava Shirokov is a GitHubber! <a href="https://t.co/SvrgNVks">https://t.co/SvrgNVks</a>
							</p>
							<p class="meta">
								<a href="http://twitter.com/github/statuses/212351714228047870"
									title="view tweet on twitter">4 days ago</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- /.module -->

		</div>
		<!-- /.sidebar -->
	</div>
	<!-- /#content -->

<%@ include file="footer.jsp"%>
</body>
</html>
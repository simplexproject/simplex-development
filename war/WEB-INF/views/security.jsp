<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container well"
		style="width: 300px; text-align: center;">
		<h2>Reset Password</h2>
		<br />
		<form>
			<input type="text" class="input-xlarge" id="oldPassword"
				placeholder="Current Password"> <input type="text"
				class="input-xlarge" id="newPassword" placeholder="New Password">
			<input type="text" class="input-xlarge" id="newPasswordConfirmation"
				placeholder="Confirm New Password">
		</form>
		<button class="btn">
			<i class="icon-lock"></i>
			<h6>Change my password</h6>
		</button>
	</section>
</body>
<%@include file="footer.jsp"%>
</html>
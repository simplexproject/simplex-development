<legend>Supporters</legend>
<section class="span12">
	<ul class="thumbnails">
		<li>
				<a href="https://www.facebook.com/amosyoffe" target="_blank"><span
					class="supName">Amos Yoffe</span><img class="supImg"
					src="images/supporters/AmosYoffe.jpg" alt=""></a>
			</li>
		<li>
				<a href="https://www.facebook.com/tamuz" target="_blank"><span
					class="supName">Omer Tamuz</span><img class="supImg"
					src="images/supporters/OmerTamuz.jpg" alt=""></a>
			</li>
		<li>
				<a href="https://www.facebook.com/xanderg" target="_blank"><span
					class="supName">Xander Garbett</span><img class="supImg"
					src="images/supporters/XanderGarbett.jpg" alt=""></a>
			</li>
		<li>
				<a href="https://www.facebook.com/hamzeh.younis" target="_blank"><span
					class="supName">Hamzeh Younis</span><img class="supImg"
					src="images/supporters/HamzehYounis.jpg" alt=""></a>
			</li>
		<li>
				<a href="https://www.facebook.com/yanir.levi.7" target="_blank"><span
					class="supName">Yanir Levi</span><img class="supImg"
					src="images/supporters/YanirLevi.jpg" alt=""></a>
			</li>
		<li><a href="https://www.facebook.com/zohar7ch" target="_blank"><span
				class="supName">Zohar Shevach</span><img class="supImg"
				src="images/supporters/ZoharShevach.jpg" alt=""></a></li>
	</ul>
</section>
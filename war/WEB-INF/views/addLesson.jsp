<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container">
		<section class="well centered" id="content">
			<%
				if (request.getSession().getAttribute("Lesson") == null) {
			%>
			<label>Name</label> <input type="text" id="name"> <br /> <label>Description</label>
			<textarea id="description" class="input-xlarge"></textarea>
			<br />
			<button class="btn btn-info" onclick="subPartOne();">Continue</button>
			<%
				} else {
			%>
			<%
				}
			%>
		</section>
	</section>
	<!-- Including footer -->
	<%@ include file="footer.jsp"%>
	<!-- #End footer -->
</body>
</html>
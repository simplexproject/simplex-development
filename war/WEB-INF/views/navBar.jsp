<%@page import="edu.mit.meet.meetag.connectors.ACLManager"%>
<%@page import="edu.mit.meet.meetag.connectors.DAO"%>
<%@page import="edu.mit.meet.meetag.entities.User"%>
<%@page import="edu.mit.meet.meetag.enumurations.*"%>
<section id="headerPusher">
	<!-- push -->
</section>
<nav id="navigation-bar" class="navbar navbar-fixed-top"
	role="navigation">
	<a class="brand" href="/">SIMPLE<sub>x</sub>
	</a>
	<ul id="navBar" class="nav">
		<%
			if (session.getAttribute("User") == null) {
		%>
		<li id="index"><a href="/">Home</a></li>
		<%
			} else {
		%>
		<li id="home"><a href="/home">Home</a></li>
		<%
			}
		%>
		<li id="features"><a href="/features">Features</a></li>
		<li id="courses"><a href="/courses">Courses</a></li>
		<li id="about"><a href="/about">About Us</a></li>
		<li id="contact"><a href="/contact">Contact Us</a></li>
	</ul>
	<ul class="nav pull-right">
		<%
			if (session.getAttribute("User") == null) {
		%>
		<li id="login"><a href="/login">Login</a></li>
		<%
			} else {
		%>

		<li><a data-toggle="modal" href="#myModal"><i
				class=" icon-flag"></i></a></li>

		<li class="dropdown"><a href="#" class="dropdown-toggle"
			data-toggle="dropdown">Hello, ${User.firstName}! &ensp;<b
				class="caret"></b>
		</a>

			<ul class="dropdown-menu">
				<li><a href="/home"><i class="icon-home"></i> Home</a></li>
				<li><a href="/profile"><i class="icon-user"></i> Profile</a></li>
				<li><a href="/messages"><i class="icon-inbox"></i> Messages</a></li>
				<li><a href="/security"><i class="icon-lock"></i> Security</a></li>

				<%
					if (ACLManager.allowedAccess(Role.Contributor)) {
				%>
				<li><a href="/creator"><i class="icon-plus-sign"></i>
						Creator</a></li>
				<%
					}
				%>
				<%
					if (ACLManager.allowedAccess(Role.Administrator)) {
				%>
				<li><a href="/administration"><i class="icon-cog"></i>
						Administration</a></li>

				<%
					}
				%>

				<li><a href="/help"><i class="icon-flag"></i> Help</a></li>
				<li class="divider"></li>
				<li><a href="/login/logout"><i class="icon-off"></i> Logout</a></li>
			</ul></li>
		<%
			}
		%>
	</ul>
</nav>
<script type="text/javascript">
	$(document).ready(function() {
		var height = $("#navigation-bar").height();
		$("#headerPusher").css("height", height);
	});
</script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<script type="text/javascript">
		$('#betaNotification').hide();
	</script>
	<section class="container">
		<div class="alert alert-error" style="width: 85%;">
			<h3>
				<strong>Oh snap!</strong> The resource you are looking for is not
				here.
			</h3>
			<br />
		</div>
		<ul class="thumbnails">
			<li class=""><img src="/images/404.jpg" alt=""></li>
		</ul>
	</section>
	<!-- Including footer -->
	<%@ include file="footer.jsp"%>
	<!-- #End footer -->
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
<link rel="stylesheet" type="text/css" href="css/dialog.css">
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<div class="dialog">

		<div class="dialog-content">
			<h2>Login</h2>
			<br />
			<h6>Login to get started!</h6>
			
			<!--<form action="/login/regular" method="post">
				<div align="center">
					<input id="email" name="email" type="text" class="input0s"
						placeholder="Email"> <br /> <input id="password"
						name="password" type="password" class="input"
						placeholder="Password"> <br /> <br />
					<button type="submit" class="btn btn-primary">Sign in</button>
				</div>
			</form>-->
			
			<div align="center">
				<input id="email" name="email" type="text" class="input0s" placeholder="Email">
				<br />
				<input id="password" name="password" type="password" class="input" placeholder="Password">
				<br /> <br />
				<button class="btn btn-primary" onclick="login();">Sign in</button>
			</div>
			
			<hr><br />
			
			<a href='#' onclick="facebookLogin()"><img style="width: 150px;" src="images/facebook-logo-large.png"></img></a>
			<a href='#' onclick="googleLogin()"><img style="margin-left: 25px; width: 150px;" src="images/google-logo-large.png"></img></a>
			<a href='#' onclick="twitterLogin()"><img style="margin-left: 25px; width: 150px;" src="images/twitter-logo-large.png"></img></a>
		</div>

		<div class="dialog-footer">
			<div class="dialog-footer-left">
				<a href="/">Go home</a>
			</div>
			<div class="dialog-footer-right">
				<a href="/register"> New to SIMPLE<sub>x</sub>?</a>
			</div>
		</div>

	</div>
	<%@ include file="footer.jsp"%>
</body>
<script type="text/javascript">
	$(document).ready($("#login").addClass('active'));

	function login(){
		$.
		ajax({
			type : "POST",
			url : "http://" + window.location.host +"/login/regular",
			data : {
				"user" : $("#email").val(),
				"pass" : $("#password").val(),
			},
			success : function(data) {
				if(data=="false"){
					showFailNotification("Login Failed");	
				}
			},
			error : function() {
				alert("Login Failed");
			}
		});
	}
//
	function facebookLogin()
	{
		var client_id;
		var redirect_uri = window.location.protocol + "//" + document.location.host + '/login/facebook';
		if(window.location.hostname == "localhost")
		{
			client_id = '458368827546920';
		}
		else
		{
			client_id = '323904147673897';
		}	
		var url = 'https://www.facebook.com/dialog/oauth?client_id=' + client_id + '&redirect_uri=' + redirect_uri + '&scope=email,user_birthday&response_type=code';
		window.open(url,'_self');
	}

	function googleLogin()
	{
		var client_id;
		var redirect_uri = window.location.protocol + "//" + document.location.host + '/login/google';
		if(window.location.hostname == "localhost")
		{
			client_id = '937715082662-h6eaeq2apf8eblhu6f7b01hpcb51diui.apps.googleusercontent.com';
		}
		else
		{
			client_id = '937715082662.apps.googleusercontent.com';
		}	
		var url = 'https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=' + client_id + '&redirect_uri=' + redirect_uri + '&scope=https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/plus.me';
		window.open(url,'_self');
	}
	
	function twitterLogin()
	{
		var login_uri = window.location.protocol + "//"
		var host = document.location.host;
		login_uri += host;
		login_uri += '/login/twitter';
		var redirect =  window.location.protocol + "//" + document.location.host + "/home";
		window.open(login_uri,'_self');
	}
</script>
</html>
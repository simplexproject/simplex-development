<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>

<!-- Importing jQuery library to enable panning effect -->
<script src="/script/jquery.spritely-0.1.js"></script>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section id="snippets"
		style="background-image: url('/images/snippets.png'); text-align: center; color: #555555; border-bottom: 1pxsolid #E1E1E1; background-color: #F0F0F0; height: 5cm; padding: 60px; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.3);">
	</section>
	<section class="container" style="margin-top: 5%;">
		<section class="row">
			<section class="span4">
				<!-- Including social links -->
				<%@ include file="social.jsp"%>
			</section>
			<section class="span4">
				<h2>Get Involved</h2>
				<p>
					Do you want to teach beginners and experts to code on our platform?
					Want to join the SIMPLE<sub>x</sub> editors community? <a
						href="/creator">Click here to get started.</a>
				</p>
			</section>
			<section class="span4">
				<!-- Including insights -->
				<%@ include file="insights.jsp"%>
			</section>
		</section>
		<hr>
	</section>
	<!-- Including footer -->
	<%@ include file="footer.jsp"%>
	<!-- #End footer -->
</body>
<!-- Custom scripts for the page -->
<script type="text/javascript">
	//Setting the correct navBar element as active
	$(document).ready($('#home').addClass('active'));
	//Hiding unwanted notification-area on this page
	$(document).ready($('#notification-area').hide());
	$(document).ready($('#headerPadding').hide());

	//This function is responsible for panning the sexy snippets.
	//Default fps = 30.
	//Default speed = 2.
	//Default dir = "left."
	$(document).ready(function() {
		$('#snippets').pan({
			fps : 30,
			speed : 1,
			dir : 'left'
		});
	});
</script>
</html>
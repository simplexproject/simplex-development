<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>

<!-- Importing jQuery library to enable panning effect -->
<script src="/script/jquery.spritely-0.1.js"></script>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section id="world"
		style="background-image: url('/images/worldmap.png'); text-align: center; color: #555555; border-bottom: 1px solid #E1E1E1; background-color: #F0F0F0; padding-top: 60px; padding-bottom: 100px; box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.3); z-index: 1000; margin-bottom: 30px;">
		<section
			style="margin-top: 10%; background: transparent url(world.png) 0 130px repeat-x;">
		</section>
		<h2 style="float: middle; opacity: 0.5; color: black;">Designed
			For</h2>
		<h1
			style="float: middle; opacity: 0.5; color: black; margin-bottom: 0; font-size: 60px; line-height: 1; letter-spacing: -1px;">Everyone,
			Everywhere.</h1>
	</section>
	<section class="container">
		<section class="row">
			<section class="span8 pull-left">
				<h3>
					<img src="images/glyphicons/png/glyphicons_024_parents.png">
					Built for Beginners and Experts Alike
				</h3>
				<p>
					SIMPLE<sub>x</sub> is the first website for learning programming
					languages from scratch up to an expert's level.<br /> Our platform
					encourages people from any background to learn state-of-the-art
					programming languages.
				</p>
			</section>
			<section class="span4">
				<section class="pull-right"
					style="max-height: 200px; max-width: 200px; background-color: white; text-align: center;">
					<img alt="Built for Beginners and Experts Alike."
						src="/images/features/SIMPLExForDummies.png"
						style="max-height: 200px;">
				</section>
			</section>
		</section>
		<hr>
		<section class="row">
			<section class="span4 pull-left"
				style="max-height: 200px; max-width: 200px; background-color: white; text-align: center;">
				<img alt="Playground." src="/images/features/playground.jpg">
			</section>
			<section class="span8 pull-right">
				<h3>
					<img src="images/glyphicons/png/glyphicons_289_podium.png">
					Playground
				</h3>
				<p>Using our online interactive Playground, you can easily get
					past the technical hurdle of setting up your own integrated
					development environment. Simply put -- Less setup, more coding.</p>
			</section>
		</section>
		<hr>
		<section class="row">
			<section class="span8 pull-left">
				<h3>
					<img src="images/glyphicons/png/glyphicons_214_resize_small.png">
					Collaborative Platform
				</h3>
				<p>
					We believe that everyone has the potential to teach, and who's a
					better teacher than you? Using our interactive lesson creation
					wizard, you can contribute to the content available on the
					platform. The wizard makes it super easy to go from zero to lesson
					in a couple of minutes which enables you to focus more on the
					content you share with the SIMPLE<sub>x</sub> community.
				</p>
			</section>
			<section class="span4 pull-right"
				style="max-height: 200px; max-width: 200px; background-color: white; text-align: center;">
				<img alt="Collaborative Platform."
					src="/images/features/collaboration.jpg">
			</section>
		</section>
		<hr>
		<section class="row">
			<section class="span4 pull-left"
				style="max-height: 200px; max-width: 200px; background-color: white; text-align: center;">
				<img alt="Learning Made Fun - Social Integration and Gamification."
					src="/images/features/gamification.jpg">
			</section>
			<section class="span8 pull-right">
				<h3>
					<img src="images/glyphicons/png/glyphicons_043_group.png">
					Learning Made Fun - Social Integration and Gamification
				</h3>
				<p>
					We know you love interacting with your friends even while you're
					studying. We also know that programming can be really fun if the
					competition ingredient is added to the recipe. Our platform enables
					you to share news about your progress and rewards with your social
					networks and get updates from your friends regarding theirs. You
					can join challenges and coding tournaments with your friends and
					other SIMPLE<sub>x</sub>ers around the world, and earn badges,
					awards and your own spot in the SIMPLE<sub>x</sub> Hall of Fame.
				</p>
			</section>
		</section>
		<hr>
		<section class="row">
			<section class="span8 pull-left">
				<h3>
					<img src="images/glyphicons/png/glyphicons_214_resize_small.png">
					Real Online Compiler
				</h3>
				<p>Behind the scenes we look over your code and make sure your
					syntax is correct. We run your code, test its efficiency and
					generate its output. We even tell you how much memory your code
					took to run. All while you're busy coding.</p>
			</section>
			<section class="span4 pull-right"
				style="max-height: 200px; max-width: 200px; background-color: white; text-align: center;">
				<img alt="Real Online Compiler." src="/images/features/compiler.png">
			</section>
		</section>
	</section>
	<%@ include file="footer.jsp"%>
</body>
<script type="text/javascript">
	$(document).ready($('#features').addClass('active'));
	$("#notification-area").hide();
	$(document).ready($('#headerPadding').hide());

	$(document).ready(function() {
		$('#world').pan({
			fps : 30,
			speed : 0.5,
			dir : 'left'
		});
	});
</script>
</html>
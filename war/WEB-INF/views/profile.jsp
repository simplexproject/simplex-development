<%@page import="edu.mit.meet.meetag.entities.User"%>
<html>
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container">
		<section class="container">
			<section id="highlights" style="width: 100%;">
				<table id="basicANDsocial">
					<tr>
						<td valign="top">
							<section id="basicView" class="well"
								style="background-color: white;">
								<table>
									<tr>
										<td valign="top">
											<section id="profile_pic">
												<img src="${User.avatar}" class="thumbnail" onclick="test()"
													style="max-width: 215px; max-height: 215px;" /> <br>
											</section>
										</td>
										<td valign="top">
											<h3 style="text-align: center;">${User.firstName}
												${User.lastName}</h3>
											<section id="tabs&content"
												style="margin-left: 20px; margin-top: 20px;">
												<ul class="nav nav-tabs">
													<li class="active"><a href="#basic" data-toggle="tab"><i
															class="icon-user"></i> Basic Information</a></li>
													<li><a href="#statistics" data-toggle="tab"><i
															class="icon-tasks"></i> ${User.firstName}'s Statistics</a></li>
												</ul>
												<section class="tab-content span5">
													<section class="tab-pane fade in active" id="basic">
														<table class="table table-bordered table-striped">
															<tbody>
																<tr>
																	<td>Name</td>
																	<td>
																		<p class="click" id="full_name" style="display: inline">${User.firstName} ${User.lastName}</p>
																	</td>
																</tr>
																<tr>
																	<td>Birthdate</td>
																	<td><p class="click" id="birthdate" style="display: inline">${User.birthDate}</p></td>
																</tr>
																<tr>
																	<td>Gender</td>
																	<td><p class="click" id="gender" style="display: inline">${User.gender}</p></td>
																</tr>
															</tbody>
														</table>
													</section>
													<section class="tab-pane fade" id="statistics">
														<p class="click" style="display: inline">${User.firstName}'s
															statistics should load here.</p>
													</section>
												</section>
											</section>
										</td>
									</tr>
								</table>
							</section>
						</td>
						<td valign="top">
							<section id="social" class="well"
								style="background-color: white; margin-left: 10px;">
								<table style="text-align: center;">
									<tr>
										<td>
											<h3>${User.firstName}'s Social Links</h3>
										</td>
									</tr>
									<tr>
										<td style="padding-top: 20px; padding-bottom: 20px;"><img
											style="width: 150px;" src="images/facebook-logo-large.png"
											onclick="window.location=('https://www.facebook.com/dialog/oauth?client_id=323904147673897&redirect_uri=http://localhost:8888/login/facebook&scope=email,user_birthday&response_type=code');"></img></td>
									</tr>
									<tr>
										<td style="padding-bottom: 20px;"><img
											style="width: 150px;" src="images/google-logo-large.png"
											onclick="window.location=('https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=937715082662.apps.googleusercontent.com&redirect_uri=http://localhost:8888/login/google&scope=https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/plus.me');"></img></td>
									</tr>
									<tr>
										<td><img style="width: 150px;"
											src="images/twitter-logo-large.png"
											onclick="window.location=('https://api.twitter.com/oauth/request_token?oauth_callback=http://localhost:8888/login/twitter');"></img></td>
									</tr>
								</table>
							</section>
						</td>
					</tr>
				</table>
			</section>
			<section class="well" style="background-color: white;">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#userCourses" data-toggle="tab"><i
							class="icon-file"></i> ${User.firstName}'s Courses</a></li>
					<li><a href="#userLessons" data-toggle="tab"><i
							class="icon-file"></i> ${User.firstName}'s Lessons</a></li>
				</ul>
				<section class="tab-content">
					<section class="tab-pane fade in active" id="userCourses">
						<p style="display: inline">Etsy mixtape wayfarers, ethical wes
							anderson tofu before they sold out mcsweeney's organic lomo retro
							fanny pack lo-fi farm-to-table readymade.</p>
					</section>
					<section id="userLessons" class="tab-pane fade">
						<p style="display: inline">Trust fund seitan letterpress,
							keytar raw denim keffiyeh etsy art party before they sold out
							master cleanse gluten-free squid scenester freegan cosby sweater.</p>
					</section>
				</section>
			</section>
		</section>
		<script type="text/javascript">
	$(document).ready(function() {
		var highlightsHeight = ($("#highlights").height() - 40);
		var containerWidth = $(".container").width();
		$("#basicANDsocial").css("width", containerWidth);
		$("#basicView").css("height", highlightsHeight);
		$("#social").css("height", highlightsHeight);
	});
</script>
	</section>
	<%@ include file="footer.jsp"%>
</body>
<script>
	$(function() {

		$(".editable_select").editable(updateRole, {
			indicator : '<img src="images/indicator.gif">',
			data : "{'User':'User','Administrator':'Administrator'}",
			type : "select",
			submit : "OK"
		});

		$(".click")
				.editable(
						updateUser,
						{
							indicator : "<img src='images/indicator.gif'>",
							tooltip : "Click to edit...",
							style : "inherit"
						});

		function updateRole(value, settings) {

			var user = this.parentNode.parentNode.id;
			$.ajax({
				type : "POST",
				url : location.protocol + '//' + location.host + "/user/" + user + "/update",
				dataType : "json",
				data :
				{
					"field" : "role",
					"value" : value
				}
			});

			return value;

		}
		function updateUser(value, settings) {
			var field = this.id;

			$.ajax({
				type : "POST",
				url : location.protocol + '//' + location.host + "/user/me/update",
				dataType : "json",
				data : {
					"field" : field,
					"value" : value
				}
			});
			return value;
		}
	});
</script>
</html>
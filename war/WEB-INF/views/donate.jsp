<!DOCTYPE html>
<%@page
	import="org.springframework.web.bind.annotation.SessionAttributes"%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>


<body>
<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<script type="text/javascript">
		$('#betaNotification').hide();
	</script>
	<section class="container">
		<section class="well">
			<h1 style="margin-left: 10px;">
				Donate<sub>x</sub>
			</h1>
			<h6 style="margin-left: 15px">Together we can create a
				technologically advanced world.</h6>
			<br /> <br />
			<p>
				Just leave us your name and e-mail address to request an invitation
				to try out SIMPLE<sub>x</sub>.
			<p>
				Once you receive an invite, you'll be able to test drive our new
				community features before anyone else and can lend a hand by helping
				other SIMPLE<sub>x</sub> users get the best experience.
			</p>
			<p>
				We're counting on your feedback and participation to make SIMPLE<sub>x</sub>
				even more helpful for you and the millions of SIMPLE<sub>x</sub>
				users who visit us every day!
			</p>
			<br />
			<form style="margin-left: 10px;" class="">
				<input type="text" class="input" placeholder="Name"> <br />
				<input type="text" class="input" placeholder="Email"> <br />
				<div class="input-prepend input-append">
					<span class="add-on">$</span><input class="span2"
						id="appendedPrependedInput" size="16" placeholder="Amount"
						style="width: 183px;" type="text">
				</div>
				<br />
				<button type="submit" class="btn btn-primary">Donate</button>
			</form>
			<img style="margin-left: 580px; margin-top: -140px;" alt="Donate"
				src="/images/donate.png"> <br />

			<p>
				To contact us about supporting our efforts, email us at <a
					href="mailto:donate@simplex.co">donate@simplex.co</a>.
			</p>


		</section>
	</section>
	<%@ include file="footer.jsp"%>
</body>
</html>
<section id="footerPadding" style="height: 40px"></section>
<footer
	style="background: whiteSmoke; border-top: 1px solid #E5E5E5; bottom: 0; min-height: 1%; position: fixed; width: 100%; z-index: 1000; box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);">
	<p align="center">
		<br /> � 2012 <strong>SIMPLE<sub>x</sub></strong> uses the <a
			href="http://ideone.com">IDEOne API</a> &copy; by <a
			href="http://sphere-research.com">Sphere Research Labs</a>
	</p>
</footer>
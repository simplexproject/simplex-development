<%@page import="edu.mit.meet.meetag.entities.Course"%>
<section class="container">
	<section id="noCourses">
		<h1
			style="color: white; text-shadow: -1px 0 #E5E5E5, 0 1px #E5E5E5, 1px 0 #E5E5E5, 0 -1px #E5E5E5; margin-bottom: 40px;">
			There are no Courses that exist yet. <br /> Create a new Course and
			it will be displayed here.
		</h1>
	</section>
	<section id="exCoursesPanel">
		<h2>Courses</h2>
		<hr style="margin-top: -15px;" />
		<section>
			<ul class="thumbnails" id="existingCourses">
			</ul>
		</section>
	</section>
</section>
<section class="container well">
	<a href="javascript:void(0);">
		<section class="add" data-toggle="modal" data-target="#myModal">
			<img style="height: 80px; width: 80px;" src="/images/plus.png" />
		</section>
	</a>
</section>
<!-- Modal -->
<section class="modal fade" id="myModal">
	<section class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3 id="myModalLabel">Create A New Course</h3>
	</section>
	<section class="modal-body">
		<form class="pull-left" action="/creator/createCourse"
			id="addCourseForm">
			<input id="cNaI" type="text" name="cName" placeholder="Course Name" />
			<br />
			<textarea id="cDesI" name="cDesc" placeholder="Course Description"
				rows="3"></textarea>
			<br /> <input id="cAvI" type="text" name="cAvatar"
				placeholder="Course Avatar (paste link here)" />
		</form>
		<section id="help-info" class="span2 label label-info"></section>
	</section>
	<section class="modal-footer">
		<button class="btn" data-dismiss="modal">Cancel</button>
		<a id="addButton" rel="popover" data-original-title="Preview"
			class="btn btn-primary submit">Create</a>
	</section>
</section>
<script type="text/javascript" src="/script/wizard.js"></script>
<script type="text/javascript" src="/script/popover.js"></script>
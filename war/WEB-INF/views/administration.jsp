<!DOCTYPE html>
<%@page import="edu.mit.meet.meetag.entities.SimplexViewResolver"%>
<%@page import="edu.mit.meet.meetag.entities.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="edu.mit.meet.meetag.connectors.DAO"%>
<%@page import="edu.mit.meet.meetag.connectors.ConfigurationManager"%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>

<!-- These must be moved to a .css file, this is not conventional code. -->
<style type="text/css">
.editable_select select {
	width: 65%;
	margin-right: 5%;
	max-height: 75%;
}

.editable_select button {
	width: 25%;
	max-height: 75%;
}

.click input {
	min-width: 50%;
}
</style>

<script type="text/javascript">
	$(function() {
		$(".editable_select")
				.editable(
						updateRole,
						{
							indicator : '<img src="images/indicator.gif">',
							data : "{'User':'User','Contributor':'Contributor','Administrator':'Administrator'}",
							type : "select",
							submit : "OK"
						});

		$(".click").editable(updateUser, {
			indicator : "<img src='images/indicator.gif'>",
			tooltip : "Click to edit...",
			style : "inherit"
		});

		$(".click_org").editable(updateOrganization, {
			indicator : "<img src='images/indicator.gif'>",
			tooltip : "Click to edit...",
			style : "inherit"
		});

	});

	function changeStatus(id) {
		var obj = $("#status" + id)[0];
		$.ajax({
			type : "GET",
			url : location.protocol + '//' + location.host+"/system/siteStatus/" + id,
			dataType : "json"
		});

	}
</script>

</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container">
		<h2>
			SIMPLE<sub>x</sub> Administration
		</h2>
		<p>Click the tabs below to toggle between different control
			panels.</p>
		<ul id="myTab" class="nav nav-tabs">
			<li class="active"><a href="#users" data-toggle="tab"><i
					class="icon-user"></i> Users</a></li>
			<li><a href="#organizations" data-toggle="tab"><i
					class="icon-briefcase"></i> Organizations</a></li>
			<li><a href="#platform" data-toggle="tab"><i
					class="icon-wrench"></i> Platform</a></li>
			<li class="dropdown"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown"><i class="icon-file"></i> Content <b
					class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#courses" data-toggle="tab">Courses</a></li>
					<li><a href="#lessons" data-toggle="tab">Lessons</a></li>
				</ul></li>
		</ul>
		<section id="myTabContent" class="tab-content">

			<section class="tab-pane active" id="users">
				<%@include file="administrationEditUser.jsp"%>
			</section>
			<section class="tab-pane" id="organizations">
				<%@include file="administrationEditOrganization.jsp"%>
			</section>

			<section class="tab-pane " id="platform">
				<table class="table table-bordered table-striped">
					<tbody>
						<tr>
							<td>Site Status</td>
							<td>
								<%
									int currentStatus = SiteStatus.valueOf(
											ConfigurationManager.getInstance().getSetting(
													SiteStatus.SITE_STATUS_KEY)).getId();
									/*Default is online*/
									if (currentStatus == SiteStatus.Default.getId()) {
										currentStatus = SiteStatus.Online.getId();
									}
									ArrayList<String> statuses = new ArrayList<String>();
									for (int i = 0; i < 5; i++) {
										if (i == currentStatus) {
											statuses.add(i, "active btn btn-primary");
										} else {
											statuses.add(i, "btn btn-primary");
										}
									}
								%>
								<section class="btn-group" data-toggle="buttons-radio">
									<p>
										<button
											class="<%=statuses.get(SiteStatus.Online.getId())%> btn btn-success"
											onclick="changeStatus(<%=SiteStatus.Online.getId()%>);">Online</button>
										<button
											class="<%=statuses.get(SiteStatus.Offline.getId())%> btn"
											onclick="changeStatus(<%=SiteStatus.Offline.getId()%>);">Offline</button>
										<button
											class="<%=statuses.get(SiteStatus.Maintenance.getId())%> btn btn-warning"
											onclick="changeStatus(<%=SiteStatus.Maintenance.getId()%>);">Maintenance</button>
									</p>
								</section>
							</td>
						</tr>
						<tr>
							<td>Development Status</td>
							<td>

								<section class="btn-group" data-toggle="buttons-radio">
									<p>
										<button
											class="<%=statuses.get(SiteStatus.Beta.getId())%> btn btn-info"
											onclick="changeStatus(<%=SiteStatus.Beta.getId()%>);">Beta</button>
										<button
											class="<%=statuses.get(SiteStatus.Testing.getId())%> btn btn-danger"
											onclick="changeStatus(<%=SiteStatus.Testing.getId()%>);">Testing</button>
									</p>
								</section>
							</td>
						</tr>
						<tr>
							<td>Lesson and Course Approval</td>
							<td>
								<section class="btn-group" data-toggle="buttons-radio">
									<button class="btn btn-primary">Automatic</button>
									<button class="active btn btn-primary">Manual</button>
								</section>
							</td>
						</tr>
						<tr>
							<td>News</td>
							<td>
								<button class="btn btn-primary">Add new update</button>
							</td>
						</tr>
					</tbody>
				</table>
			</section>
			<section class="tab-pane " id="courses">
				<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they
					sold out mcsweeney's organic lomo retro fanny pack lo-fi
					farm-to-table readymade. Messenger bag gentrify pitchfork tattooed
					craft beer, iphone skateboard locavore carles etsy salvia banksy
					hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify
					squid 8-bit cred pitchfork. Williamsburg banh mi whatever
					gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk
					vice blog. Scenester cred you probably haven't heard of them, vinyl
					craft beer blog stumptown. Pitchfork sustainable tofu synth
					chambray yr.</p>
			</section>
			<section class="tab-pane " id="lessons">
				<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy
					art party before they sold out master cleanse gluten-free squid
					scenester freegan cosby sweater. Fanny pack portland seitan DIY,
					art party locavore wolf cliche high life echo park Austin. Cred
					vinyl keffiyeh DIY salvia PBR, banh mi before they sold out
					farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral,
					mustache readymade thundercats keffiyeh craft beer marfa ethical.
					Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
			</section>
		</section>
		<hr>
	</section>
	<!-- Including footer -->
	<%@ include file="footer.jsp"%>
	<!-- #End footer -->
	<script src="/script/updateUser.js"></script>
	<script src="/script/updateOrganization.js"></script>
</body>
</html>
<!-- TODO: read more on taglib -->
<%@page import="edu.mit.meet.meetag.entities.Component"%>
<%@page import="java.util.List"%>
<%@page import="edu.mit.meet.meetag.entities.Lesson"%>
<html>
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="row">
		<section class="container">
			<section class="well">
				<h4>${Lesson.title}</h4>
				<br> ${Lesson.description} <br>
			</section>

			<%
				Lesson l = (Lesson) request.getAttribute("Lesson");
				int i=0;
				for (Component comp : l.getComponents()) {
					System.out.println("Iter'ing comp's");
					if (comp == null)
						System.out.println("SHIT");
					request.setAttribute("componentId", comp.getId()+i++);
					for (String key : comp.getParams().keySet()) {
						System.out.println("iter'ing key's");
						request.setAttribute(key, comp.getParam(key));
					}
					System.out.println("including");
					pageContext.include(comp.getView(), true);
					System.out.println("PROBLEM IS NOW?");
				}
			%>
		</section>
	</section>


	<%@ include file="footer.jsp"%>
</body>
</html>
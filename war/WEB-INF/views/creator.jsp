<!DOCTYPE html>
<%@page import="edu.mit.meet.meetag.entities.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="edu.mit.meet.meetag.connectors.DAO"%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body style="background-image: url('/images/creatorBackground.png');">
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container">
		<section class="well hero-unit span6">
			<section class="span4">
				<h2>
					Teach beginners and experts to code on our platform<i
						class=" icon-plus-sign"></i>
				</h2>
				<br>
				<h6>
					Becoming a course creator is quick and easy.<br /> Start building
					right away!
				</h6>
				<br>
				<button class="btn btn-primary btn-large"
					onClick="location.href='/wizard'">Manage Courses</button>
			</section>
			<section>
				<img src="/images/pencil.gif">
			</section>
		</section>
	</section>
	<script lang="text/javascript">
		$('#footerPadding').hide();
	</script>
	<%@ include file="footer.jsp"%>
</body>
</html>
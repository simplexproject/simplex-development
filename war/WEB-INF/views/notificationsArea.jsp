<!-- defining notification area -->
<section id="notification-area" style="position: fixed;">
	<%
		if (request.getParameter("notification") == null) {
	%>
	<section style="width: 350px; margin: 5px;"></section>
	<%
		} else {
			if (request.getParameter("notification-type").equals("success")) {
	%>
	<section class="alert alert-success fade in"
		style="width: 350px; margin: 5px;">
		<button class="close" data-dismiss="alert">&times;</button>
		<strong>Woo-hoo!</strong>
		<%=request.getParameter("notification")%>
	</section>
	<%
		} else if (request.getParameter("notification-type").equals(
					"error")) {
	%>
	<section class="alert alert-error fade in"
		style="width: 350px; margin: 5px;">

		<button class="close" data-dismiss="alert">&times;</button>
		<strong>Oh snap!</strong>
		<%=request.getParameter("notification")%>
	</section>
	<%
		} else if (request.getParameter("notification-type").equals(
					"info")) {
	%>
	<section class="alert alert-info fade in"
		style="width: 350px; margin: 5px;">

		<button class="close" data-dismiss="alert">&times;</button>
		<strong>Heads up!</strong>
		<%=request.getParameter("notification")%>
	</section>
	<%
		} else if (request.getParameter("notification-type").equals(
					"warning")) {
	%>
	<section class="alert fade in" style="width: 350px; margin: 5px;">

		<button class="close" data-dismiss="alert">&times;</button>
		<strong>Warning!</strong>
		<%=request.getParameter("notification")%>
	</section>
	<%
		}
		}
	%>
</section>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container" style="margin-top: 1.61803398875%;">
		<section class="span12"></section>
		<section class="span8">
			<form class="bs-docs-example form-horizontal">
				<legend>Feedback</legend>
				<div class="control-group">
					<p>Use the form below to send us your comments. We read all
						feedback carefully, but please note that we cannot respond to the
						comments you submit.</p>
				</div>
				<div class="control-group">
					<input type="text" id="name" placeholder="Name">
				</div>
				<div class="control-group">
					<input type="text" id="email" placeholder="Email">
				</div>
				<div class="control-group">
					<input type="text" id="subject" placeholder="Subject">
				</div>
				<div class="control-group">
					<select id="feedback-type">
						<option value="">Select Feedback Type:</option>
						<option value="Bug Report">Bug Report</option>
						<option value="Design">Design</option>
						<option value="Content Related Feedback">Content Related
							Feedback</option>
					</select>
				</div>
				<div class="control-group">

					<select id="os" name="os">
						<option value="">Select OS:</option>
						<option value="Windows XP">Windows XP</option>
						<option value="Windows Vista">Windows Vista</option>
						<option value="Windows 7">Windows 7</option>
						<option value="Windows 8">Windows 8</option>

						<option value="OS X Snow Leopard">OS X Snow Leopard</option>
						<option value="OS X Lion">OS X Lion</option>
						<option value="OS X Mountain Lion">OS X Mountain Lion</option>

						<option value="Ubuntu">Ubuntu</option>
						<option value="Debian">Debian</option>


					</select>
				</div>
				<div class="control-group">

					<select id="browser" name="brwoser">
						<option value="">Select Browser:</option>
						<option value="Chrome">Chrome</option>
						<option value="Firefox">Firefox</option>
						<option value="Safari">Safari</option>
						<option value="Internet Explorer">Internet Explorer</option>
						<option value="Opera">Opera</option>
						<option value="Other">Other</option>
					</select>
				</div>
				<div class="control-group">

					<textarea id="comments" class="span4" name="comments"
						placeholder="Comments"></textarea>
				</div>
			</form>
		</section>
	</section>
	<%@ include file="footer.jsp"%>
</body>
</html>
<section class="well" onclick="slideDesc()">
	<h3>${title}</h3>
	<br />
	<section align="center">
		<iframe width=${width } height=${height
						}
			src="http://www.youtube.com/embed/${videoID}" frameborder="0"
			allowfullscreen></iframe>
	</section>
	<section id="description" style="margin-top: 10px; display: none;">
		<h6>${description}</h6>
	</section>
</section>
<script type="text/javascript">
	function slideDesc() {
		$("#description").slideToggle("slow");
	}
</script>
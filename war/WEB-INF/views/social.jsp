<h2>Social</h2>
<p>Use the following social links to connect with us. Stay tuned for
	more updates.</p>

<ul class="unstyled contact-types">
	<li><a href="https://twitter.com/simplexproject" target="_blank"><img
			src="/images/social/twitter.png" /></a> <a
		href="https://www.facebook.com/SIMPLEx.project" target="_blank"><img
			src="/images/social/facebook.png" /></a> <a
		href="https://plus.google.com/u/1/b/114523489619719975580/114523489619719975580/posts" target="_blank"><img
			src="/images/social/gplus2.png" /></a> <a
		href="http://blog.thesimplex.co" target="_blank"><img
			src="/images/social/blogger.png" /></a></li>
</ul>
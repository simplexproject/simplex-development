<section class="well">
	<section onclick="slideAnswer(${id})">
		<h3><%=request.getAttribute("title")%></h3>
		<h6 style="margin-left: 20px;"><%=request.getAttribute("question")%></h6>
	</section>
	<section id="${id}" style="display: none;">
		<br />
		<textarea id="text${id}" onkeyup="showSubmission(${id})" rows="3"
			style="min-width: 900px; max-width: 900px; max-height: 202px; padding: 19px;"
			placeholder="Answer here."></textarea>
		<section id="doneyet${id}"
			style="background-color: #EEE; min-height: 48px; display: none;">
			<h6 style="float: left; margin: 10px; margin-top: 15px;">Done?</h6>
			<button style="float: right; margin: 10px;" class="btn btn-primary">Submit</button>
		</section>
	</section>
</section>

<script type="text/javascript">
	function showSubmission(id) {
		var text = $("#text"+id);
		var length = text[0].value.length;

		if (length <= 0) {
			document.getElementById("doneyet"+id).style.display = "none";
		} else if (length >= 1) {
			document.getElementById("doneyet"+id).style.display = "block";
		}
	}
	function slideAnswer(id) {
		$("#"+id).slideToggle("slow");
	}
</script>
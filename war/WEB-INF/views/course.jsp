<%@page import="edu.mit.meet.meetag.entities.Course"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container">
		<table>
			<tr>
                <td valign="top"><img alt=${course.name} class="img-polaroid small"
					src=${course.avatar} style="display: inline; width: 140px; height: 140px;">
				</td>
				<td valign="top">
					<section style="margin-left: 20px">
						<h2 style="display: inline">${course.name}</h2>
						<p>${course.description}</p>
						<%
							User user = (User) (session.getAttribute("User"));

							Course course = (Course) (request.getAttribute("course"));
							//if (user.isEnrolled(course.getId())) {
							if (!user.isEnrolled(course.getId())){ %>
								<button id="enrollButton" class="btn btn-primary" href="#" onclick="enrollUser()">Enroll now!</button>
							<%
							}else{							%>
								<span class="label label-info btn-primary btn-large" href="#" onclick="unenrollUser()">You are enrolled to this course!</span>
							<%} %>						
					</section></td>
			</tr>
		</table>
		<br />
		<section id="accordion2" class="accordion">
			<c:forEach items="${course.lessons}" var="lesson">
				<section class="accordion-group">
					<section class="accordion-heading">
						<a class="accordion-toggle" data-toggle="collapse"
							data-parent="#accordion2" href="#collapse${lesson.id}">${lesson.title}
						</a>
					</section>
					<section id="collapse${lesson.id}" class="accordion-body collapse">
						<section class="accordion-inner">
							${lesson.description} <a
								href="/course/${course.id}/${lesson.id }">Get Started!</a>
						</section>
					</section>
				</section>
			</c:forEach>
		</section>

	</section>

	<%@ include file="footer.jsp"%>
<script type="text/javascript">
	function enrollUser(){
		$.ajax({
			type : "POST",
			url : location.protocol + '//' + location.host + "/user/enroll/" + ${course.id},
			success : function(data) {
				window.location.href = "#";
				$("#enrollButton").replaceWith('<span class="label label-info btn-primary btn-large" href="#" onclick="unenrollUser()">You are enrolled to this course!</span>')
			},
			error : function(request, status, error) {
				alert(location.protocol + '//' + location.host + "/user/enroll/" + ${course.id})
				alert(status + " " + error);
			}
		});
	}
	function unenrollUser(){
        $.ajax({
			type : "POST",
			url : location.protocol + '//' + location.host + "/user/unenroll/" + ${course.id},
			success : function(data) {
            window.location.href = "#";
				$("#enrollButton").replaceWith('<span class="label label-info btn-primary btn-large" href="#" onclick="enrollUser()">Enroll now!</span>')
			},
			error : function(request, status, error) {
				alert(location.protocol + '//' + location.host + "/user/unenroll/" + ${course.id})
				alert(status + " " + error);
			}
		});
	}
</script>
</body>
</html>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
<script type="text/javascript">
	$(document).ready($('#help').addClass('active'));
</script>
</head>
<body>
<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container" style="width: 600px;">
		<section class="well" style="background-color: white;">
			<h1>Help</h1>
			<ul class="nav nav-tabs" style="margin-top: 10px;">
				<li class="active"><a href="#helpBasics" data-toggle="tab"><i
						class="icon-file"></i>Basics</a></li>
				<li><a href="#helpTakeCourses" data-toggle="tab"><i
						class="icon-file"></i>Taking Courses</a></li>
				<li><a href="#helpPlayground" data-toggle="tab"><i
						class="icon-file"></i>Playground</a></li>
				<li><a href="#helpAccount" data-toggle="tab"><i
						class="icon-file"></i>Account</a></li>
			</ul>
			<section class="tab-content">
				<section class="tab-pane fade in active" id="helpBasics">
					Basic help.</section>
				<section class="tab-pane fade in" id="helpTakeCourses">
					Courses help.</section>
				<section class="tab-pane fade in" id="helpPlayground">
					Playground help.</section>
				<section class="tab-pane fade in" id="helpAccount">Account
					help.</section>
			</section>
		</section>
	</section>
		<%@ include file="footer.jsp"%>
</body>
</html>
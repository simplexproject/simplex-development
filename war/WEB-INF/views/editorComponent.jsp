<script type="text/javascript">
function saveSettings${componentId}(){
	editor${componentId}.setTheme($("#theme").val());
	var Lang = require($("#mode").val()).Mode;
    editor${componentId}.getSession().setMode(new Lang());
    editor${componentId}.setFontSize($("#fontsize").val());
    editor${componentId}.setShowFoldWidgets($("#folding").is(':checked'));
    editor${componentId}.setHighlightActiveLine($("#highlight_active").is(':checked'));
    editor${componentId}.setShowInvisibles($("#show_hidden").is(':checked'));
    editor${componentId}.setBehavioursEnabled($("#enable_behaviours").is(':checked'));
    editor${componentId}.setHighlightSelectedWord($("#highlight_selected_word").is(':checked'));
    
}
function resetEditor${componentId}(){
	editor${componentId}.selectAll();
	editor${componentId}.insert("#Welcome to SIMPLEx\n");
}
</script>
<section class="well container"
	style="background-color: white; box-shadow: 0px 0px 12px rgba(0, 0, 0, 0.3);">
	<section>
		<h3>
			<i class="icon-chevron-right"></i>
			<%=request.getAttribute("title")%></h3>
		<h6 style="margin-left: 20px;"><%=request.getAttribute("description")%></h6>
	</section>
	<section>
		<%@ include file="editor.jsp"%>
	</section>
	<section class="container">
		<hr>
	</section>
	<section id="options" class="span4 container pull-left">
		<button onclick="sendCompilationReuqest${componentId}()">
			<i id="run-icon" class="icon-play-circle"></i> Run
		</button>
		<button onclick="resetEditor${componentId}()">
			<i class="icon-repeat"></i> Clear
		</button>
		<button data-toggle="modal" data-target="#myModal${componentId}">
			<i class="icon-cog"></i> Settings
		</button>
	</section>
	<section id="status" class="span8 container pull-right">
		<div id="status-notification${componentId}"></div>
	</section>
	<div id="myModal${componentId}" class="modal fade">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<table id="controls">
				<tr>
					<td><label for="mode">Mode</label></td>
					<td><select id="mode" size="1">
							<option value="ace/mode/c_cpp">C++</option>
							<option value="ace/mode/clojure">Clojure</option>
							<option value="ace/mode/coffee">Coffee</option>
							<option value="ace/mode/coldfusion">Coldfusion</option>
							<option value="ace/mode/csharp">Csharp</option>
							<option value="ace/mode/css">CSS</option>
							<option value="ace/mode/groovy">Groovy</option>
							<option value="ace/mode/haxe">Haxe</option>
							<option value="ace/mode/html">HTML</option>
							<option value="ace/mode/java">Java</option>
							<option value="ace/mode/javascript">JavaScript</option>
							<option value="ace/mode/json">Json</option>
							<option value="ace/mode/latex">Latex</option>
							<option value="ace/mode/lua">Lua</option>
							<option value="ace/mode/markdown">Markdown</option>
							<option value="ace/mode/ocaml">Ocaml</option>
							<option value="ace/mode/perl">Perl</option>
							<option value="ace/mode/pgsql">Pgsql</option>
							<option value="ace/mode/php">PHP</option>
							<option value="ace/mode/powershell">PowerShell</option>
							<option value="ace/mode/python" selected="selected">Python</option>
							<option value="ace/mode/ruby">Ruby</option>
							<option value="ace/mode/scad">Scad</option>
							<option value="ace/mode/scala">Scala</option>
							<option value="ace/mode/scss">SCSS</option>
							<option value="ace/mode/sql">SQL</option>
							<option value="ace/mode/svg">SVG</option>
							<option value="ace/mode/textile">Textile</option>
							<option value="ace/mode/xml">XML</option>
					</select></td>
				</tr>
				<tr>
					<td><label for="theme">Theme</label></td>
					<td><select id="theme" size="1">
							<option value="ace/theme/chrome">Chrome</option>
							<option value="ace/theme/clouds">Clouds</option>
							<option value="ace/theme/clouds_midnight">Clouds
								Midnight</option>
							<option value="ace/theme/cobalt">Cobalt</option>
							<option value="ace/theme/crimson_editor">Crimson Editor</option>
							<option value="ace/theme/dawn">Dawn</option>
							<option value="ace/theme/dreamweaver">Dreamweaver</option>
							<option value="ace/theme/eclipse" selected="selected">Eclipse</option>
							<option value="ace/theme/idle_fingers">idleFingers</option>
							<option value="ace/theme/kr_theme">krTheme</option>
							<option value="ace/theme/merbivore">Merbivore</option>
							<option value="ace/theme/merbivore_soft">Merbivore Soft</option>
							<option value="ace/theme/mono_industrial">Mono
								Industrial</option>
							<option value="ace/theme/monokai">Monokai</option>
							<option value="ace/theme/pastel_on_dark">Pastel on dark</option>
							<option value="ace/theme/solarized_dark">Solarized Dark</option>
							<option value="ace/theme/solarized_light">Solarized
								Light</option>
							<option value="ace/theme/textmate">TextMate</option>
							<option value="ace/theme/twilight">Twilight</option>
							<option value="ace/theme/tomorrow">Tomorrow</option>
							<option value="ace/theme/tomorrow_night">Tomorrow Night</option>
							<option value="ace/theme/tomorrow_night_blue">Tomorrow
								Night Blue</option>
							<option value="ace/theme/tomorrow_night_bright">Tomorrow
								Night Bright</option>
							<option value="ace/theme/tomorrow_night_eighties">Tomorrow
								Night 80s</option>
							<option value="ace/theme/vibrant_ink">Vibrant Ink</option>
					</select></td>
				</tr>
				<tr>
					<td><label for="fontsize">Font Size</label></td>
					<td><select id="fontsize" size="1">
							<option value="10px">10px</option>
							<option value="11px">11px</option>
							<option value="12px" selected="selected">12px</option>
							<option value="14px">14px</option>
							<option value="16px">16px</option>
							<option value="20px">20px</option>
							<option value="24px">24px</option>
					</select></td>
				</tr>
				<tr>
					<td><label for="folding">Code Folding</label></td>
					<td><input type="checkbox" name="folding" id="folding" checked>
					</td>
				</tr>
				<tr>
					<td><label for="select_style">Full Line Selection</label></td>
					<td><input type="checkbox" name="select_style"
						id="select_style" checked></td>
				</tr>
				<tr>
					<td><label for="highlight_active">Highlight Active
							Line</label></td>
					<td><input type="checkbox" name="highlight_active"
						id="highlight_active" checked></td>
				</tr>
				<tr>
					<td><label for="show_hidden">Show Invisibles</label></td>
					<td><input type="checkbox" name="show_hidden" id="show_hidden">
					</td>
				</tr>
				<tr>
					<td><label for="enable_behaviours">Enable Behaviours</label></td>
					<td><input type="checkbox" id="enable_behaviours" checked>
					</td>
				</tr>
				<tr>
					<td><label for="highlight_selected_word">Highlight
							selected word</label></td>
					<td><input type="checkbox" id="highlight_selected_word"
						checked></td>
				</tr>
				<!--     <tr> -->
				<!--       <td > -->
				<!--         <label for="show_hscroll">Persistent HScroll</label> -->
				<!--       </td><td> -->
				<!--         <input type="checkbox" name="show_hscroll" id="show_hscroll"> -->
				<!--       </td> -->
				<!--     </tr> -->
				<!--     <tr> -->
				<!--       <td > -->
				<!--         <label for="keybinding">Key Binding</label> -->
				<!--       </td><td> -->
				<!--         <select id="keybinding" size="1"> -->
				<!--           <option value="ace">Ace</option> -->
				<!--           <option value="vim">Vim</option> -->
				<!--           <option value="emacs">Emacs</option> -->
				<!--           <option value="custom">Custom</option> -->
				<!--         </select> -->
				<!--       </td> -->
				<!--     </tr> -->
				<!--     <tr> -->
				<!--       <td > -->
				<!--         <label for="soft_wrap">Soft Wrap</label> -->
				<!--       </td><td> -->
				<!--         <select id="soft_wrap" size="1"> -->
				<!--           <option value="off">Off</option> -->
				<!--           <option value="40">40 Chars</option> -->
				<!--           <option value="80">80 Chars</option> -->
				<!--           <option value="free">Free</option> -->
				<!--         </select> -->
				<!--       </td> -->
				<!--     </tr> -->
				<!--     <tr> -->
				<!--       <td > -->
				<!--         <label for="show_gutter">Show Gutter</label> -->
				<!--       </td><td> -->
				<!--         <input type="checkbox" id="show_gutter" checked> -->
				<!--       </td> -->
				<!--     </tr> -->
				<!--     <tr> -->
				<!--       <td > -->
				<!--         <label for="show_print_margin">Show Print Margin</label> -->
				<!--       </td><td> -->
				<!--         <input type="checkbox" id="show_print_margin" checked> -->
				<!--       </td> -->
				<!--     </tr> -->
				<!--     <tr> -->
				<!--       <td > -->
				<!--         <label for="soft_tab">Use Soft Tab</label> -->
				<!--       </td><td> -->
				<!--         <input type="checkbox" id="soft_tab" checked> -->
				<!--       </td> -->
				<!--     </tr> -->
			</table>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<button class="btn btn-primary"
				onclick="saveSettings${componentId}()" data-dismiss="modal">Save
				settings</button>
		</div>
	</div>
	<script type="text/javascript">
		$('#myModal${componentId}').modal({show:false});
	</script>
</section>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- Cross site title tag -->
<title>Programming Made Simple | SIMPLEx</title>

<!-- Importing jQuery 1.10.2 -->
<script src="/script/jquery-1.10.2.js"></script>

<!-- Importing Bootstrap JavaScript plugins -->
<script src="/bootstrap/js/bootstrap.js"></script>

<!-- Importing bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.css" />

<!-- Importing scripts for tag cloud -->
<script src="/script/tagcanvas.min.js"></script>

<!-- Other imports -->
<script src="/script/initiateAceOnLesson.js"></script>
<script src="/script/jeditable.js"></script>
<script src="/script/persistence.js"></script>
<!-- Import notification -->
<script src="/script/notifications.js"></script>
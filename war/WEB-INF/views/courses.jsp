<%@page import="edu.mit.meet.meetag.entities.Course"%>
<%@page import="java.util.ArrayList"%>
<%@page import="edu.mit.meet.meetag.connectors.DAO"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
<script type="text/javascript">
	window.onload = function() {
		TagCanvas.textColour = '#252525';
		TagCanvas.textHeight = 25;
		TagCanvas.outlineThickness = 0.01;
		TagCanvas.outlineColour = '#FFFFFF';
		TagCanvas.maxSpeed = 0.01;
		TagCanvas.minBrightness = 0.2;
		TagCanvas.depth = 0.25;
		TagCanvas.pulsateTo = 10;
		TagCanvas.initial = [ 0.1, -0.1 ];
		TagCanvas.decel = 0.98;
		TagCanvas.reverse = true;
		TagCanvas.hideTags = false;
		TagCanvas.shadow = '#ccf';
		TagCanvas.shadowBlur = 3;
		TagCanvas.weight = true;
		TagCanvas.weightMode = "size";
		TagCanvas.imageScale = null;
		TagCanvas.zoomMax = 3.0;
		TagCanvas.zoomMin = 1.0;
		try {

			$("#notification-area").hide();
			var comp = document.getElementById("click");
			if (comp.className == "icon-th-list") {
				comp.className = "icon-globe";
			} else {
				comp.className = "icon-th-list";
			}
			page(1);
			document.getElementById("myCanvas").setAttribute("width",
					screen.width);
			document.getElementById("myCanvas").setAttribute("height",
					screen.height - 400);
			TagCanvas.Start('myCanvas', 'tagList');
		} catch (e) {
			alert("Opps! The sphere has been deflated.");
			// something went wrong, hide the canvas container
		}
	};
	function switchView() {
		var comp = document.getElementById("click");
		if (comp.className == "icon-th-list") {
			comp.className = "icon-globe";
		} else {
			comp.className = "icon-th-list";
		}
		$("#myCanvas").slideToggle("slow");
		$("#listView").slideToggle("slow");
	}
	var pages;
	function page(num){
		for(var i = 0;i<100;i++){
			$("#page"+i).hide();
			$("#link"+i).removeClass("active");
		}
		$("#page"+num).show("slow");
		$("#link"+num).addClass("active");
	}
	function load(){
		for(var i = 0;i<100;i++){
			$("#page"+i).hide();
			$("#link"+i).removeClass("active");
		}
		$("#page1").show();
		$("#link1").addClass("active");
	}
</script>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container"
		style="background: transparent;">
		<ul style="margin-left: 90%;">
			<li class="btn" style="display: inline;"
				onclick="switchView();load();"><a id="click"
				class="icon-th-list"></a>
			</li>
		</ul>
	</section>
	<canvas height="800" id="myCanvas"
		style="display: none; background: transparent;">
					 Anything in here will be replaced on browsers that support the canvas element
				</canvas>
	<section class="container" id="listView">
		<section class="container" id="tagList" >
			<%
				DAO dao = DAO.getDAO();
				ArrayList<Course> courses = dao.getAllCourses();
				int i = 1;
				for (Course temp : courses) {
					if (temp.getAvatar() == null)
						temp.setAvatar("/images/logo.jpg");
					if (i % 10 == 1) {
			%><section id="page<%=i%>" style="display: none;">
				<%
					}
				%>
				<section class="well span5" style="max-height: 50px;">
					<table>
						<tr>
							<td valign="top"><img class="thumbnail"
								src=<%=temp.getAvatar()%>
								style="max-width: 50px; max-height: 50px" />
							</td>
							<td valign="top"><a data-weight="<%=temp.getPopularity()%>"
								style="font-size:<%=temp.getPopularity() + 12%>pt; margin-left:10px;"
								href="/course/<%=temp.getId()%>">
								<%if(temp.getName().length()>40){ %>
								<%=temp.getName().substring(0,40)%>... 
								<% }else{%>
								<%=temp.getName() %>
								<%} %>
								</a> <% if(temp.getDescription().length()>101){ %>
								<p style="margin-left: 10px;"><%=temp.getDescription().substring(0, 100)%>...
									<% }else{
									%>
								
								<p style="margin-left: 10px;"><%=temp.getDescription()%>
									<%}%>
								</p>
						</tr>
					</table>
				</section>
				<%
					if (i % 10 == 0 || temp.equals(courses.get(courses.size() - 1))) {
				%>
			</section>
			<%
				}
					i++;
				}
			%>
		</section>
		<section class="pagination" style="padding-left:40%">
			<ul>
							<%
					int n = 1, x = 1;
					for (; n < i; n += 10) {
				%><li id="link<%=n%>" onclick="page(<%=n%>);"><a href="#"><%=x%></a>
							</li>
							<%
					x++;
					}
				%>

						</ul>
		
		</section>
	</section>

	<%@ include file="footer.jsp"%>
</body>
<script type="text/javascript">
	$(document).ready($("#courses").addClass('active'));
</script>
</html>
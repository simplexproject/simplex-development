<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
<style>
@import "css/wizard.css";
</style>
</head>
<body style="background: whiteSmoke;">
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container">
		<h1>
			SIMPLE<sub>x</sub> Creator
		</h1>
	</section>
	<section class="container">
		<h2>Create A Lesson</h2>
		<section class="well" style="padding-bottom: 0;">
			<section class="hero-unit" id="add" data-toggle="modal"
				data-target="#myModal">
				<img src="/images/plus.png" style="height: 80px; width: 80px;">
			</section>
		</section>
	</section>
	<!-- Modal -->
	<section class="modal fade" id="myModal">
		<section class="modal-header">
			<button type="button" class="close" data-dismiss="modal">x</button>
			<h3 id="myModalLabel">Modal Header</h3>
		</section>
		<section class="modal-body">
			<form class="pull-left" action="/creator/createCourse"
				id="addCourseForm">
				<input id="cNaI" type="text" name="cName" placeholder="Course Name" />
				<br />
				<textarea id="cDesI" name="cDesc" placeholder="Course Description"
					rows="3"></textarea>
				<br /> <input id="cAvI" type="text" name="cAvatar"
					placeholder="Course Avatar (paste link here)" />
			</form>
			<section id="help-info" class="span2 label label-info"></section>
		</section>
		<section class="modal-footer">
			<button class="btn" data-dismiss="modal">Cancel</button>
			<a id="addButton" rel="popover" data-original-title="Preview"
				class="btn btn-primary submit" data-dismiss="modal">Add</a>
		</section>
	</section>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#myModal").modal({
				show : false
			});
		});
	</script>
	<%@ include file="footer.jsp"%>
</body>
<script type="text/javascript">
	$("#headerPadding").hide();
</script>
</html>
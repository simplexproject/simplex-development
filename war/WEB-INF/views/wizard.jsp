<%@page import="edu.mit.meet.meetag.entities.Course"%>
<%@page import="java.util.ArrayList"%>
<%@page import="edu.mit.meet.meetag.connectors.DAO"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
<style>
@import "css/popover.css";

@import "css/wizard.css";
</style>
</head>
<body style="background: whiteSmoke;">
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->
	<section class="container">
		<h1>
			SIMPLE<sub>x</sub> Creator
		</h1>
	</section>
	<section id="courseCreatorContainer">
		<%@include file="courseCreator.jsp"%>
	</section>
	<%@ include file="footer.jsp"%>
</body>
<script type="text/javascript">
	$("#headerPadding").hide();
	$(document).ready(initializePopovers);
</script>
</html>
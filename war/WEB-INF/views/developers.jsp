<legend>Who Are We?</legend>
<section class="span12">
	<ul class="thumbnails">
		<li class="span3"><a href="https://www.facebook.com/geokhoury"
			target="_blank" rel="popover"
			data-original-title="George Khoury - CEO"
			data-content='<section class="bio">Being a co-founder, George is busy setting the vision for SIMPLE<sub>x</sub> and setting its milestones. George is a dedicated MEET alumnus who has served as a Y1 and Y2 mentor in the yearlong program for the past three years. George is currently an undergraduate student at the German-Jordanian University, where he intends to pursue a degree in Computer Engineering. Outside SIMPLE<sub>x</sub>, George loves reading books, listening to music, learning and teaching languages and computer programming. He also loves taking part in activities that have a positive impact on the world.</section><legend/><section class="quoteTitle"><img src="/images/glyphicons/png/quotations.png" class="quotationMarks"> Favourite Quote</section><strong class="quote">"Stay hungy, stay foolish." - Steve Jobs</strong>'><span
				class="devName">George Khoury</span><img class="devImg"
				src="images/developers/GeorgeKhoury.jpg" alt=""></a></li>
		<li class="span3"><a href="https://www.facebook.com/Sadek.Jabr"
			target="_blank" rel="popover" data-original-title="Sadek Jabr - DBA"
			data-content='<section class="bio">Sadek Jabr is a highly motivated MEET Yearlong Mentor, and for the second year, he will be teaching a highly demanding curriculum at MEET. In SIMPLE<sub>x</sub>, Sadek takes a leading role in designing and developing the front-end interface. Sadek is currently in his third year of Computer Science and Business studies at Birzeit University. In his free time, Sadek also runs a small software development business.</section><legend/><section class="quoteTitle"><img src="/images/glyphicons/png/quotations.png" class="quotationMarks"> Favourite Quote</section><strong class="quote">"Be the change you want to see in the world" - Ghandi</strong>'><span
				class="devName">Sadek Jabr</span><img class="devImg"
				src="images/developers/SadekJabr.jpg" alt=""></a></li>
		<li class="span3"><a
			href="https://www.facebook.com/Shawish.Ahmed.M" target="_blank"
			rel="popover" data-original-title="Ahmed Shawish - QA Manager"
			data-content='<section class="bio">In addition to being responsible of enforcing correct coding conventions and high coding standards as the QA Manager at SIMPLE<sub>x</sub>, Ahmed also develops here and there. Ahmed is a MEET Alumnus from the class of 2009 who has been actively engaged with the MEET network since graduating, and he currently volunteers as a teaching assistant in the 2012 yearlong program. Ahmed refuses to hopelessly accept the reality, but instead believes that he can make a change in the world. And as a fresh high school graduate, Ahmed intends to pursue his studies at the Hebrew University majoring in Computer Science.</section><legend/><section class="quoteTitle"><img src="/images/glyphicons/png/quotations.png" class="quotationMarks"> Favourite Quote</section><strong class="quote">"I would rather be a comma than a full-stop." - Coldplay</strong>'
			data-placement="left"><span class="devName">Ahmed Shawish</span><img
				class="devImg" src="images/developers/AhmedShawish.jpg" alt=""></a></li>
		<li class="span3"><a href="https://www.facebook.com/matan.mates"
			target="_blank" rel="popover"
			data-original-title="Matan Mates - Co-CTO"
			data-content='<section class="bio">Serving as CTO, Matan devotes a large portion of his time to SIMPLE<sub>x</sub> by planning and directing development, giving technical input and maintaining progress. Matan is a MEET alumnus of class 2009 who had just graduated from the Hebrew University Secondary School. During his studies he participated in various competitions and projects, FIRST FRC (First Robotics Competition), the IMUN (Israel Model United Nations) and a research project in collaboration with the Weizmann Institute. Matan enjoys Hiking and disintegrating things to figure out how they work.</section><legend/><section class="quoteTitle"><img src="/images/glyphicons/png/quotations.png" class="quotationMarks"> Favourite Quote</section><strong class="quote">"It is absurd to divide people into good or bad. People are either charming or tedious." - Oscar wilde</strong>'
			data-placement="left"><span class="devName">Matan Mates</span><img
				class="devImg" src="images/developers/MatanMates.jpg" alt=""></a></li>
		<li class="span3"><a href="https://www.facebook.com/david0673"
			target="_blank" rel="popover"
			data-original-title="David Heller - Co-CTO"
			data-content='<section class="bio">David, as Co-CTO, mostly works on keeping the process of development up and running. David is currently undergoing his third out of five years of military service in the IDF, where he serves as a Software Engineer. David, just like the rest of the group of founders, is a MEET alumnus from the class of 2007. David has gained his experience in computer science through self-study, MEET, Ness Technologies and the IDF software engineering training program. David also loves music, dancing and bartending. He plans to pursue a degree at MIT and give back to the community after he finishes his military service.</section><legend/><section class="quoteTitle"><img src="/images/glyphicons/png/quotations.png" class="quotationMarks"> Favourite Quote</section><strong class="quote">"Anyone who has never made a mistake has never tried anything new." - Albert Einstein.</strong>'><span
				class="devName">David Heller</span><img class="devImg"
				src="images/developers/DavidHeller.jpg" alt=""></a></li>
		<li class="span3"><a
			href="https://www.facebook.com/leemor.zucker" target="_blank"
			rel="popover" data-original-title="Leemor Zucker - CFO"
			data-content='<section class="bio">Leemor is handling the financial aspect of SIMPLE<sub>x</sub>, working closely with George on PR and marketing. Leemor is currently an undergraduate student at Tel Aviv University, where she is hoping to pursue a degree in brain science. As a MEET Alumna, Leemor is an active member in the MEET Alumni Network who has worked and volunteered at MEET for the past four years. Leemor enjoys reading books, watching movies and listening to music of various genres. Leemor is passionate about self learning and self expanding to broaden the borders of self-awareness and consciousness in order to grow towards a better world.</section><legend/><section class="quoteTitle"><img src="/images/glyphicons/png/quotations.png" class="quotationMarks"> Favourite Quote</section><strong class="quote">Loading...</strong>'><span
				class="devName">Leemor Zucker</span><img class="devImg"
				src="images/developers/LeemorZucker.jpg" alt=""></a></li>
		<li class="span3"><a href="https://www.facebook.com/chani1995"
			target="_blank" rel="popover"
			data-original-title="Ran Czaczkes - Developer"
			data-content='<section class="bio">Ran is a highly motivated developer at SIMPLE<sub>x</sub>. Ran is currently volunteering for the Social Service year in Jerusalem, and as a mentor, Ran works with youth at risk. Ran is a devoted MEET alumnus and intends to contribute as much as he could for the benefit of his community. Ran has a well-rounded personality, and through self-study, MEET and school studies, has gained very valuable skills such as 3D modeling and programming. Ran loves music, and he has been playing the guitar for over four years. Ran will be starting his military service soon, and after he is done with it, he plans to apply to MIT after to pursue a degree in Computer Science.</section><legend/><section class="quoteTitle"><img src="/images/glyphicons/png/quotations.png" class="quotationMarks"> Favourite Quote</section><strong class="quote">"Knowledge speaks, but wisdom listens." - Jimi Hendrix</strong>'
			data-placement="left"><span class="devName">Ran Czaczkes</span><img
				class="devImg" src="images/developers/RanCzaczkes.jpg" alt=""></a></li>
		<li class="span3"><a href="https://www.facebook.com/yuvalyogev"
			target="_blank" rel="popover"
			data-original-title="Yuval Yogev - Intern Developer"
			data-content='<section class="bio">Yuval Yogev is an intern developer at SIMPLE<sub>x</sub> who helps develop in several areas of the project, and he is also working on an image processing project. Yuval is a MEET Alumnus from the class of 2010, and he is both a high school student and a University student where he is currently taking first degree Computer Science courses in the Hebrew University which he will be credited for after finishing his military service.</section><legend/><section class="quoteTitle"><img src="/images/glyphicons/png/quotations.png" class="quotationMarks"> Favourite Quote</section><strong class="quote">"Think it over, think it under." - Winnie the Pooh</strong>'
			data-placement="left"><span class="devName">Yuval Yogev</span><img
				class="devImg" src="images/developers/YuvalYogev.jpg" alt=""></a></li>
	</ul>
</section>
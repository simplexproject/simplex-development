<section id="editor${componentId }" class="editor"></section>
<style>
.editor {
	position: relative;
	width: 100%;
	height: 400px;
}
</style>
<script src="/script/LoadACE.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	var editor${componentId} = ace.edit("editor${componentId}");
	function sendCompilationReuqest${componentId}() {
		var code = editor${componentId}.getSession().getValue();
		var lan = "Python";
		$
				.ajax({
					type : "POST",
					url : "http://localhost:8888/compiler/compile",
					data : {
						"code" : code,
						"language" : lan,
					},
					beforeSend : function() {
						$("#run-icon").attr("class", "icon-refresh");
						$("#status-notification${componentId}").attr("class",
								"alert alert-info fade in");
						$("#status-notification${componentId}").text(
								"Hold on, we're working ...");
					},
					success : function(data) {
						var responseObject = $.parseJSON(data);
						$("#run-icon").attr("class", "icon-play-circle");
						alert(data);
						$("#status-notification${componentId}").text(
								"Compiled and ran successfully! Output: " + responseObject.programOutput);
						
						if(responseObject.compiledSuccessfully == true) {
							if(responseObject.ranSuccessfully == true) {
								$("#status-notification${componentId}").attr("class",
										"alert alert-success");
								$("#status-notification${componentId}").text(
										"Compiled and ran successfully! Output: " + responseObject.programOutput);
							} else {
								$("#status-notification${componentId}").attr("class",
										"alert alert-danger");
								$("#status-notification${componentId}").text(
										"Error: " + responseObject.programErrors);
							}
						} else {
							$("#status-notification${componentId}").attr("class",
									"alert alert-danger");
							$("#status-notification${componentId}").text(
									"Error: Your code did not compile successfully. <br /> Error:" + responseObject.programErrors);
						}
					},
					error : function() {
						$("#run-icon").attr("class", "icon-warning-sign");
						$("#status-notification${componentId}").attr("class",
								"alert alert-block alert-error fade in");
						$("#status-notification").text(
								"Connection error, please try again later.");

					}
				});
	}
	editor${componentId}.setTheme("ace/theme/eclipse");
	var Lang = require("ace/mode/python").Mode;
	editor${componentId}.getSession().setMode(new Lang());
	editor${componentId}.insert("#Welcome to SIMPLEx\n");
    editor${componentId}.setHighlightSelectedWord(true);
</script>
<%@page import="edu.mit.meet.meetag.entities.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="edu.mit.meet.meetag.connectors.DAO"%>


<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Email</th>
			<th>Name</th>
			<th class="span2">Role</th>
			<th>Account Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		<%
			int currentUser = 1;
			String email, first_name, last_name, role;
			ArrayList<User> users = DAO.getDAO().getUsers(0, 10);
			for (User user : users) {
				email = currentUser + " email";
				first_name = currentUser + " first_name";
				last_name = currentUser + " last_name";
				role = currentUser + " role";
		%>
		<tr id="<%=user.getEmail()%>">
			<td><%=currentUser%></td>
			<td>

				<p id="<%=email%>" style="display: inline"><%=user.getEmail()%></p>
			</td>
			<td>
				<p class="click" id="<%=first_name%>" style="display: inline"><%=user.getFirstName()%></p>
				<p class="click" id="<%=last_name%>" style="display: inline"><%=user.getLastName()%></p>
			</td>
			<td><span class="editable_select inherit" id="<%=role%>"
				style="display: inline; max-width: 5px;"><%=user.getRole()%></span>

			</td>
			<td>
				<%
					if (user.isVerified()) {
				%> <span class="badge badge-success">Verified</span> <%
 	} else {
 %> <span class="badge badge-warning">Not Verified</span> <%
 	}
 %>
			</td>
			<td><a href="#"><i class="icon-trash"
					onclick="deleteUser('<%=user.getEmail()%>')"></i></a></td>
		</tr>
		<%
			currentUser++;
			}
		%>
	</tbody>
</table>
<section class="pagination">
	<ul>
		<li><a href="#">Prev</a></li>
		<li><a href="#">Next</a></li>
	</ul>
</section>

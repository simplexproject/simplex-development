<%@page import="edu.mit.meet.meetag.entities.*"%>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<!-- Including all head imports from head.jsp -->
<%@ include file="head.jsp"%>
</head>
<body>
	<!-- Including header which has the navBar -->
	<%@ include file="header.jsp"%>
	<!-- #End header -->

	<section class="container">
		<table>
			<tr>
				<td valign="top">
					<div class="span3">
						<ul class="nav nav-list affix">
							<li class="nav-header">List header</li>
							<li><a href="#"><i class="icon-list"></i> Feed</a></li>
							<li class="active"><a href="courses"> <i
									class="icon-book"></i>Current Courses
							</a></li>
							<li class="divider"></li>
							<li><a href="profile">Profile</a></li>
							<li><a href="#">Settings</a></li>
							<li class="divider"></li>
							<li><a href="help">Help</a></li>
						</ul>
					</div>
				</td>
				<td>
					<section class="span12">
						<ul class="thumbnails">

							<%
								List<Long> enrolledIds = ((User) (session.getAttribute("User")))
																																								.getEnrolledCourses();
																																						ArrayList<Course> enrolledCourses = new ArrayList<Course>();

																																						for (int i = 0; i < enrolledIds.size(); i++) {
																																							DAO dao = DAO.getDAO();
																																							Course course = dao.getCourse(enrolledIds.get(i));
																																							enrolledCourses.add(course);
																																						}

																																						if (enrolledCourses.size() > 0) {
																																							for (Course course : enrolledCourses) {
							%>

							<li class="span2"><a href="/course/<%=course.getId()%>"
								target="_parent" rel="popover"
								data-original-title=<%=course.getName()%>
								data-content='<section class="courseDescription"><%=course.getDescription()%></section>'><span
									class="courseName"><%=course.getName()%></span><img
									class="courseAvatar" src=<%=course.getAvatar()%> alt=""></a></li>

							<%
								}
							%>
						</ul>

					</section> <%
 	} else {
 %>
					<h1>You are not enrolled in any courses.</h1> <%
 	}
 %>
				</td>
			</tr>
		</table>
	</section>
	<%@ include file="footer.jsp"%>

</body>
<script type="text/javascript" src="/script/popover.js"></script>
<script type="text/javascript">
	$(document).ready(initializePopovers);
</script>
</html>

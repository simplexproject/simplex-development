## Welcome to the SIMPLEx Code Repository!

###### Please note that this repository is **private** and any unauthorized access or usage is strictly **prohibited**.


To get started

* Include all of these libraries in your build path 

> https://bitbucket.org/simplexproject/simplex-development/src/e057d15824e39de6354568a61d71d57a4e4c0c40/war/WEB-INF/lib?at=development

* Make sure you add
	* JDK 1.6+
	* Google App Engine SDK 1.8.x+

* Make sure your **Default Output Folder** is set to

> **[PROJECT BASE]**/war/WEB-INF/classes

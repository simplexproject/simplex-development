/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.enumurations;

public enum SiteStatus {
	Online(1), Offline(0), Maintenance(2), Default(-1),Beta(3), Testing(4);
	public static final String SITE_STATUS_KEY = "SITE_STATUS_KEY";
	int n;

	private SiteStatus(int n) {
		this.n = n;
	}

	public int getId() {
		return n;
	}

}

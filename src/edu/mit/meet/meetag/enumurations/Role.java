/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.enumurations;

import com.googlecode.objectify.annotation.Entity;

@Entity(name = "Role")
public enum Role {
	Banned(0), Guest(1), User(2), Contributor(3), Administrator(4);

	private int level;

	Role() {
		this.level = 1;
	}

	Role(int level) {
		this.level = level;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * 
	 * @param level
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * 
	 * @param role
	 * @return
	 */

	public boolean allowedAccess(Role role) {

		if (role.getLevel() > this.level) {
			return false;
		} else {
			return true;
		}
	}

}

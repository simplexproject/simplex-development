/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mit.meet.meetag.connectors.IDEoneInitializer;

/**
 * This servlet will handle the data passed from the editor and then creates a
 * submission and sends it to IDEone.
 * 
 * @version 0.1
 * @author gkhoury
 */
@SuppressWarnings({ "unused", "serial" })
public class Compile extends HttpServlet {

	private String editor;
	private String mode;
	private String theme;
	private String gutter;
	private String fontSize;
	private String softWrap;
	private String showPrintMargin;
	private String useSoftTabs;
	private String showInvisibles;
	private IDEoneInitializer ideConnector = new IDEoneInitializer();

	/**
	 * doGet to handle get HTTP requests
	 * 
	 * @param req
	 *            - HttpSerlvetRequest
	 * @param resp
	 *            - HttpServletResponse
	 * 
	 */

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		// Reading request parameters and assigning them to the fields
		editor = req.getParameter("editor");
		mode = req.getParameter("mode");
		theme = req.getParameter("theme");
		gutter = req.getParameter("gutter");
		fontSize = req.getParameter("fontSize");
		softWrap = req.getParameter("softWrap");
		showPrintMargin = req.getParameter("showPrintMargin");
		useSoftTabs = req.getParameter("useSoftTabs");
		showInvisibles = req.getParameter("showInvisibles");

		// Initialize the IDEone environment

		ideConnector.initialize();
		Map<String, String> submissionResponse = ideConnector.createSunmission(
				editor,ideConnector.getLanguagesCode(mode), "", true, true);
		System.out.println(submissionResponse);
		String link = submissionResponse.get("link");
		System.out.println("Link = " + link);

		resp.sendRedirect("http://ideone.com/" + link);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		doGet(req, resp);
	}
}
/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.entities.Lesson;

public class AddLesson extends HttpServlet {

	/**
	 * A default serialize version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * A doGet implementation that adds the to the DB. Will forward to login.jsp
	 * 
	 * @param request
	 *            the request for adding this user
	 * @param response
	 *            the response after adding this user
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String name = request.getParameter("name");
		String description = request.getParameter("description");

		Lesson l = new Lesson((long) Math.floor((Math.random() * 100000000)),
				name, description);
		HttpSession session = request.getSession(true);
		DAO d = DAO.getDAO();
		d.addAndUpdateLesson(l);
		session.setAttribute("Lesson", l.toString());
	}

	/**
	 * Calls the doGet method * @param request the request for adding this user
	 * 
	 * @param response
	 *            the response after adding this user
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}

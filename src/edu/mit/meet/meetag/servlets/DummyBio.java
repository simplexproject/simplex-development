/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.servlets;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DummyBio {

	/**
	 * A doGet implementation that adds the to the DB. Will forward to login.jsp
	 * 
	 * @param request
	 *            the request for adding this user
	 * @param response
	 *            the response after adding this user
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String json = "{\"id\": \"1328439985\", \"name\": \"Zohar Shevach\", \"first_name\": \"Zohar\", \"last_name\": \"Shevach\"}";
		System.out.println("Recieved GET");
		Writer write = response.getWriter();
		write.write(json);

	}

	/**
	 * Calls the doGet method * @param request the request for adding this user
	 * 
	 * @param response
	 *            the response after adding this user
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}

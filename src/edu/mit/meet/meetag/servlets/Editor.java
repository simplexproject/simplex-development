/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@SuppressWarnings("serial")
public class Editor extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String editor = req.getParameter("editor");
		String mode = req.getParameter("mode");
		String theme = req.getParameter("theme");
		String gutter = req.getParameter("gutter");
		String fontSize = req.getParameter("fontSize");
		String softWrap = req.getParameter("softWrap");
		String showPrintMargin = req.getParameter("showPrintMargin");
		String useSoftTabs = req.getParameter("useSoftTabs");
		String showInvisibles = req.getParameter("showInvisibles");
		req.setAttribute("editor",editor);
		req.setAttribute("mode", mode);
		req.setAttribute("theme", theme);
		req.setAttribute("gutter", gutter);
		req.setAttribute("fontSize", fontSize);
		req.setAttribute("softWrap", softWrap);
		req.setAttribute("showPrintMargin", showPrintMargin);
		req.setAttribute("useSoftTabs", useSoftTabs);
		req.setAttribute("showInvisibles", showInvisibles);
		RequestDispatcher requestDispatcher = req
				.getRequestDispatcher("/editor-submitted.jsp");

		try {
			requestDispatcher.forward(req, resp);
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doGet(req, resp);
	}
}

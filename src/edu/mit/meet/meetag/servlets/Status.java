/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.servlets;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class Status extends HttpServlet {

	private final String url = "http://ideone.com/api/1/service.wsdl"; // API access URL

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		//Trying to connecto to the IDEOne API
		try {
			final URLConnection connection = new URL(url).openConnection();
			connection.connect(); // Upon succesfful connection
			System.out.println("Service " + url + " available, yeah!");

			resp.sendRedirect("/status.jsp");

		} catch (final IOException e) {
			
			//When service is unavailable
			System.out.println("Service " + url + " unavailable, oh no!");
			resp.sendRedirect("/503.jsp"); //Redirect user to 503 page
		}

	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet(req, resp);
	}

}

/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.connectors.EmailSender;
import edu.mit.meet.meetag.entities.User;

@Controller
@RequestMapping(value = "/signup")
public class SignupController {


	@RequestMapping(value = "/regular", method = RequestMethod.POST)
	public ModelAndView signup(@RequestParam String email, @RequestParam String password, 
			@RequestParam String passwordConf, @RequestParam String firstName,
			@RequestParam String lastName, HttpServletRequest request) {
		
		boolean hasBetaInvite = BetaController.hasBetaInvite(email);
		
		if (hasBetaInvite){
			User user = CreateUser(email, password, passwordConf, firstName, lastName);
			
			if ((user.getPassword() != null) && (password.equals(passwordConf))) {
				DAO dao = DAO.getDAO();
				dao.addAndUpdateUser(user);
				System.out.println("Validation Code:" + User.getVerificationKey(user.getEmail()));
				
				EmailSender.sendVerificationEmail("no-reply@thesimplex.co", email, firstName, lastName,
						"http://thesimplexdev.appspot.com/signup/verify?email=" + user.getEmail() + "&key="
						+ User.getVerificationKey(user.getEmail()));

				ModelAndView mav = new ModelAndView();
				mav.addObject("notification",
						"Please verify your account to sign in!");
				mav.addObject("notification-type", "info");
				RedirectView rv = new RedirectView("/login", false);
				mav.setView(rv);
				return mav;
			
			} else {
				ModelAndView mav = new ModelAndView();
				RedirectView rv = new RedirectView("/404", false);
				mav.setView(rv);
				return mav;
			}
		}
		else
		{
			return new ModelAndView(new RedirectView("/invite", false));
		}
	}


	@RequestMapping(value = "/verify", method = RequestMethod.GET)
	public ModelAndView verify(@RequestParam String email, @RequestParam String key) {
		DAO dao = DAO.getDAO();
		User user = dao.getUser(email);
		if (user == null) {
			return new ModelAndView(new RedirectView("/404", false));
		} else {
			if (user.isVerified()) {
				ModelAndView mav = new ModelAndView();
				mav.addObject("notification", "Your user has already been activated.");
				mav.addObject("notification-type", "info");
				RedirectView rv = new RedirectView("/login", false);
				mav.setView(rv);
				return mav;
			} else {
				if (User.getVerificationKey(email).equals(key)) {
					System.out.println("Verified user at /verify in signupController");
					user.activateUser();
					dao.addAndUpdateUser(user);
					ModelAndView mav = new ModelAndView();
					mav.addObject("notification", "Thank you, your account has been activated.");
					mav.addObject("notification-type", "success");
					RedirectView rv = new RedirectView("/login", false);
					mav.setView(rv);
					return mav;
				}

				return new ModelAndView(new RedirectView("/404", false));
			}
		}

	}
	
	public User CreateUser(String email, String password, String passwordConf, String firstName, String lastName)
	{
		User user = new User();
		
		user.setEmail(email.toLowerCase());
		user.setPassword(User.getEncryptedPassword(password,
				user.getEmail()));
		System.out.println("Password set to " + user.getPassword());
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAvatar("/images/mrX.jpeg");
		user.setRegularLogin(true);
		
		return user;
	}
}
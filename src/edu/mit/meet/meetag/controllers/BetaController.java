/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.api.client.http.LowLevelHttpRequest;
import com.google.api.client.http.LowLevelHttpResponse;
import com.google.api.client.http.javanet.NetHttpTransport;

@Controller
@RequestMapping(value = "/beta")
public class BetaController {
	
	
	/**
	 * Write the request to the Google servers.
	 * 
	 * @param String url
	 *            a String that represent the request
	 */
	
	private static ArrayList<String> request(String url) {
		NetHttpTransport aht = new NetHttpTransport();
	
		LowLevelHttpRequest req;
		try {
			req = aht.buildGetRequest(url);
			req.setTimeout(3000,2500);
			
			try {
				LowLevelHttpResponse res = req.execute();
				return getResponse(res.getContent());
			} catch (IOException e) {
				System.err.println("IOException in setting request content.");
				e.printStackTrace();
			}
		} catch (IOException e1) {
			System.err
					.println("IOException when trying to create the connection to the Google servers.");
			e1.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Reading the response from the server. If there is no response - returns
	 * null.
	 * 
	 * @return ArrayList<String> where each element is a line in the response of
	 *         the xml. null if you can't read the response and an empty
	 *         ArrayList if there is no response
	 */
	
	private static ArrayList<String> getResponse(InputStream in) {
		// Reading from the stream
		BufferedReader bReader = null;
		try {
			bReader = new BufferedReader(new InputStreamReader(in));
			ArrayList<String> response = new ArrayList<String>();
			String line = bReader.readLine();
			while (line != null) {
				response.add(line);
				line = bReader.readLine();
			}
			return response;
		} catch (IOException e) {
			System.err.println("Couldn't read the response properly");
			e.printStackTrace();
		} finally {
			// This part is in order to close the buffer properly
			if (bReader != null) {
				try {
					bReader.close();
				} catch (IOException e) {
					System.err.println("Couldn't close writer");
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	/**
	 * This method sends a request using the 'request' method to the Google servers, specifically to Google Drive 
	 * and fetches the spreadsheet under the knowledge base collection -- 'SIMPLEx Beta Testers', then parses it and
	 * adds it to and ArrayList<String>
	 * 
	 * @return ArrayList<String> -- The list of emails with beta invites.
	 */
	
	public static ArrayList<String> getBetaTestersList(){
		return request("https://docs.google.com/spreadsheet/pub?key=0Aj7_R19nKlCYdFdRdFJsd1pxZFdpY2h2Z2xiMnRQREE&single=true&gid=0&output=txt");
	}
	
	/**
	 * This method checks if a specific user has a beta invite. It loads the 'Beta Testers List' and checks if
	 * the 'email' exists. 
	 * 
	 * @param email -- The user's email.
	 * @return boolean - true if the user has a beta invite, false if otherwise.
	 */
	
	public static boolean hasBetaInvite(String email) {
		ArrayList<String> betaTesters = BetaController.getBetaTestersList();
		return betaTesters.contains(email);
	}
	
	/**
	 * This method verifies the user with email if the provided key is valid. This step is *required* before 
	 * a beta user can log-in to the system.
	 * 
	 * @param key -- A SHA-256 encrypted key.
	 * @param email -- The user email.
	 * @return returns true if the provided email and key match.
	 */
	
	@RequestMapping(value = "/verify/{key}/{email}")
	public boolean verify(@RequestParam String key, @RequestParam String email){
		return getBetaVerificationKey(email).equals(key);
	}
	
	/**
	 * This method encrypts the user's email into a SHA-256, with the "beta:" appended
	 * to the beginning of the email. This key is used to verify the validity of the invite.
	 * 
	 * @param String email -- The user's email
	 * @return String -- The hashed email
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	
	public static String getBetaVerificationKey(String email) {
		email = "beta:" + email;
		
		try {
			MessageDigest m = MessageDigest.getInstance("SHA-256");

			m.update(email.getBytes(), 0, email.length());

			String key = new BigInteger(1, m.digest()).toString(16);

			return key;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;

		}
	}
}

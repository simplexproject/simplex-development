/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

import com.google.gson.Gson;

import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.entities.FacebookUser;
import edu.mit.meet.meetag.entities.GoogleUser;
import edu.mit.meet.meetag.entities.TwitterUser;
import edu.mit.meet.meetag.entities.User;
import edu.mit.meet.meetag.enumurations.Role;

@Controller
@RequestMapping(value = "/login")
public class LoginController
{
	@RequestMapping(value = "/regular", method = RequestMethod.POST)
	public @ResponseBody String ajaxLogin(@RequestParam String user,@RequestParam String pass, HttpServletRequest request)
	{
		User u = DAO.getDAO().validateUser(user, pass);

		if(u != null && u.isVerified())
		{
			request.getSession(true).setAttribute("User", user);
			return (new Gson()).toJson(Boolean.TRUE);
		}

		return (new Gson()).toJson(Boolean.FALSE);
	}
	
	// TODO: Make sure redirect is coming from Facebook, otherwise someone can just forge a POST with any JSON Object.
	@RequestMapping(value = "/facebook")
	public RedirectView LoginFaceook(@RequestParam String code, HttpServletRequest request)
	{
		if (code != null)
		{
			DAO dao = DAO.getDAO();
			FacebookUser facebookUser = FacebookUser.getFacebookUser(FacebookUser.getAccessToken(code, request.getLocalName()));
			String email = facebookUser.getEmail();
			
			ArrayList<String> simplexDevelopersFacebookEmail = new ArrayList<String>();
			simplexDevelopersFacebookEmail.add("dh0673@gmail.com");
			simplexDevelopersFacebookEmail.add("mtn.mates@gmail.com");
			simplexDevelopersFacebookEmail.add("e.ahmed.shawish@gmail.com");
			simplexDevelopersFacebookEmail.add("ranczazckes@gmail.com");
			simplexDevelopersFacebookEmail.add("yogevyuval@gmail.com");
			simplexDevelopersFacebookEmail.add("georgekhry@gmail.com");
			simplexDevelopersFacebookEmail.add("sadoo2aa@gmail.com");
			simplexDevelopersFacebookEmail.add("");
			
			if(email != null)
			{
				if (BetaController.hasBetaInvite(email) || email.toLowerCase().contains("@thesimplex.co") || simplexDevelopersFacebookEmail.contains(email))
				{
					User user = dao.getUser(email);
					if (user == null)
					{
						user = CreateUserFromFacebook(facebookUser);
						dao.addAndUpdateUser(user);
					}
	
					request.getSession(true).setAttribute("User", user);
				}
			}
		}

		return new RedirectView("/home", false);
	}

	@RequestMapping(value = "/google")
	public RedirectView LoginGoogle(@RequestParam String code, HttpServletRequest request)
	{
		if (code != null)
		{
			DAO dao = DAO.getDAO();
			GoogleUser googleUser = GoogleUser.getGoogleUser(GoogleUser.getAccessToken(code));
			String email = googleUser.getEmail();

			if(BetaController.hasBetaInvite(email) || email.toLowerCase().contains("@thesimplex.co"))
			{
				User user = dao.getUser(email);
				
				if (user == null)
				{
					user = CreateUserFromGoogle(googleUser);
					dao.addAndUpdateUser(user);
				}
				if (user != null && user.isVerified())
				{
					request.getSession(true).setAttribute("User", user);
				}
			}
		}

		return new RedirectView("/home", false);
	}

	@RequestMapping(value = "/twitter")
	public RedirectView LoginTwitter(HttpServletRequest request)
	{
		String oauthConsumerKey;
		String oauthConsumerSecret;
		Boolean isLocal = request.getLocalName().equals("127.0.0.1");
		System.out.println("isLocal: " + isLocal);
		
		if(isLocal)
		{
			oauthConsumerKey = "67JhIrZGdWqIFbTv2r2MUg";
			oauthConsumerSecret = "shpTW0KbHuvwbAX11uUJf9GviO0NjChXuUEVyfSYGvc";
		}
		else
		{
			oauthConsumerKey = "tatX4i5XbE7A8LoNpVsg";
			oauthConsumerSecret = "ZodkUBlYJka6ylPSdocWBdspla7Nztv4xIuo3lU0PU";	
		}
		
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true);
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		twitter.setOAuthConsumer(oauthConsumerKey,oauthConsumerSecret);
        request.getSession().setAttribute("twitter", twitter);
        
        try
        {
            StringBuffer callbackURL = request.getRequestURL();
            int index = callbackURL.lastIndexOf("/");
            callbackURL.replace(index, callbackURL.length(), "").append("/twitterCallback");

            RequestToken requestToken = twitter.getOAuthRequestToken(callbackURL.toString());
            request.getSession().setAttribute("requestToken", requestToken);
    		return new RedirectView(requestToken.getAuthenticationURL(),false);
        }
        catch (TwitterException e)
        {
            System.out.println(e.getMessage());
        }
        
		return new RedirectView("/home", false);
	}
	
	@RequestMapping(value = "/twitterCallback")
	public RedirectView LoginTwitterCallBack(HttpServletRequest request)
	{
		Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
        RequestToken requestToken = (RequestToken) request.getSession().getAttribute("requestToken");
        String verifier = request.getParameter("oauth_verifier");
        try {
            AccessToken token = twitter.getOAuthAccessToken(requestToken, verifier);
            twitter4j.User tUser = twitter.showUser(token.getUserId());
            
            User user = new User();
            String[] names = tUser.getName().split(" ");
            user.setFirstName(names[0]);
            user.setLastName(names[1]);
            user.setAvatar(tUser.getProfileImageURL().toString());
            request.getSession(false).setAttribute("User", user);

            request.getSession().removeAttribute("requestToken");
        } catch (TwitterException e) {
            System.out.println(e.getMessage());
        }
		return new RedirectView("/home", false);
	}

	@RequestMapping(value = "/logout")
	public ModelAndView Logout(HttpServletRequest request)
	{
		request.getSession().invalidate();
		ModelAndView mav = new ModelAndView();
		mav.setView(new RedirectView("/", false));

		return mav;
	}

	public User CreateUserFromFacebook(FacebookUser facebookUser)
	{
		User user = new User();

		user.setEmail(facebookUser.getEmail());
		user.setFirstName(facebookUser.getFirst_name());
		user.setLastName(facebookUser.getLast_name());
		user.setBirthdate(facebookUser.getBirthday());
		user.setGender(facebookUser.getGender());
		user.setAvatar("http://graph.facebook.com/" + facebookUser.getId() + "/picture");
		System.out.println(user.getAvatar() + " AVATAR");
		user.activateUser();
		
		if(user.getEmail().toLowerCase().contains("@thesimplex.co"))
		{
			user.setRole(Role.Administrator);
		}
		
		return user;
	}

	public User CreateUserFromGoogle(GoogleUser googleUser)
	{
		User user = new User();

		user.setEmail(googleUser.getEmail());
		user.setFirstName(googleUser.getGiven_name());
		user.setLastName(googleUser.getFamily_name());
		user.setGender(googleUser.getGender());
		user.setAvatar(googleUser.getPicture());
		user.activateUser();
		
		if(user.getEmail().toLowerCase().contains("@thesimplex.co"))
		{
			user.setRole(Role.Administrator);
		}
		
		return user;
	}

	public User CreateUserFromTwitter(TwitterUser twitterUser)
	{
		User user = new User();

		user.setFirstName(twitterUser.getGiven_name());
		user.setLastName(twitterUser.getFamily_name());
		user.setGender(twitterUser.getGender());
		user.setAvatar(twitterUser.getPicture());
		user.activateUser();
		
		if(user.getEmail().toLowerCase().contains("@thesimplex.co"))
		{
			user.setRole(Role.Administrator);
		}
		
		return user;
	}
}
/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import edu.mit.meet.meetag.connectors.ACLManager;
import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.entities.User;
import edu.mit.meet.meetag.enumurations.Role;


/**
 * @brief	Handles all of the CRUD operations for User.
 * @author	Zohar Shevach.
 */
@Controller
@RequestMapping("/user")
public class UserController {

	/**
	 * @brief	The method to delete a user by its email. Minimal access level is administrator.
	 * @param	email	-	The email of the user to delete.
	 * @return	ResponseEntity<Byte> with the response.HttpStatus.OK if the deletion was done successfully,
	 * 			And HttpStatus.UNAUTHORIZED if this method was called by someone who is not administrator.
	 */
	@RequestMapping(value = "/{email}/delete")
	public ResponseEntity<Byte> delete(@PathVariable String email) {
		if (ACLManager.adminCheck()) {
			DAO dao = DAO.getDAO();
			dao.deleteUser(email);
			return new ResponseEntity<Byte>(HttpStatus.OK);
		}
		return new ResponseEntity<Byte>(HttpStatus.UNAUTHORIZED);
	}

	/**
	 * @brief	Method to update certain fields of User.
	 * @param	String	-	email user's email, used for fetching user.
	 * @param	String	-	field name of field being passed.
	 * @param	String	-	value value to be applied to field.
	 * @return	User user the User object to which the values were applied.
	 */
	private User updateUser(String email, String field, String value) {
		if (ACLManager.adminCheck()) {
			DAO dao = DAO.getDAO();
			// System.out.println(email);
			// System.out.println(field);
			// System.out.println(value);
			User user = dao.getUser(email);
			System.out.println(field);
			if (field.equals("first_name")) {
				user.setFirstName(value);
			} else if (field.equals("last_name")) {
				user.setLastName(value);
			} else if (field.equals("role")) {
				user.setRole(Role.valueOf(value));
			} else if (field.equals("birthdate")) {
				user.setBirthdate(value);
			} else if (field.equals("gender")) {
				user.setGender(value);
			} else {
				return null;
			}

			dao.addAndUpdateUser(user);
			return user;
		}
		return null;
	}

	/**
	 * @brief	Method to update certain fields of User.
	 * @param	email	-	email user's email, used for fetching user.
	 * @param	field	-	field name of field being passed.
	 * @param	value	-	value value to be applied to field.
	 * @return	ResponseEntity<Byte> with the response.HttpStatus.OK if the update was done successfully,
	 * 			HttpStatus.BAD_REQUEST if it couldn't update the user (does not exists or can't update the
	 *			field) and HttpStatus.UNAUTHORIZED if this method was called by someone who is not administrator.
	 */
	@RequestMapping(value = "/{email}/update")
	public ResponseEntity<Byte> update(@PathVariable String email,
			@RequestParam String field, @RequestParam String value) {
		if (ACLManager.adminCheck()) {
			if (updateUser(email, field, value) != null) {
				return new ResponseEntity<Byte>(HttpStatus.OK);
			} else {
				// tried to update user that does not exists or a field that
				// cann't be updated
				return new ResponseEntity<Byte>(HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<Byte>(HttpStatus.UNAUTHORIZED);
	}

	/**
	 * Method to update certain fields of the current logged in user (in the
	 * session)
	 * 
	 * @param	field	-	field name of field being passed.
	 * @param	value	-	value value to be applied to field.
	 * @param	request	-	The servlet request that called this method
	 * @return	ResponseEntity<Byte> with the response.HttpStatus.OK if the update was done successfully,
	 * 			HttpStatus.BAD_REQUEST if it couldn't update the user (does not exists or can't update the field)
	 * 			and HttpStatus.UNAUTHORIZED if this method was called by someone who is not administrator.
	 */
	@RequestMapping(value = "/me/update")
	public ResponseEntity<Byte> updateMe(@RequestParam String field,
			@RequestParam String value, HttpServletRequest request) {
		// Why admin check???
		if (ACLManager.adminCheck()) {
			String email = ((User) DAO.getDAO().getUser((String) request.getSession(true).getAttribute("User"))).getEmail();
			User retUser = updateUser(email, field, value);
			
			if (retUser != null) {
				request.getSession(true).setAttribute("User",
						retUser.getEmail());
				return new ResponseEntity<Byte>(HttpStatus.OK);

			} else {
				// tried to update user that does not exists or a field that
				// can't be updated
				return new ResponseEntity<Byte>(HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<Byte>(HttpStatus.UNAUTHORIZED);
	}

	@RequestMapping(value = "/{email}/upgrade")
	public ModelAndView upgrade(@PathVariable String email) {
		if (ACLManager.adminCheck()) {

			DAO dao = DAO.getDAO();
			System.out.println(email);
			User user = dao.getUser(email);
			if (user == null) {
				System.out.println("User is null!");
			}
			user.setRole(Role.Administrator);
			dao.addAndUpdateUser(user);
		}
		return null;
	}

	@RequestMapping(value = "/{email}/downgrade")
	public ModelAndView downgrade(@PathVariable String email) {
		if (ACLManager.adminCheck()) {

			DAO dao = DAO.getDAO();
			User user = dao.getUser(email);
			user.setRole(Role.User);
			dao.addAndUpdateUser(user);
		}
		return null;
	}

	@RequestMapping(value = "/{email}/status/{status}")
	public ModelAndView setStatus(@PathVariable String email, @PathVariable String status) {
		if (ACLManager.adminCheck()) {
			DAO dao = DAO.getDAO();
			User user = dao.getUser(email);
			user.setStatus(status);
			dao.addAndUpdateUser(user);
		}
		return null;
	}

	@RequestMapping(value = "/enroll/{courseId}")
	public @ResponseBody String enrollUser(@PathVariable long courseId){
		User user = (User)GeneralController.session().getAttribute("User");
		user.enrollInCourse(courseId);
		DAO.getDAO().addAndUpdateUser(user);
		GeneralController.session().setAttribute("User", user);
		System.out.println(user.getEnrolledCourses());
		return "OK";
	}
	
	@RequestMapping(value = "/unenroll/{courseId}")
	public @ResponseBody String unenrollUser(@PathVariable long courseId){
	System.out.println("Unenroll!");
        User user = (User)GeneralController.session().getAttribute("User");
		user.unenrollFromCourse(courseId);
        System.out.println("Unenrolling user " + user.getEmail() + "from course " + courseId);
        DAO.getDAO().addAndUpdateUser(user);
		GeneralController.session().setAttribute("User", user);
		System.out.println(user.getEnrolledCourses());
		return "OK";
	}
	@RequestMapping(value = "/ajaxLogin", method = RequestMethod.POST)
	public @ResponseBody String ajaxLogin(@RequestParam String user,@RequestParam String pass,
			HttpServletRequest request){
		System.out.println("Ajax Login");
		DAO dao = DAO.getDAO();
		User u = dao.validateUser(user, pass);
		if(u != null && u.isVerified()){
			request.getSession(true).setAttribute("User", user);
			return (new Gson()).toJson(Boolean.TRUE);
		}
		return (new Gson()).toJson(Boolean.FALSE);
	}
}
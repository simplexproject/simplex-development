/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;

import edu.mit.meet.meetag.connectors.ACLManager;
import edu.mit.meet.meetag.entities.User;
import edu.mit.meet.meetag.enumurations.Role;

@Controller
@RequestMapping(value = "*")
public class GeneralController {
	@Autowired
	ViewResolver res;

	@RequestMapping(value = "/**")
	public String generalRedirect(HttpServletRequest request, Model model) {
		Role role;       
		// Getting the user from the session
		if (GeneralController.session().getAttribute("User") != null) {
			User user = (User) (GeneralController.session()
					.getAttribute("User"));
			role = user.getRole();
		} else {
			role = Role.Guest;
		}
		String location = request.getServletPath().replaceFirst("/", "");
		System.out.println("General Controller redirecting to " + location);

		// If page does not exist send 404.
		if (!ACLManager.getInstance().pageExists(location)) {
			return "404";
		}
		// If page exists and user permitted to access it according
		// to the ACLManager, send them to view
		if (ACLManager.getInstance().permitAccess(location, role)) {
			return location;
		}
		// Send them to 403 because they are not allowed access
		return "login";

	}

	@RequestMapping(value = "/creator/{action}/{object}")
	public ModelAndView creator(HttpServletRequest request,
			@PathVariable String action, @PathVariable String object) {

		if (action.equalsIgnoreCase("create")) {
			if (object.equalsIgnoreCase("course")) {
				ModelAndView mav = new ModelAndView(
						"WEB-INF/views/creator/course.jsp");
				return mav;
			}
		}
		return null;
	}

	public static HttpSession session() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		return attr.getRequest().getSession(true); // true == allow create
	}
}
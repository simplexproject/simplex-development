/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import edu.mit.meet.meetag.connectors.ACLManager;
import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.entities.Course;
import edu.mit.meet.meetag.enumurations.Language;

@Controller
@RequestMapping(value = "/creator")
public class CreatorController {

	@RequestMapping(value = "/createCourse", method = RequestMethod.POST)
	public @ResponseBody
	String createCourse(@RequestParam String courseName,
			@RequestParam String courseDesc, @RequestParam String courseAvatar) {
		if (ACLManager.adminCheck()) {
			Course course = new Course();
			// Default lang to python
			course.setLanguage(Language.Python);
			course.setName(courseName);
			course.setDescription(courseDesc);
			course.setAvatar(courseAvatar);
			DAO.getDAO().addAndUpdateCourse(course);
			return new Gson().toJson(Boolean.TRUE);
		}

		return new Gson().toJson(Boolean.FALSE);
	}
	/**
	 * Returns all of the courses in JSON format
	 * 
	 * @return JSON(Courses)
	 */
	@RequestMapping(value = "/getCourses")
	public @ResponseBody
	String getCourses() {
		return (new Gson().toJson(DAO.getDAO().getAllCourses()));
	}

}
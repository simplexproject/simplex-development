/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import edu.mit.meet.meetag.connectors.IDEoneInitializer;
import edu.mit.meet.meetag.entities.CompilationResponse;

/*
 * This controller handles the compilation requests.
 */

@Controller
@RequestMapping(value = "/compiler")
public class CompilerController {

	/**
	 * 
	 * @param request
	 *            - An AJAX based request from the editor component to compile
	 *            the code. The request has the following parameters.
	 * 
	 *            Request parameters: code - the code to compile language - the
	 *            language in the editor
	 * 
	 * @return - A JSON response with the data from the IDEOne response.
	 */

	@SuppressWarnings("unused")
	@RequestMapping(value = "/compile")
	public @ResponseBody
	String compile(HttpServletRequest request) {

		System.out.println("Compilation request recieved.");

		String code = request.getParameter("code"); // Reading the 'code'
													// parameter from the
													// request.
		String language = request.getParameter("language"); // Reading the
															// 'language'
															// parameter from
															// the request.
		String input = ""; // TO-DO - Needs to be change to read the 'input'
							// parameter from the request.
		boolean run = true; // Run the user code

		// Initializing the IDEOne connector.
		IDEoneInitializer ide = new IDEoneInitializer(); // TO-DO - Handle when
															// the
															// initialization of
															// the connector
															// fails.

		if (ide == null) {
			System.out.println("Failed to initialize the IDEOneConnector.");
		}

		// Using GSON to map the response.
		Gson convert = new Gson();
		Map<String, String> res = ide.createSunmission(code, 4, input, run,
				true); // Creating code submission.

		System.out.println("Code submitted for compilation.");
		boolean working = true;

		/**
		 * This loop waits before checking with IDEOne for results.
		 */
		for (int i = 1; i < 5 && working; i++) {
			try {
				Thread.sleep(600 * i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// Checking the status of the current submission.

			/**
			 * Status codes
			 * 
			 * <0 - Waiting for compilation – The submission awaits execution in
			 * the queue. 0 - Done – The program has finished. 1 - Compilation –
			 * The program is being compiled. 3 - Running – The program is being
			 * executed
			 */

			if (ide.getSubmissionStatus(res.get("link")).get("status")
					.equals("0")) {
				System.out.println("Done compiling.");
				working = false;
			}

		}

		/*
		 * If the request takes too long to execute, or fails, return a false
		 * compilation response. Else, return the response.
		 */
		if (working) {
			return convert.toJson(new CompilationResponse(false, false));
		} else {

			// Get the submission details from IDEOne, create a
			// CompilationResponse object and send it back.
			CompilationResponse resp = CompilationResponse.IDEOneFactory(ide
					.getSubmissionDetails(res.get("link"), true, true, true,
							true, true));

			System.out.println("Reponse from compiler is: " + resp);
			System.out.println(resp.getProgramErrors());
			String jsonresp = convert.toJson(resp);
			return jsonresp;
		}
	}
}
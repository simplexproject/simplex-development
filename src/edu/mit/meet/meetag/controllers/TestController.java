/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import edu.mit.meet.meetag.connectors.IDEoneInitializer;
import edu.mit.meet.meetag.entities.UnitView;

@Controller
@RequestMapping(value = "/test")
public class TestController {
	@RequestMapping(value = "/testIDEOne")
	public ModelAndView testIDEOne(){
		System.out.println("Testing IDE one");
		String code = "class test{\n public static void main(String[] args){\n System.out.println(\"Hello\");\n } }";
		System.out.println("Compiling code:" + code);
		IDEoneInitializer ide = new IDEoneInitializer();
		boolean pass = false;
		
		Map<String,String> res = ide.createSunmission(code, 10, "", true, true);
		String data = "";
		String status;
		String result;
		for(int i = 0;i < 3;i++){
			status = ide.getSubmissionStatus(res.get("link")).get("status");
			result = ide.getSubmissionStatus(res.get("link")).get("result");
			data +=  "Status Code:" + status+ "<br />";
			data +=  "Result:" + result + "<br />";
			
			if(result.equals("15")){
				pass = true;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		data += "<a href=\'http://ideone.com/" + res.get("link") + "\'> To IDEOne</a>";
		return new UnitView(pass, "Test IDE One", data);
		
	}
	@RequestMapping(value = "/testView")
	public ModelAndView TestView(){
		return new UnitView(true, "Views Test");
	}	
}
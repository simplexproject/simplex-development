/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.mit.meet.meetag.connectors.EmailSender;

/**
 * @brief	This controller will handle the Contact Us page, and will send e-mails automatically to info@thesimplex.co.
 */
@Controller
@RequestMapping("/contact")
public class ContactUsController {

	/**
	 * @creator	Zohar Shevach - ??/??/????.
	 * @brief 	A method for sending SIMPLEx staff an E-Mail, this method is for supplying users a way to contact the staff.
	 * @param 	fullName	The full name of the user whom sends the message.
	 * @param 	email		The email address oh the user whom sends the message.
	 * @param 	query		The type on the email.
	 * @param 	message	The message itself.
	 * @return 	ResponseEntity<Byte> with the response.HttpStatus.OK if the email was sent successfully.
	 * @edited	David Heller - 28/09/2012.
	 */
	@RequestMapping(value = "/send")
	public ResponseEntity<Byte> sendContactEmail(@RequestParam String fullName,
			@RequestParam String email, @RequestParam String query, @RequestParam String message) {
		message = "Full Name: " + fullName + "\nEmail: " + email + "\nMessage :" + message + "\n";
		EmailSender emailSender = new EmailSender();
		emailSender.sendContactUs(query, message);
		return new ResponseEntity<Byte>(HttpStatus.OK);
	}
}
/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.mit.meet.meetag.connectors.ACLManager;
import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.entities.Organization;

@Controller
@RequestMapping("/org")
/**
 * @brief	Handles all of the CRUD operations for Organization.
 * @author	Zohar Shevach.
 * @edited	David Heller - 28/09/2012.
 */
public class OrganizationController {

	/**
	 * @brief	This method deletes an organization by its id. Minimal access level is administrator.
	 * @param	id The id of the organization to delete.
	 * @return	ResponseEntity<Byte> with the response.HttpStatus.OK if the deletion was done successfully.
	 * 			and HttpStatus.UNAUTHORIZED if this method was called by someone who is not administrator.
	 */
	@RequestMapping(value = "/{id}/delete")
	public ResponseEntity<Byte> delete(@PathVariable Long id) {
		if (ACLManager.adminCheck()) {
			DAO dao = DAO.getDAO();
			dao.deleteOrganization(id);
			return new ResponseEntity<Byte>(HttpStatus.OK);
		}
		return new ResponseEntity<Byte>(HttpStatus.UNAUTHORIZED);
	}

	/**
	 * @brief	Method to update certain fields of organization
	 * @param	id				-	organization's id, used for fetching the organization.
	 * @param	field			-	name of field being passed.
	 * @param	value			-	value to be applied to field.
	 * @return	Organization	-	The Organization object to which the values were applied.
	 */
	private Organization updateOrg(Long id, String field, String value) {
		if (ACLManager.adminCheck()) {
			DAO dao = DAO.getDAO();
			Organization organization = dao.getOrganization(id);
			System.out.println(field);
			if (field.equals("name")) {
				organization.setName(value);
			} else {
				return null;
			}
			dao.addAndUpdateOrganization(organization);
			return organization;
		}
		return null;
	}

	/**
	 * @brief	Method to update certain fields of organization.
	 * @param	id		-	organization's id, used for fetching the organization.
	 * @param	field	-	name of field being passed.
	 * @param	value	-	value to be applied to field.
	 * @return	ResponseEntity<Byte> with the response.HttpStatus.OK if the update was done successfully,
	 * 			HttpStatus.BAD_REQUEST if it couldn't update the organization (does not exists or can't update the field)
	 * 			and HttpStatus.UNAUTHORIZED if this method was called by someone who is not administrator.
	 */
	@RequestMapping(value = "/{id}/update")
	public ResponseEntity<Byte> update(@PathVariable Long id,
			@RequestParam String field, @RequestParam String value) {
		if (ACLManager.adminCheck()) {
			if (updateOrg(id, field, value) != null) {
				return new ResponseEntity<Byte>(HttpStatus.OK);
			} else {
				// Tried to update organization that does not exists or a field that can't be updated.
				return new ResponseEntity<Byte>(HttpStatus.BAD_REQUEST);
			}
		}
		
		return new ResponseEntity<Byte>(HttpStatus.UNAUTHORIZED);
	}
}
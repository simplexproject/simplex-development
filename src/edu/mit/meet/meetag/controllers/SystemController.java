/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.mit.meet.meetag.connectors.ConfigurationManager;
import edu.mit.meet.meetag.enumurations.SiteStatus;

@Controller
@RequestMapping(value = "/system")
public class SystemController {

	// 0 is OFFLINE
	// 1 is ONLINE
	// 2 is MAINTAINENCE
	@RequestMapping(value = "/siteStatus/{siteState}")
	public void setSiteStatus(@PathVariable int siteState) {
		String setting;

		switch (siteState) {
		default:
			setting = SiteStatus.Online.toString();
			break;
		case 0:
			setting = SiteStatus.Offline.toString();
			break;
		case 1:
			setting = SiteStatus.Online.toString();
			break;
		case 2:
			setting = SiteStatus.Maintenance.toString();
			break;
		case 3:
			setting = SiteStatus.Beta.toString();
			break;
		case 4: //Testing status means access to all pages on the site is enabled.
			setting = SiteStatus.Testing.toString();
			break;
		}

		System.out.println("Site status set to " + setting);
		ConfigurationManager.getInstance();
		ConfigurationManager.getInstance().setSettings(SiteStatus.SITE_STATUS_KEY, setting);
	}

}

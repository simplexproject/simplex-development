/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import edu.mit.meet.meetag.entities.Message;
import edu.mit.meet.meetag.entities.User;

@Controller
@RequestMapping(value = "/inbox")
public class InboxController {

	/**
	 * Returns a JSON Object containing all the messages inside 
	 * The users inbox.
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getMessages")
	public @ResponseBody String getMessages(HttpServletRequest request) {
		User user = (User) (request.getAttribute("User"));
		List<Message> inbox = user.getMailbox().getInbox();
		Gson g = new Gson();
		return g.toJson(inbox);
		
	}
}
/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.entities.*;
import edu.mit.meet.meetag.enumurations.*;

@Controller
@RequestMapping(value = "/generate")
public class BulkData {

	@RequestMapping(value = "/users/{number}")
	public void fillMeUp(@PathVariable int number) {

		ArrayList<String> names = new ArrayList<String>();
		names.add("George Khoury");
		names.add("Zohar Shevach");
		names.add("Sadek Jabr");
		names.add("Matan Mates");
		names.add("Ran Czaczkes");
		names.add("David Heller");
		names.add("Ahmed Shawish");
		ArrayList<String> emails = new ArrayList<String>();
		emails.add("george@thesimplex.co");
		emails.add("zohar@thesimplex.co");
		emails.add("sadek@thesimplex.co");
		emails.add("matan@thesimplex.co");
		emails.add("ran@thesimplex.co");
		emails.add("david@thesimplex.co");
		emails.add("ahmed@thesimplex.co");
		ArrayList<String> roles = new ArrayList<String>();
		roles.add("User");
		roles.add("Administrator");
		ArrayList<String> statuses = new ArrayList<String>();
		statuses.add("Verified");
		statuses.add("Not Verified");
		statuses.add("Banned");

		DAO dao = DAO.getDAO();

		ArrayList<User> users = new ArrayList<User>();

		for (int i = 0; i < number; i++) {
			Collections.shuffle(names);
			Collections.shuffle(emails);
			Collections.shuffle(roles);
			Collections.shuffle(statuses);
			User user = new User();
			user.setFirstName(names.get(0).split(" ")[0]);
			user.setLastName(names.get(0).split(" ")[1]);
			user.setEmail(emails.get(0) + i);
			user.setRole(Role.values()[(int) (Math.random() * Role.values().length)]);
			user.setStatus(statuses.get(0));
			MailBox mailbox = new MailBox(user.getEmail());
			user.setMailbox(mailbox);
			users.add(user);
			Message msg = new Message();
			msg.setTitle("Testing Messages System");
			List<Message> mb = user.getMailbox().getInbox();
			mb.add(msg);
			System.out.println(mb.size());
			System.out.println("Value of Mailbox is " + mb);
			dao.addAndUpdateUser(user);

		}

		// ArrayList<Message> messages = new ArrayList<Message>();
		// messages.add(msg);

	}

	@RequestMapping(value = "/courses/{number}")
	public void fillMeUpCourses(@PathVariable int number) {

		ArrayList<String> titles = new ArrayList<String>();
		titles.add("Intro to Python");
		titles.add("Java 101");
		titles.add("C for newbies");
		titles.add("Android Development");
		titles.add("GCL");
		titles.add("JavaScript");
		titles.add("HTML");
		titles.add("Ruby On Rails");
		titles.add("Ruby");
		titles.add("Ada");
		titles.add("Joss");

		ArrayList<String> descriptions = new ArrayList<String>();

		descriptions
				.add("The word data is the plural of datum, neuter past participle of the Latin dare");
		descriptions
				.add("Information as a concept bears a diversity of meanings.");
		descriptions
				.add("Java technology allows you to work and play in a secure computing environment.");
		descriptions
				.add("Python is a programming language that lets you work more quickly.");
		descriptions
				.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
		descriptions
				.add("It is a long established fact that a reader will be distracted by the");
		descriptions
				.add("Contrary to popular belief, Lorem Ipsum is not simply random text.");
		descriptions
				.add("There are many variations of passages of Lorem Ipsum available, but the majority");
		descriptions
				.add("Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium");

		ArrayList<Language> language = new ArrayList<Language>();
		language.add(Language.Python);
		language.add(Language.C);
		language.add(Language.Java);
		language.add(Language.Ada);
		language.add(Language.Cobol);

		ArrayList<String> avatars = new ArrayList<String>();
		avatars.add("http://www.redorbit.com/media/uploads/2004/10/0_746ac3fe2bf33c3a0a07b05a23c46e47.jpg");
		avatars.add("http://file-hub.com/new/logo/c.gif");
		avatars.add("http://www.seas.gwu.edu/~mfeldman/ada-coin.jpg");
		avatars.add("http://a4.mzstatic.com/us/r30/Purple/v4/95/18/d6/9518d6c7-9c3a-3114-31c4-c35de5164a87/mzl.mftjqmrs.png");

		for (int i = 0; i < number; i++) {
			Collections.shuffle(titles);
			Collections.shuffle(descriptions);
			Collections.shuffle(language);
			Collections.shuffle(avatars);

			Course course = new Course();
			course.setLanguage(language.get((int) Math.floor(Math.random()	* language.size())));
			course.setName(titles.get(0));
			course.setDescription(descriptions.get(0) + descriptions.get(1)
					+ descriptions.get(2) + descriptions.get(3));
			course.setPopularity((int) (Math.random() * 10));
			course.setAvatar(avatars.get(0));
			course.setLessons(fillMeUpLessons(number));
			DAO.getDAO().addAndUpdateCourse(course);
		}
	}

	@RequestMapping(value = "/lessons/{number}")
	public ArrayList<Lesson> fillMeUpLessons(@PathVariable int number) {

		ArrayList<String> titles = new ArrayList<String>();
		titles.add("Lesson 1");
		titles.add("Lesson 2");
		titles.add("Lesson 3");
		titles.add("Lesson 4");
		titles.add("Lesson 5");
		titles.add("Lesson 6");
		titles.add("Lesson 7");
		titles.add("Lesson 8");

		ArrayList<String> questionTitle = new ArrayList<String>();
		questionTitle.add("Question 1");
		questionTitle.add("Question 2");
		questionTitle.add("Question 3");
		questionTitle.add("Question 4");
		questionTitle.add("Question 5");
		questionTitle.add("Question 6");
		questionTitle.add("Question 7");
		questionTitle.add("Question 8");
		questionTitle.add("Question 9");

		ArrayList<String> descriptions = new ArrayList<String>();

		descriptions
				.add("The word data is the plural of datum, neuter past participle of the Latin dare");
		descriptions
				.add("Information as a concept bears a diversity of meanings.");
		descriptions
				.add("Java technology allows you to work and play in a secure computing environment.");
		descriptions
				.add("Python is a programming language that lets you work more quickly.");
		descriptions

				.add("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
		descriptions
				.add("It is a long established fact that a reader will be distracted by the");
		descriptions
				.add("Contrary to popular belief, Lorem Ipsum is not simply random text.");
		descriptions
				.add("There are many variations of passages of Lorem Ipsum available, but the majority");
		descriptions
				.add("Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium");

		ArrayList<Language> language = new ArrayList<Language>();
		language.add(Language.Ruby);
		language.add(Language.C);
		language.add(Language.Java);
		language.add(Language.Ada);
		language.add(Language.Cobol);

		ArrayList<Lesson> lessons = new ArrayList<Lesson>();
		for (int i = 1; i <= number; i++) {
			Lesson ls = new Lesson((long) i, titles.get((int) Math.floor(Math
					.random() * titles.size())), descriptions.get((int) Math
					.floor(Math.random() * descriptions.size())));
			lessons.add(ls);
		}
		ArrayList<String> choicesList = new ArrayList<String>();
		choicesList.add("Choice 1");
		choicesList.add("Choice 2");
		choicesList.add("Choice 3");
		choicesList.add("Choice 4");

		for (Lesson l : lessons) {
			Component[] arr = new Component[8];
			arr[0] = new TextQuestion(0, false, "I am an awesome question",
					"Okay ahmed is racist", QuestionType.TextQuestion,
					"Yes Shawish is racees", "I am a shawishiboy");
			arr[1] = new MultiChoiceQuestion(1, false,
					"I'm a multichoice question", "I'm a description",
					QuestionType.MultiChoiceQuestion, "answer",
					"correctAnswer", choicesList);
			arr[2] = new TextComponent(2, "This is a text Component",
					"I have alot of text lolololololololololololololololofekfeopfwjefoiewjfewifjo");
			arr[3] = new TextQuestion(3, false, "I am YES Awesome Question",
					"Okay Matan is racist", QuestionType.TextQuestion,
					"Yes Shawish is racees", "I am a shawishiboy");
			arr[4] = new VideoComponent(4, "", "70%", "315", "t9u9mZz-o-g");

			arr[5] = new TextComponent(
					5,
					"Video",
					"<br/><iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/IKqV7DB8Iwg\" frameborder=\"0\" allowfullscreen></iframe>");
			arr[6] = new EditorComponent(
					6,
					true,
					"Editor",
					"It's a sexy editor! It's like, sooooo sexy. Too sexy. Way too sexy.",
					Language.Java);
			l.setComponents(arr);
		}
		System.out.println(descriptions.size());

		return lessons;

	}

	@RequestMapping(value = "/firstlesson")
	public void fillMeUpFirstLesson() {
		
		System.out.println("====> Dummy Python lesson generated.");
		Course core = new Course();
		core.setDescription("Start learning Python today!.");
		core.setName("Python 101");
		List<Lesson> lesson = new ArrayList<Lesson>();
		Lesson py = new Lesson(1, "Hello, Python!",
				"Introduction to Python, Print statements");
		Component[] comps = new Component[7];
		comps[0] = new TextComponent(
				0,
				"",
				"Hello and welcome to your first Python lesson. Today we will be learning about the most basic operation a computer program can do, print out something to the screen. <br/>"
						+ "Python uses the print command to print to the screen. <br/>"
						+ "Try this out for yourself! <br/>"
						+ "<blockquote> print \"Hello, Python!\"</blockquote>");
		comps[1] = new EditorComponent(
				6,
				true,
				"Editor",
				"It's a sexy editor! It's like, sooooo sexy. Too sexy. Way too sexy.",
				Language.Java);
		comps[2] = new EditorComponent(
				6,
				true,
				"Editor",
				"It's a sexy editor! It's like, sooooo sexy. Too sexy. Way too sexy.",
				Language.Java);
		comps[3] = new EditorComponent(
				6,
				true,
				"Editor",
				"It's a sexy editor! It's like, sooooo sexy. Too sexy. Way too sexy.",
				Language.Java);
		comps[4] = new TextComponent(
				4,
				"",
				"You can also have multiple statements, and Python will be able to execute all of them together. Go ahead and try it now!");
		comps[5] = new EditorComponent(
				6,
				true,
				"Editor",
				"It's a sexy editor! It's like, sooooo sexy. Too sexy. Way too sexy.",
				Language.Java);
		comps[6] = new TextComponent(
				6,
				"",
				"Comments are very important in your programs. They help you remember coding tasks you need to do. They are used to tell you what something does in English, and it helps other developers understand your code. Here's how you use comments in Python:</br></br># I am a comment. I serve as a reminder or a note.\n"
						+ "# Make sure you put a # before the start of your line, because that's how Python knows</br>"
						+ "# what to ignore.");
		core.setAvatar("http://greeennotebook.com/wp-content/uploads/2010/06/python-logo.png");
		core.setPopularity((int) (Math.random() * 10));
		py.setComponents(comps);
		lesson.add(py);
		core.setLessons((ArrayList<Lesson>) lesson);
		DAO.getDAO().addAndUpdateCourse(core);
	}

	@RequestMapping(value = "/organization")
	public void fillMeUpOrganization() {
		ArrayList<String> managers = new ArrayList<String>();
		managers.add("zohar@thesimplex.co");
		Organization org = new Organization(managers, "MEET");
		org.addUser("zohar@thesimplex.co");
		DAO.getDAO().addAndUpdateOrganization(org);

	}
	@RequestMapping(value = "/firstUser")
	public void ahmedGayWalrus(){
		User u = new User("yuval@yuval.com", User.getEncryptedPassword("12345","yuval@yuval.com"),"Red","Lol", "/gay.jpeg", null, null, 
				null);
		DAO.getDAO().addAndUpdateUser(u);
		
	}
}
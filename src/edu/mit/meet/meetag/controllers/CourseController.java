/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.RedirectView;

import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.entities.Component;
import edu.mit.meet.meetag.entities.Course;
import edu.mit.meet.meetag.entities.Lesson;

@Controller
@RequestMapping("/course")
public class CourseController {

	@RequestMapping("/{id}")
	public InternalResourceView getCourse(@PathVariable long id) {
		System.out.println("Requested course is " + id);
		Course course = DAO.getDAO().getCourse(id);

		if (course != null) {
			System.out.println("Redirecting to course.jsp");

			InternalResourceView irv = new InternalResourceView(
					"/WEB-INF/views/course.jsp");
			irv.addStaticAttribute("courseId", id);
			irv.addStaticAttribute("course", course);
			return irv;
		} else {
			System.out.println("Redirecting to 404");
			return null;
		}
	}

	@RequestMapping("/{id}/{lessonid}")
	public ModelAndView getLesson(@PathVariable Long id,
			@PathVariable Long lessonid) {

		long idLong;
		try {
			idLong = id;
		} catch (Exception e) {
			e.printStackTrace();
			ModelAndView mav = new ModelAndView(new RedirectView(
					"/404?path=lesson", false));
			return mav;
		}
		DAO dao = DAO.getDAO();

		Course core = dao.getCourse(idLong);

		Lesson lesson = null;
		for (Lesson l : core.getLessons()) {
			if (l.getId().equals(lessonid)) {
				lesson = l;
			}
		}
		if (lesson == null) {
			ModelAndView mav = new ModelAndView("/404?path=lesson");
			return mav;
		}

		ModelAndView mav = new ModelAndView("lessonview");

		List<Component> lcomp = new ArrayList<Component>();
		for (Component com : lesson.getComponents()) {
			lcomp.add(com);
		}
		lesson.setComponents(lcomp.toArray(new Component[0]));
		mav.addObject("Lesson", lesson);

		mav.addObject("components", lcomp);
		return mav;

	}	
}
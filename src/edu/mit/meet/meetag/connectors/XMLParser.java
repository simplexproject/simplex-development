/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.connectors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 
 * @param <K>
 *            The Key for the map
 * @param <V>
 *            The value for the map
 */
public class XMLParser<K, V> {

	// The keyword key
	private final String KEY = "key";
	// The keyword value
	private final String VALUE = "value";
	// The group number of the key part
	private final String KEY_GRP = "\\1";
	// The group number of the key value
	private final int KEY_VAL = 2;
	// The group number of the value part
	private final String VAL_GRP = "\\3";
	// The group number of the value value
	private final int VAL_VAL = 4;
	/*
	 * The AWESOMEEEEEEEEE regex.First look for the keyword 'key', than for the
	 * value of the key.Than looks for the keyword value, than for the value of
	 * the value.
	 */
	private final String PARSE_REGEX = "\\<(" + KEY
			+ ")[^\\>]*\\>([^\\<]*)\\<\\/" + KEY_GRP + "\\>" + "\\<(" + VALUE
			+ ")[^\\>]*\\>([^\\<]*)\\<\\/" + VAL_GRP + "\\>";

	/**
	 * Generates a map for all the pairs of key-value. Assumes that a unique key
	 * will have only one value.
	 * 
	 * @param xmlResponse
	 *            ArrayList<String> hat represent the response from the server
	 * @return a map of type <K,V>, that contains all the keys and values
	 */
	@SuppressWarnings("unchecked")
	public Map<K, V> parseXMLResponse(ArrayList<String> xmlResponse) {
		String singleResponse = "";
		for(String str:xmlResponse){
			singleResponse+=str;
		}
		HashMap<K, V> responseMap = new HashMap<K, V>();
		Pattern pattern = Pattern.compile(PARSE_REGEX);

			Matcher matcher = pattern.matcher(singleResponse);
			while (matcher.find()) {
				responseMap.put((K) matcher.group(KEY_VAL),
						(V) matcher.group(VAL_VAL));
			
		}
		
		return responseMap;
	}
//	public Map<K, V> testParser(ArrayList<String> xmlResponse){
//		HashMap<K, V> responseMap = new HashMap<K, V>();
		
//	}
}

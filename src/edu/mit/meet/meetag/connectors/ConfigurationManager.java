/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.connectors;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.DAOBase;

import edu.mit.meet.meetag.entities.Settings;

public class ConfigurationManager extends DAOBase {

	private final static ConfigurationManager INSTANCE = new ConfigurationManager();

	private final static int SETTINGS_PERSISTANCE_CONSTANT = 123454321;
	private Objectify objectify;

	public static final String SETTING_DOES_NOT_EXIST = "Default";

	private Settings setting;

	public static ConfigurationManager getInstance() {
		return INSTANCE;
	}

	private ConfigurationManager() {
		ObjectifyService.register(Settings.class);
		objectify = ObjectifyService.begin();
		setting = objectify.find(Settings.class, SETTINGS_PERSISTANCE_CONSTANT);
		if (setting == null) {
			setting = new Settings(SETTINGS_PERSISTANCE_CONSTANT,
					(Map<String, String>) new HashMap<String, String>());
			objectify.put(setting);
		}
	}

	public void saveLocalSettings() {
		objectify.put(setting);
	}
	public void updateLocalSettings(){
		setting = objectify.get(Settings.class,SETTINGS_PERSISTANCE_CONSTANT);
	}
	public Iterator<Entry<String, String>> getPreferences() {
		updateLocalSettings();
		return setting.getPreferences().entrySet().iterator();
	}
	public void setSettings(String key,String value){
		setting.getPreferences().put(key,value);
		saveLocalSettings();
	}
	public String getSetting(String key){
		updateLocalSettings();
		String value = setting.getPreferences().get(key);
		if(value == null)
			return SETTING_DOES_NOT_EXIST;
		return value;
	}

}

/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.connectors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.LowLevelHttpRequest;
import com.google.api.client.http.LowLevelHttpResponse;
import com.google.api.client.http.javanet.NetHttpTransport;

/**
 * @author Matan Mates
 * @version 1.0
 */

public class IDEoneInitializer {

	private final static String SERVER_URL = "http://ideone.com/api/1/service";

	public static final String PUBLIC_ERROR_KEYWORD = "error";
	public static final String PUBLIC_LINK_KEYWORD = "link";

	// XML version
	private static final double VERSION = 1.0;
	// The URL of the SOAP ENV`
	private static final String SOAP_ENV = "http://schemas.xmlsoap.org/soap/envelope/";
	// THE URL of the XSI
	private static final String XSI = "http://www.w3.org/2001/XMLSchema-instance";
	// A static String that represent the beginning of the XML
	private static final String XML_BEGINNING = generateBeginning();
	// A static String that represent the end of the XML
	private static final String XML_END = generateEnd();
	// A static String that represent the USER-PASS of the XML
	private static final String XML_USER_PASS = generateUserPass();
	// The user
	private static final String USER = "gkhoury";
	// The freaking awesome password
	private static final String PASS = "wkwf3xA6LLzKZfgnGmGFPF";

	/**
	 * For the following, checkout IDEone API
	 */

	/**
	 * SUBMISSION_STATUS
	 */
	// The keyword for getting the submission status from IDEone
	private static final String SUBMISSION_STATUS = "getSubmissionStatus";
	// The keyword for getting the submission status link parameter from IDEone
	private static final String SUB_STAT_LINK = "link";
	// The params of the submission status request
	private static final ArrayList<String> SUB_STAT_PARAMS = getSubmissionStatusParams();

	/**
	 * SUBMISSION_DETAILS
	 */

	// The keyword for getting the submission details from IDEone
	private static final String SUBMISSION_DETAILS = "getSubmissionDetails";
	// The keyword for getting the submission details link parameter from IDEone
	private static final String SUB_DET_LINK = "link";
	// The keyword for getting the submission details source parameter from
	// IDEone
	private static final String SUB_DET_SRC = "withSource";
	// The keyword for getting the submission details input parameter from
	// IDEone
	private static final String SUB_DET_IN = "withInput";
	// The keyword for getting the submission details output parameter from
	// IDEone
	private static final String SUB_DET_OUT = "withOutput";
	// The keyword for getting the submission details sdterr parameter from
	// IDEone
	private static final String SUB_DET_STDERR = "withStderr";
	// The keyword for getting the submission details compilation info parameter
	// from IDEone
	private static final String SUB_DET_CMP = "withCmpinfo";
	// The params of the submission status request
	private static final ArrayList<String> SUB_DET_PARAMS = getSubmissionDetailsParams();

	/**
	 * CREATE_SUBMISSION
	 */

	// The keyword for getting the submission details from IDEone
	private static final String CREATE_SUBMISSION = "createSubmission";
	// The keyword for getting the submission source code parameter from
	// IDEone
	private static final String CRT_SUB_SRC = "sourceCode";
	// The keyword for getting the submission language parameter from
	// IDEone
	private static final String CRT_SUB_LAN = "language";
	// The keyword for getting the submission input parameter from
	// IDEone
	private static final String CRT_SUB_IN = "input";
	// The keyword for getting the submission run parameter from
	// IDEone
	private static final String CRT_SUB_RUN = "run";
	// The keyword for getting the submission private parameter from
	// IDEone
	private static final String CRT_SUB_PRIV = "private";
	// The params of the submission status request
	private static final ArrayList<String> CRT_SUB_PARAMS = getCreateSubmissionParams();

	/**
	 * ELSE
	 */

	// The keyword for getting the languages from IDEone
	private static final String GET_LANGUAGES = "getLanguages";
	// The keyword for test function from IDEone
	private static final String TEST_FUNC = "testFunction";

	// A symbol for opening an open element in XML
	private static final String OPEN_OPEN_ELEMENT = "<";
	// A symbol for opening a close element in XML
	private static final String OPEN_CLOSE_ELEMENT = "</";
	// A symbol for closing an element in XML
	private static final String CLOSE_ELEMENT = ">";
	// A double quote sign
	private static final String DOUBLE_QUOTE = "\"";

	// The map of all the languages to their code
	private static HashMap<String, Integer> LANGUAGES_CODE = initToNull();
	// The sign in the beginng of a version of a language on IDEone
	private static final String VERSION_BEGNING = "(";
	// an XML parser
	private XMLParser<String, String> parser;

	// The HttpURLConnection to the IDEone server
	// private HttpURLConnection connection;

	/**
	 * Default constructor
	 */
	public IDEoneInitializer() {
		parser = new XMLParser<String, String>();
		initLanguagesMap();

	}

	private static HashMap<String, Integer> initToNull() {
		return null;
	}

	/**
	 * Initialize the connection to the server. Might throw an exception id an
	 * error occurred
	 */
	public void initialize() {
		// URL u;
		// try {
		// u = new URL(defaultServer);
		// URLConnection uc = u.openConnection();
		// connection = (HttpURLConnection) uc;
		// connection.setDoOutput(true);
		// connection.setDoInput(true);
		// connection.setRequestMethod("POST");
		// connection.setRequestProperty("Connection", "keepAlive");
		// initLanguagesMap();
		//
		// } catch (MalformedURLException e) {
		// System.err.println("Cann't find URL of the connection");
		// e.printStackTrace();
		// } catch (IOException e) {
		// System.err.println("Cann't create the connection");
		// e.printStackTrace();
		// }
	}

	/**
	 * This method initialize the map of all the languages that IDEone supports
	 */
	private void initLanguagesMap() {
		if (LANGUAGES_CODE == null) {
			LANGUAGES_CODE = new HashMap<String, Integer>();
			Map<String, String> tmpMap = getSupportedLanguages();
			for (Map.Entry<String, String> entry : tmpMap.entrySet()) {
				if (entry.getKey().toLowerCase().equals("error")) {
					continue;
				}
				LANGUAGES_CODE.put(
						entry.getValue()
								.substring(
										0,
										(entry.getValue().indexOf(
												VERSION_BEGNING) - 1))
								.toLowerCase(),
						Integer.parseInt(entry.getKey()));
			}
		}
	}

	/**
	 * A destroy method. Still unknown what will happen if we are trying to
	 * destory a dead connection
	 */
	public void destroy() {
		// connection.setRequestProperty("Connection", "close");
	}

	/**
	 * Send a getSupportedLanguages request and prints the response Executing
	 * the xmlRequet to the server.
	 * 
	 * @param xmlRequest
	 *            a String that represent the request
	 * @return Map<String, String> that represent the response of the execution
	 *         (an empty ArrayList if the response is empty), and null if there
	 *         was a problem (it actually should throw an exception)
	 */
	public Map<String, String> getSupportedLanguages() {
		String req = createOnlyStringsRequest(GET_LANGUAGES,
				new ArrayList<String>(), new ArrayList<String>());
		return executeXML(req);

	}

	/**
	 * Send a getSubmissionStatus request and prints the response Executing the
	 * xmlRequet to the server.
	 * 
	 * @param link
	 *            the link identifier for the IDEone
	 * @return Map<String,String> that represent the response of the execution
	 *         (an empty ArrayList if the response is empty), and null if there
	 *         was a problem (it actually should throw an exception)
	 */
	public Map<String, String> getSubmissionStatus(String link) {
		ArrayList<String> values = new ArrayList<String>();
		values.add(link);
		String req = createOnlyStringsRequest(SUBMISSION_STATUS,
				SUB_STAT_PARAMS, values);
		return executeXML(req);

	}

	/**
	 * Send a getSubmissionDetails request and prints the response Executing the
	 * xmlRequet to the server.
	 * 
	 * @param link
	 *            the link identifier for the IDEone
	 * @param withSource
	 *            determines whether the source code should be return or not
	 * @param withInput
	 *            determines whether the input data should be return or not
	 * @param withOutput
	 *            determines whether the output of the program should be return
	 *            or not
	 * @param withStderr
	 *            determines whether the std err should be return or not
	 * @param withCmpinfo
	 *            determines whether the compilation information should be
	 *            return or not
	 * @return Map<String,String> that represent the response of the execution
	 *         (an empty ArrayList if the response is empty), and null if there
	 *         was a problem (it actually should throw an exception)
	 */
	public Map<String, String> getSubmissionDetails(String link,
			boolean withSource, boolean withInput, boolean withOutput,
			boolean withStderr, boolean withCmpinfo) {
		ArrayList<String> values = new ArrayList<String>();
		values.add(link);
		values.add(String.valueOf(withSource));
		values.add(String.valueOf(withInput));
		values.add(String.valueOf(withOutput));
		values.add(String.valueOf(withStderr));
		values.add(String.valueOf(withCmpinfo));
		String req = createOnlyStringsRequest(SUBMISSION_DETAILS,
				SUB_DET_PARAMS, values);
		return executeXML(req);

	}

	/**
	 * Send a createSubmission request and prints the response Executing the
	 * xmlRequet to the server.
	 * 
	 * 
	 * @param sourceCode
	 *            source code of the paste
	 * @param language
	 *            the language identifier for the IDEone
	 * @param b
	 *            Data to the program given in the stdin
	 * @param run
	 *            determines whether the execute the code or not
	 * @param privateValue
	 *            determines whether the paste should be private
	 * @return Map<String,String> that represent the response of the execution
	 *         (an empty ArrayList if the response is empty), and null if there
	 *         was a problem (it actually should throw an exception)
	 */

	public Map<String, String> createSunmission(String sourceCode,
			int language, String input, boolean run, boolean privateValue) {
		ArrayList<String> values = new ArrayList<String>();
		values.add(sourceCode);
		values.add(String.valueOf(language));
		values.add(input);
		values.add(String.valueOf(run));
		values.add(String.valueOf(privateValue));
		String req = createOnlyStringsRequest(CREATE_SUBMISSION,
				CRT_SUB_PARAMS, values);
		return executeXML(req);
	}

	/**
	 * Send a testFunc request and prints the response Executing the xmlRequet
	 * to the server.
	 * 
	 * @return Map<String,String> that represent the response of the execution
	 *         (an empty ArrayList if the response is empty), and null if there
	 *         was a problem (it actually should throw an exception)
	 */
	public Map<String, String> testFunc() {
		String req = createOnlyStringsRequest(TEST_FUNC,
				new ArrayList<String>(), new ArrayList<String>());
		return executeXML(req);

	}

	/**
	 * Reading the response from the server. If there is no response - returns
	 * null.
	 * 
	 * @return ArrayList<String> where each element is a line in the response of
	 *         the xml. null if you can't read the response and an empty
	 *         ArrayList if there is no response
	 */
	private ArrayList<String> getResponse(InputStream in) {
		// Reading from the stream
		BufferedReader bReader = null;
		try {
			bReader = new BufferedReader(new InputStreamReader(in));
			ArrayList<String> response = new ArrayList<String>();
			String line = bReader.readLine();
			while (line != null) {
				response.add(line);
				line = bReader.readLine();
			}
			return response;
		} catch (IOException e) {
			System.err.println("Couldn't read the response properly");
			e.printStackTrace();
		} finally {
			// This part is in order to close the buffer properly
			if (bReader != null) {
				try {
					bReader.close();
				} catch (IOException e) {
					System.err.println("Couldn't close writer");
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * Write the request to the IDEone server.
	 * 
	 * @param xmlRequest
	 *            a String that represent the request
	 */
	private ArrayList<String> writeRequest(String xmlRequest) {
		NetHttpTransport aht = new NetHttpTransport();

		LowLevelHttpRequest req;
		try {
			req = aht.buildPostRequest(SERVER_URL);
			req.setTimeout(5000, 5000);
			// Writing to the stream
			byte[] contentInBytes = xmlRequest.getBytes();
			ByteArrayContent byteArrContent = new ByteArrayContent(null,
					contentInBytes);
			try {
				req.setContent(byteArrContent);
				LowLevelHttpResponse res = req.execute();
				return getResponse(res.getContent());
			} catch (IOException e) {
				System.err.println("IOException in setting request content");
				e.printStackTrace();
			}
		} catch (IOException e1) {
			System.err
					.println("IOException when trying to create the connection (to IDEone with NetHttpTransport)");
			e1.printStackTrace();
		}
		return null;
	}

	/**
	 * Executing the xmlRequet to the server.
	 * 
	 * @param xmlRequest
	 *            a String that represent the request
	 * @return Map<String,String> that represent the response of the execution
	 *         (an empty ArrayList if the response is empty), and null if there
	 *         was a problem (it actually should throw an exception) Send a
	 *         getSupportedLanguages request and prints the response
	 */
	private Map<String, String> executeXML(String xmlRequest) {
		ArrayList<String> temp = writeRequest(xmlRequest);
		return parser.parseXMLResponse(temp);
	}

	/**
	 * Create a requests of type String only If the requests does not have any
	 * children - should get an empty ArrayLists!!
	 * 
	 * @param type
	 *            the type of the request
	 * @param params
	 *            List of types of the children (not null!)
	 * @param values
	 *            List of types of the children values (not null!)
	 * @return a String that represent the request
	 */
	private String createOnlyStringsRequest(String type,
			ArrayList<String> params, ArrayList<String> values) {
		String req = XML_BEGINNING;
		req += OPEN_OPEN_ELEMENT + type + CLOSE_ELEMENT;
		req += XML_USER_PASS;
		for (int i = 0; i < params.size(); i++) {
			req += generateElement(params.get(i), values.get(i));
		}
		req += OPEN_CLOSE_ELEMENT + type + CLOSE_ELEMENT;
		req += XML_END;
		return req;
	}

	/**
	 * Generate one element of the XML (not a nested element!) Assumes valid
	 * input
	 * 
	 * @param type
	 *            the type of the element
	 * @param value
	 *            the value of the element
	 * @return a String that represent the element
	 */
	private static String generateElement(String type, String value) {
		String elem = OPEN_OPEN_ELEMENT + type + CLOSE_ELEMENT;
		elem += value;
		elem += OPEN_CLOSE_ELEMENT + type + CLOSE_ELEMENT;
		return elem;
	}

	/**
	 * Generate the permanent String that represent the user and password
	 * 
	 * @return the constant user-password part
	 */
	private static String generateUserPass() {
		String userPass = generateElement("user", USER);
		userPass += generateElement("pass", PASS);
		return userPass;
	}

	/**
	 * Generate the permanent String that represent the beginning of the XML
	 * file
	 * 
	 * @return the constant beginning
	 */
	private static String generateBeginning() {
		String begining = "<?xml version=" + DOUBLE_QUOTE + VERSION
				+ DOUBLE_QUOTE + "?>";
		begining += "<SOAP-ENV:Envelope xmlns:SOAP-ENV=" + DOUBLE_QUOTE
				+ SOAP_ENV + DOUBLE_QUOTE + "xmlns:xsi=" + DOUBLE_QUOTE + XSI
				+ DOUBLE_QUOTE + ">";
		begining += "<SOAP-ENV:Body>";
		return begining;
	}

	/**
	 * Generate the permanent String that represent the end of the XML file
	 * 
	 * @return the constant end
	 */
	private static String generateEnd() {
		String end = "</SOAP-ENV:Body></SOAP-ENV:Envelope>";
		return end;
	}

	/**
	 * @return An ArrayList of all the parameters needed to send a submission
	 *         status request
	 */
	private static ArrayList<String> getSubmissionStatusParams() {
		ArrayList<String> params = new ArrayList<String>();
		params.add(SUB_STAT_LINK);
		return params;
	}

	/**
	 * @return An ArrayList of all the parameters needed to send a submission
	 *         details request
	 */
	private static ArrayList<String> getSubmissionDetailsParams() {
		ArrayList<String> params = new ArrayList<String>();
		params.add(SUB_DET_LINK);
		params.add(SUB_DET_SRC);
		params.add(SUB_DET_IN);
		params.add(SUB_DET_OUT);
		params.add(SUB_DET_STDERR);
		params.add(SUB_DET_CMP);
		return params;
	}

	/**
	 * @return An ArrayList of all the parameters needed to send a create
	 *         submission request
	 */
	private static ArrayList<String> getCreateSubmissionParams() {
		ArrayList<String> params = new ArrayList<String>();
		params.add(CRT_SUB_SRC);
		params.add(CRT_SUB_LAN);
		params.add(CRT_SUB_IN);
		params.add(CRT_SUB_RUN);
		params.add(CRT_SUB_PRIV);
		return params;
	}

	/**
	 * This method returns the code of the language, as it is represented on the
	 * IDEone API
	 * 
	 * @param language
	 *            the language you want its' code
	 * @return the code, as an int, of the wanted language
	 */
	public int getLanguagesCode(String language) {
		return LANGUAGES_CODE.get(language.toLowerCase().trim());

	}

	
	/*
	 * Adding for quick debugging. Run this class separately as a Java class to call the ide test function. 
	 * */
	public static void main(String[] args) {
		IDEoneInitializer testInitiatlizer = new IDEoneInitializer();
		testInitiatlizer.initialize();
		System.out.println(testInitiatlizer.testFunc());
		System.out.println(testInitiatlizer.getLanguagesCode("Python"));
	}

}
/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.connectors;

import java.util.ArrayList;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;
import com.googlecode.objectify.util.DAOBase;

import edu.mit.meet.meetag.entities.Course;
import edu.mit.meet.meetag.entities.Group;
import edu.mit.meet.meetag.entities.Lesson;
import edu.mit.meet.meetag.entities.MailBox;
import edu.mit.meet.meetag.entities.Organization;
import edu.mit.meet.meetag.entities.Settings;
import edu.mit.meet.meetag.entities.User;

/**
 * This class is the DAO class - a connector to the database. This is a
 * singleton.
 * 
 * @author George Khoury
 * 
 */
public class DAO extends DAOBase {

	/**
	 * the singleton itself
	 */
	private static final DAO instance = new DAO();

	private DAO() {
	}

	/**
	 * Static block of code that register all the entities to the database. All
	 * the entities that are stored in the database should be register here.
	 */
	static {
		ObjectifyService.register(edu.mit.meet.meetag.entities.Message.class);
		ObjectifyService.register(MailBox.class);
		ObjectifyService.register(User.class);
		ObjectifyService.register(Group.class);
		ObjectifyService.register(Lesson.class);
		ObjectifyService.register(Course.class);
		ObjectifyService.register(Organization.class);
		// ObjectifyService.register(Component.class);
		// ObjectifyService.register(TextQuestion.class);
		// ObjectifyService.register(Settings.class);
	}

	/**
	 * A static getInstance method that returns the singleton
	 * 
	 * @return
	 */
	public static DAO getDAO() {
		return instance;
	}

	/*
	 * 
	 * ======= User section =======
	 */

	/**
	 * Returns the user by its id, if not found return null
	 * 
	 * @param email
	 *            the id of the wanted user
	 * @return a user object if the user was found, null otherwise
	 */
	public User getUser(String email) {
		Objectify objectify = ObjectifyService.begin();
		return objectify.find(User.class, email);
	}

	/**
	 * This method takes a User. If it already exists in the database it updates
	 * it, otherwise it adds it to the databas Assumes that the input in not
	 * null.
	 * 
	 * @param lesson
	 *            The user to add/update
	 */
	public void addAndUpdateUser(User user) {
		Objectify objectify = ObjectifyService.begin();
		objectify.put(user);
	}

	/**
	 * @brief This method delete user from the database. Assumes that the input
	 *        is valid (i.e. not null).
	 * @param user
	 *            - the user to delete.
	 */
	public void deleteUser(User user) {
		Objectify objectify = ObjectifyService.begin();
		objectify.delete(user);
	}

	/**
	 * @brief Deletes the user by its email.
	 * @param email
	 *            - the id of the wanted user.
	 */
	public void deleteUser(String email) {
		Objectify objectify = ObjectifyService.begin();
		objectify.delete(objectify.find(User.class, email));
	}

	/**
	 * @brief Validate user finds the user in the databse (by it's email) and
	 *        validates. That the given password is like the one iin the
	 *        database.
	 * @param email
	 *            - the email of the user.
	 * @param password
	 *            - the password of the user.
	 * @return the user id the given password matches the one in the database
	 *         (if the user exists), Otherwise null.
	 */
	public User validateUser(String email, String password) {
		Objectify objectify = ObjectifyService.begin();

		if (email != null && !email.equals("") && password != null
				&& !password.equals("")) { // Checks for valid parameters, and
											// then search for the user in the
											// database:
			User usr = objectify.find(User.class, email.toLowerCase().trim());

			if ((usr != null)
					&& (usr.getPassword().equals(User.getEncryptedPassword(
							password, email)))) { // Checks validation of
													// (encrypted) password:
				System.out.println("Successful validation");
				return usr;
			}
		}

		System.out.println("Failed login attempt:\n" + "email: " + email
				+ " Password:" + password);
		return null;
	}

	/**
	 * @brief This method returns an ArrayList of radix users in the database,
	 *        starting from location index.
	 * @param index
	 *            - which location to start from.
	 * @param radix
	 *            - amount of users to get.
	 * @return ArrayList of radix users, starting from the given index.
	 */
	public ArrayList<User> getUsers(int index, int radix) {
		ArrayList<User> users = new ArrayList<User>();
		Objectify objectify = ObjectifyService.begin();
		Query<User> objectifyUsers = objectify.query(User.class).offset(index)
				.limit(radix);

		for (User u : objectifyUsers) {
			users.add(u);
		}

		return users;
	}

	/*
	 * ======= Lesson section =======
	 */

	/**
	 * @brief Returns a lesson by the given id.
	 * @param id
	 *            - the lesson to get.
	 * @return A lesson in the database with the diven id, or null if does not
	 *         exist
	 */
	public Lesson getLesson(Long id) {
		Objectify objectify = ObjectifyService.begin();
		return objectify.find(Lesson.class, id);
	}

	/**
	 * @brief This method takes a lesson. If it already exists in the database
	 *        it updates it, Otherwise it adds it to the databas Assumes that
	 *        the input in not null.
	 * @param lesson
	 *            - The lesson to add/update.
	 */
	public void addAndUpdateLesson(Lesson lesson) {
		Objectify objectify = ObjectifyService.begin();
		objectify.put(lesson);
	}

	/*
	 * ======= Course section =======
	 */

	/**
	 * @brief Returns the course by its id, if not found return null.
	 * @param id
	 *            - the id of the wanted course.
	 * @return The course if was found in the database, null otherwise.
	 */
	public Course getCourse(long id) {
		Objectify objectify = ObjectifyService.begin();
		return objectify.find(Course.class, id);
	}

	/**
	 * @brief This method takes a course. If it already exists in the database
	 *        it updates it, Otherwise it adds it to the databas Assumes that
	 *        the input in not null.
	 * @param course
	 *            - The course to add/update.
	 */
	public void addAndUpdateCourse(Course course) {
		Objectify objectify = ObjectifyService.begin();
		objectify.put(course);
	}

	/**
	 * @brief This method returns an ArrayList of radix courses in the database,
	 *        starting from location index.
	 * @param index
	 *            - which location to start from.
	 * @param radix
	 *            - amount of users to get.
	 * @return ArrayList of radix courses, starting from the given index
	 */
	public ArrayList<Course> getCourses(int index, int radix) {
		ArrayList<Course> courses = new ArrayList<Course>();
		Objectify objectify = ObjectifyService.begin();
		Query<Course> crs = objectify.query(Course.class).offset(index)
				.limit(radix);

		for (Course course : crs) {
			courses.add(course);
		}

		return courses;
	}

	/**
	 * @return ArrayList of all the courses in the database
	 */
	public ArrayList<Course> getAllCourses() {
		ArrayList<Course> courses = new ArrayList<Course>();
		Objectify objectify = ObjectifyService.begin();
		Query<Course> crs = objectify.query(Course.class).offset(0);

		for (Course course : crs) {
			courses.add(course);
		}
		return courses;
	}

	/*
	 * ======= Settings section =======
	 */

	/**
	 * @brief This method takes a setting set. If it already exists in the
	 *        database it updates it, Otherwise it adds it to the databas
	 *        Assumes that the input in not null.
	 * @param settings
	 *            - The settings set to add/update.
	 * @param type
	 *            - The type of the given setting set.
	 */
	public void addAndUpdateSettingsSet(Settings settings) {
		Objectify objectify = ObjectifyService.begin();
		objectify.put(settings);
	}

	/*
	 * ======= Organization section =======
	 */

	/**
	 * @brief Deletes the organization by its id.
	 * @param id
	 *            - The id of the organization you want to delete.
	 */
	public void deleteOrganization(Long id) {
		Objectify objectify = ObjectifyService.begin();
		objectify.delete(objectify.find(Organization.class, id));
	}

	/**
	 * @brief Returns the organization by its id, if not found return null.
	 * @param id
	 *            - the id of the wanted organization.
	 * @return The organization if was found in the database, null otherwise.
	 */
	public Organization getOrganization(Long id) {
		Objectify objectify = ObjectifyService.begin();
		return objectify.find(Organization.class, id);
	}

	/**
	 * @brief This method takes a organization. If it already exists in the
	 *        database it updates it, otherwise it adds it to the databas
	 *        Assumes that the input is not null.
	 * @param orginization
	 *            - The organization to add/update.
	 */
	public void addAndUpdateOrganization(Organization orginization) {
		Objectify objectify = ObjectifyService.begin();
		objectify.put(orginization);
	}

	/**
	 * @brief This method returns an ArrayList of radix organizations in the
	 *        database, starting from location index.
	 * @param index
	 *            - which location to start from.
	 * @param radix
	 *            - amount of users to get.
	 * @return ArrayList of radix organizations, starting from the given index.
	 */
	public ArrayList<Organization> getOrganizations(int index, int radix) {
		ArrayList<Organization> organizations = new ArrayList<Organization>();
		Objectify objectify = ObjectifyService.begin();
		Query<Organization> orgs = objectify.query(Organization.class)
				.offset(index).limit(radix);

		for (Organization org : orgs) {
			organizations.add(org);
		}

		return organizations;
	}
}
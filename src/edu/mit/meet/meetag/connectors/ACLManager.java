/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.connectors;

import java.util.HashMap;
import java.util.Map;

import edu.mit.meet.meetag.controllers.GeneralController;
import edu.mit.meet.meetag.entities.User;
import edu.mit.meet.meetag.enumurations.Role;
import edu.mit.meet.meetag.enumurations.SiteStatus;

public class ACLManager {
	private Map<String, Role> acl;
	private static final ACLManager INSTANCE = new ACLManager();

	private ACLManager() {
		acl = new HashMap<String, Role>();

		// Guest role - These permissions are provisioned to non-logged in
		// users.

		acl.put("index", Role.Guest);
		acl.put("indexre", Role.Guest);
		acl.put("login", Role.Guest);
		acl.put("about", Role.Guest);
		acl.put("register", Role.Guest);
		acl.put("invite", Role.Guest);
		acl.put("contact", Role.Guest);
		acl.put("403", Role.Guest);
		acl.put("404", Role.Guest);
		acl.put("503", Role.Guest);
		acl.put("offline", Role.Guest);
		acl.put("help", Role.Guest);
		acl.put("status", Role.Guest);
		acl.put("developers", Role.Guest);
		acl.put("features", Role.Guest);
		acl.put("feedback", Role.Guest);
		acl.put("", Role.Guest); // Handling default (index.jsp)
		acl.put("testingcustomtag", Role.Guest);

		// User role - These permissions are provisioned to regular users.

		acl.put("lessonview", Role.User);
		acl.put("textComponent", Role.User);
		acl.put("course", Role.User);
		acl.put("courses", Role.User);
		acl.put("profile", Role.User);
		acl.put("messages", Role.User);
		acl.put("lessons", Role.User);
		acl.put("home", Role.User);
		acl.put("security", Role.User);

		// Contributor role - These permissions to content creators.

		acl.put("creator", Role.Contributor);
		acl.put("wizard", Role.Contributor);
		acl.put("lessoncreator", Role.Contributor);

		// Administration
		acl.put("administration", Role.Administrator);
	}

	/**
	 * This method gives you back the singleton instance of ACLManager
	 * 
	 * @return ACLManager instance
	 */
	public static ACLManager getInstance() {
		return ACLManager.INSTANCE;
	}

	/**
	 * @brief This method returns true if the User's role allows them to access
	 *        the resource referred to by the address and false if otherwise.
	 * @param address
	 *            -
	 * @param role
	 * @return
	 * @author
	 * @modified
	 * @modifier
	 */
	public boolean permitAccess(String address, Role role) {
		if (SiteStatus.valueOf(ConfigurationManager.getInstance().getSetting(
				SiteStatus.SITE_STATUS_KEY)) == SiteStatus.Testing) {
			return true;
		}

		if (acl.containsKey(address) && role.allowedAccess(acl.get(address))) {
			return true;
		}
		return false;
	}

	/**
	 * @brief This method cross references the address against the list of pages
	 *        listed in 'acl' map. If the page exists true is returned.
	 * 
	 * @param address
	 *            - The address of the page you wish to check.
	 * @return boolean - true if page exists
	 */
	public boolean pageExists(String address) {
		if (SiteStatus.valueOf(ConfigurationManager.getInstance().getSetting(
				SiteStatus.SITE_STATUS_KEY)) == SiteStatus.Testing) {
			return true;
		}

		if (acl.containsKey(address)) {
			return true;
		}
		return false;
	}

	/**
	 * @brief Checks if a user is authenticated with a minimum access level of
	 *        --> 'Administrator.
	 * 
	 * @return boolean - true if the user is 'Administrator'
	 */
	public static boolean adminCheck() {
		User user = (User) GeneralController.session().getAttribute("User");

		if (user != null && user.getRole().allowedAccess(Role.Administrator)) {
			return true;
		}
		return false;
	}

	/**
	 * @brief Checks if a specific 'User Role' is allowed access.
	 * 
	 * @return boolean - true if the user is role permits user access.
	 */
	public static boolean allowedAccess(Role role) {
		User user = (User) GeneralController.session().getAttribute("User");

		if (user != null && user.getRole().allowedAccess(role)) {
			return true;
		}
		return false;
	}
}

/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.connectors;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.google.apphosting.api.ApiProxy;

/**
 * This class acts as a centralized mailer.
 * 
 * It handles emails such as
 * 
 * - Verfication Emails - Contact Emails
 * 
 * @author George Khoury
 * 
 */

public class EmailSender {

	/**
	 * Must fill the following demands to work, this is why my email is
	 * hardcoded To set the sender address, the app calls the setFrom() method
	 * on the MimeMessage object. The sender address must be one of the
	 * following types:
	 * 
	 * he address of a registered administrator for the application The address
	 * of the user for the current request signed in with a Google Account. You
	 * can determine the current user's email address with the Users API. The
	 * user's account must be a Gmail account, or be on a domain managed by
	 * Google Apps. Any valid email receiving address for the app (such as
	 * xxx@APP-ID.appspotmail.com).
	 * 
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param URL
	 */
	public static void sendVerificationEmail(String fromAddr, String email,
			String firstName, String lastName, String URL) {

		Message msg = new MimeMessage(
				Session.getDefaultInstance(new Properties()));
		String emailBody = "<h1>Welcome to SIMPLEx!</h1>/nHi George, SIMPLEx needs to verify your email address before you can login. Please verify your email address by clicking the link below. <a href=\""
				+ URL
				+ "\""
				+ ">Verify your email address</a> \n- The SIMPLEx Team \n";
		try {
			msg.setFrom(new InternetAddress(fromAddr, "SIMPLEx"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					email, firstName + " " + lastName));
			msg.setSubject("Verify your SIMPLEx account!");
			msg.setContent(emailBody, "text/html");

			Transport.send(msg);

		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This message sends an email to 'info@thesimplex.co', after the 'Contact Us' form has been submitted. 
	 * 
	 * @param messageBody
	 *            the body of the email
	 * @param fromAddr
	 *            The address of the sender
	 * @param fullName
	 *            the full name of the sender
	 * @param query
	 *            the type of message
	 */
	public void sendContactUs(String query, String messageBody) {
		Message msg = new MimeMessage(
				Session.getDefaultInstance(new Properties()));

		try {
			// Gets the application id on runtime
			ApiProxy.Environment env = ApiProxy.getCurrentEnvironment();
			msg.setFrom(new InternetAddress("no-reply@" + env.getAppId()
					+ ".appspotmail.com", "Contact Us"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"info@thesimplex.co", "SIMPLEx"));
			msg.setSubject("[SIMPLEx] Contact Us Form -- " + query);
			msg.setContent(messageBody, "text/html");
			Transport.send(msg);

		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

}

/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.tagsupport;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.el.ELException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.codec.binary.Base64;

import edu.mit.meet.meetag.connectors.DAO;
import edu.mit.meet.meetag.controllers.GeneralController;
import edu.mit.meet.meetag.entities.User;

@SuppressWarnings({ "deprecation", "serial" })
public class UserTag extends TagSupport {
	private User user;
	private String email;
	private String format;

	public void setEmail(String email){
		this.email=email;
	}
	public void setFormat(String format){
		this.format=format;
	}
	public int doStartTag() throws JspException{
		try{
			JspWriter out = pageContext.getOut();
			String codeHTML="";
			user = DAO.getDAO().getUser(email);
			Scanner sc ;
			String att = Base64.encodeBase64String(user.getEmail().getBytes()).substring(3, 13).toLowerCase();
			System.out.println("att:"+att);
			if (user!=null){
				if(format.equals("profile")){
					sc= new Scanner(new File(GeneralCustomTag.loadPath("profile")));			
					GeneralController.session().setAttribute(att,user );
				}else if(format.equals("adminstratorPage")){
					sc= new Scanner(new File(GeneralCustomTag.loadPath("useradminstratorpage")));
				}else{
					sc= new Scanner(new File(GeneralCustomTag.loadPath("unknownformat")));
				}
			}else{
				sc = new Scanner(new File(GeneralCustomTag.loadPath("users404")));
			}
			while(sc.hasNextLine())
				codeHTML += sc.nextLine();
			codeHTML = codeHTML.replaceAll("User", att);
			codeHTML = (String) pageContext.getExpressionEvaluator().evaluate(codeHTML, String.class, pageContext.getVariableResolver(),null);
			out.print(codeHTML);
			sc.close();
		}catch (IOException e){
			throw new JspException("Error: "+e.getMessage());
		} catch (ELException e) {
			e.printStackTrace();
		}
		
		return SKIP_BODY;
	}
	public int doEndTag() throws JspException{
		return EVAL_PAGE;
	}

}

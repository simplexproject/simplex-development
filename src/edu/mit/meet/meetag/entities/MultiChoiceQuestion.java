/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.util.ArrayList;

import com.googlecode.objectify.annotation.Subclass;

import edu.mit.meet.meetag.enumurations.QuestionType;

/**
 * @author Matan Mates matan@thesimplex.co
 * @version 0.1
 * @since 2012-08-08
 */

@Subclass
public class MultiChoiceQuestion extends Component {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5689799780398996079L;
	// Fields
	private String title; // Questions title
	private String question; // Question text (Actual Question)
	private ArrayList<String> choices;
	private String correctAnswer; // The correct answer which will be used to
									// compare the user's input to

	/**
	 * @param title
	 * @param question
	 * @param type
	 * @param correctAnswer
	 */
	public MultiChoiceQuestion(int id, boolean required, String title,
			String question, QuestionType type, String answer,
			String correctAnswer, ArrayList<String> choices) {
		super(id, required, null);
		setView("/WEB-INF/views/multiChoiceQuestionComponent.jsp");
		this.title = title;
		super.getParams().put("title", title);
		this.question = question;
		super.getParams().put("question", question);
		this.correctAnswer = correctAnswer;
		this.choices = choices;
		super.getParams().put("choices", choices);

	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
		super.getParams().put("title", title);
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question
	 *            the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
		super.getParams().put("question", question);
	}

	/**
	 * @return the correctAnswer
	 */
	public String getCorrectAnswer() {
		return correctAnswer;
	}

	/**
	 * @param correctAnswer
	 *            the correctAnswer to set
	 */
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	/**
	 * @return the choices
	 */
	public ArrayList<String> getChoices() {
		return choices;
	}

	/**
	 * @param choices
	 *            the choices to set
	 */
	public void setChoices(ArrayList<String> choices) {
		this.choices = choices;
		super.getParams().put("choices", choices);

	}

	// @Override
	// public View rednerComponent() {
	// return null;
	// }

}

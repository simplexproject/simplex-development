/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Id;

import org.apache.commons.codec.binary.Base64;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Serialized;

import edu.mit.meet.meetag.enumurations.Role;

/**
 * @author George Khoury
 * 
 */

@Entity(name = "User")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7385020724608587665L;
	// Entity Fields
	private String firstName;
	private String lastName;
	private boolean verified;
	private String avatar;
	@Id
	private String email;
    private String password;
	
	@Serialized
	private List<Long> enrolledCourses;
	private boolean regularLogin;
	private String gender;
	private String birthDate;
	private String status;
	private Role role;
	private Key<Channel>[] channels;
	private Key<User>[] friends;
	private Key<Award>[] awards;
	@javax.persistence.Embedded
	private MailBox mailbox;

	/**
	 * Empty constructor
	 */
	public User() {
		super();
		role = Role.User;
		this.enrolledCourses = new ArrayList<Long>();
	}

	/**
	 * Basic constructor
	 * 
	 * @param username
	 *            the username of the user
	 * @param password
	 *            the password of the user
	 * @param firstName
	 *            the user first name
	 * @param lastName
	 *            the user last name
	 * @param avatar
	 *            the location of the user avatar's
	 * @param email
	 *            the email address of the user
	 * @param gender
	 *            the gender of the user (Male of Female)
	 * @param birthDate
	 *            the user birth date (Formatted in dd/mm/yyy)
	 */
	public User(String email, String password, String firstname,
			String lastname, String avatar, String gender, String birthDate,
			Role role) {
		super();
		this.role = role;
		this.password = password;
		this.firstName = firstname;
		this.lastName = lastname;
		this.email = email;
		this.verified = false;
		this.avatar = avatar;
		this.gender = gender;
		this.birthDate = birthDate;
		this.enrolledCourses = new ArrayList<Long>();
		this.mailbox = new MailBox(email);
	}
	
	public void enrollInCourse(Long courseId){
		if(!this.enrolledCourses.contains(courseId)){
			enrolledCourses.add(courseId);
		}
	}
	public void unenrollFromCourse(Long courseId){
			enrolledCourses.remove(courseId);
	}
	public boolean isEnrolled(Long courseId){
		return enrolledCourses.contains(courseId);
	}
	
	public List<Long> getEnrolledCourses() {
		return enrolledCourses;
	}

	public void setEnrolledCourses(List<Long> enrolledCourses) {
		this.enrolledCourses = enrolledCourses;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the birthdate
	 */
	public String getBirthdate() {
		return birthDate;
	}

	/**
	 * @param birthdate
	 *            the birthdate to set
	 */
	public void setBirthdate(String birthdate) {
		this.birthDate = birthdate;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar
	 *            the avatar to set
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * @return the channels
	 */
	public Key<Channel>[] getChannels() {
		return channels;
	}

	/**
	 * @param channels
	 *            the channels to set
	 */
	public void setChannels(Key<Channel>[] channels) {
		this.channels = channels;
	}

	/**
	 * @return the friends
	 */
	public Key<User>[] getFriends() {
		return friends;
	}

	/**
	 * @param friends the friends to set
	 */
	public void setFriends(Key<User>[] friends) {
		this.friends = friends;
	}

	/**
	 * @return the awards
	 */
	public Key<Award>[] getAwards() {
		return awards;
	}

	/**
	 * @param awards the awards to set.
	 */
	public void setAwards(Key<Award>[] awards) {
		this.awards = awards;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	/**
	 * @return the creationIP
	 */

	public String getBirthDate() {
		return birthDate;
	}

	/**
	 * @return the regularLogin
	 */
	public boolean isRegularLogin() {
		return regularLogin;
	}

	/**
	 * @param regularLogin the regularLogin to set.
	 */
	public void setRegularLogin(boolean regularLogin) {
		this.regularLogin = regularLogin;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return	the mailbox.
	 */
	public MailBox getMailbox() {
		return mailbox;
	}

	/**
	 * @param	mailbox	-	the mailbox to set.
	 */
	public void setMailbox(MailBox mailbox) {
		this.mailbox = mailbox;
	}

	
	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName 
				+ ", email=" + email + ", password=" + password + ", avatar="
				+ avatar + ", preferences=" + ", channels=" + channels
				+ ", friends=" + friends + ", awards=" + awards + "]";
	}

	/**
	 * This method encrypts the user password
	 * 
	 * @param password
	 * @param salt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	public static String getEncryptedPassword(String password, String salt) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.reset();

			md.update(salt.getBytes());
			return new String(Base64.encodeBase64(md.digest(password
					.getBytes("UTF-8"))));

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;

		}
	}

	public void activateUser() {
		this.verified = true;
	}

	/**
	 * @brief	This method encrypts the user password.
	 * @param	password	-	
	 * @param	salt		-	
	 * @return	String		-	
	 * @throws	NoSuchAlgorithmException
	 * @throws	UnsupportedEncodingException
	 */
	public static String getVerificationKey(String email) {
		try {
			MessageDigest m = MessageDigest.getInstance("SHA-256");
			m.update(email.getBytes(), 0, email.length());
			String key = new BigInteger(1, m.digest()).toString(16);

			return key;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
}

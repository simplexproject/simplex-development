/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.util.Locale;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import edu.mit.meet.meetag.connectors.ACLManager;
import edu.mit.meet.meetag.connectors.ConfigurationManager;
import edu.mit.meet.meetag.enumurations.SiteStatus;

/**
 * @author George Khoury george@thesimplex.co
 * @version 0.1
 * @since 2012-07-30
 */
public class SimplexViewResolver extends InternalResourceViewResolver {

	/*
	 * In the constructor we are setting the default suffix and prefix for the
	 * internal view resolution
	 */
	public SimplexViewResolver() {

		this.setPrefix("/WEB-INF/views/");
		this.setSuffix(".jsp");
		// this.set
	}

	// Override the view resolver, put in a clause to catch home page.u
	@Override
	public View resolveViewName(String viewName, Locale locale) throws Exception
	{
		String siteStatus = ConfigurationManager.getInstance().getSetting(SiteStatus.SITE_STATUS_KEY);
		System.out.println(siteStatus);
		String lowerViewName = viewName.toLowerCase(); 
		
		if (siteStatus != null && !ACLManager.adminCheck()) {
			if (!lowerViewName.startsWith("login")) {
				if ( SiteStatus.valueOf(siteStatus) == SiteStatus.Offline) {	
					return super.resolveViewName("offline", locale);
				}
				else if (SiteStatus.valueOf(siteStatus) == SiteStatus.Beta &&
						!lowerViewName.startsWith("invite") && !lowerViewName.equals("")
						&& !lowerViewName.startsWith("about") && !lowerViewName.startsWith("features")
						&& !lowerViewName.startsWith("index") && !lowerViewName.startsWith("contact") )
				{	
					return super.resolveViewName("invite", locale);
				}
			}
		}
		
		if (viewName.equals("")) {
			return super.resolveViewName("index", locale);
		}
	
		return super.resolveViewName(viewName, locale);
	}
}
/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

/**
 * @author George Khoury
 * 
 */

public class Editor {

	// Entity Fields
	
	private int width;
	private int height;


	/**
	 * Empty constructor
	 */
	public Editor() {
		super();
	}


	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}


	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}


	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}


	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	
	

}
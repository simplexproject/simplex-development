/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;


import org.springframework.web.servlet.ModelAndView;

public class UnitView extends ModelAndView{
	public UnitView(boolean success){
		super("BooleanView");
		String testStatus;
		addObject("testName", "Unknown Test");
		testStatus = fromBoolean(success);
		addObject("testStatus",testStatus);
	}
	public UnitView(boolean success,String testName){
		super("BooleanView");
		String testStatus;
		addObject("testName", testName);
		testStatus = fromBoolean(success);
		addObject("testStatus",testStatus);
	}
	
	public UnitView(boolean success,String testName,String extraData){
		super("BooleanView");
		String testStatus;
		addObject("testName", testName);
		testStatus = fromBoolean(success);
		addObject("testStatus",testStatus);
		addObject("extraData",extraData);
	}
	
	
	
//	public UnitView(TestSuite testSuite){
//		TestResult res = new TestResult();
//		testSuite.run(res);
//	}
	
	private static String fromBoolean(boolean state){
		if(state)
			return "Passed";
		return "Failed";
	}
	
	
}

/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.util.Set;

import edu.mit.meet.meetag.enumurations.Role;

public class URLMapping {
	private String path;
	private Set<Role> allowedRoles;

	public URLMapping(String path, Set<Role> allowedRoles) {
		this.path = path;
		this.allowedRoles = allowedRoles;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return the allowedRoles
	 */
	public Set<Role> getAllowedRoles() {
		return allowedRoles;
	}
	public boolean allowedAccess(Role role){
		if(allowedRoles.contains(role))
			return true;
		return false;
	}

	
}

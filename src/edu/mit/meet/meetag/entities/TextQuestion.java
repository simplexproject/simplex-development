/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import com.googlecode.objectify.annotation.Subclass;

import edu.mit.meet.meetag.enumurations.QuestionType;

/**
 * @author George Khoury george@thesimplex.co
 * @version 0.1
 * @since 2012-07-25
 */

@Subclass
public class TextQuestion extends Component {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields
	private int id;
	private String title; // Questions title
	private String question; // Question text (Actual Question)
	private QuestionType type; // Question type based on enum QuestionType
	private String correctAnswer; // The correct answer which will be used to
									// compare the user's input to

	/**
	 * @param title
	 * @param question
	 * @param type
	 * @param correctAnswer
	 */
	public TextQuestion(int id, boolean required, String title,
			String question, QuestionType type, String answer,
			String correctAnswer) {
		super(id, required, null);
		setView("/WEB-INF/views/textQuestion.jsp");
		this.id = id;
		super.getParams().put("id", id);
		this.title = title;
		super.getParams().put("title", title);
		this.question = question;
		super.getParams().put("question", question);
		this.type = type;
		this.correctAnswer = correctAnswer;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
		super.getParams().put("title", title);
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question
	 *            the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
		super.getParams().put("question", question);
	}

	/**
	 * @return the type
	 */
	public QuestionType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(QuestionType type) {
		this.type = type;
	}

	/**
	 * @return the correctAnswer
	 */
	public String getCorrectAnswer() {
		return correctAnswer;
	}

	/**
	 * @param correctAnswer
	 *            the correctAnswer to set
	 */
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
		super.getParams().put("id", id);
	}

	// @Override
	// public View rednerComponent() {
	// return null;
	// }

}
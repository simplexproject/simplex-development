/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.util.Arrays;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.googlecode.objectify.Key;

@Entity(name = "Group")
public class Group {
	@Id
	private Long id;
	private String name;
	private Key<User>[] managers;
	private Key<User>[] users;
	private Key<Post>[] posts;

	public Group() {
		super();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the managers
	 */
	public Key<User>[] getManagers() {
		return managers;
	}

	/**
	 * @param managers
	 *            the managers to set
	 */
	public void setManagers(Key<User>[] managers) {
		this.managers = managers;
	}

	/**
	 * @return the users
	 */
	public Key<User>[] getUsers() {
		return users;
	}

	/**
	 * @param users
	 *            the users to set
	 */
	public void setUsers(Key<User>[] users) {
		this.users = users;
	}

	/**
	 * @return the posts
	 */
	public Key<Post>[] getPosts() {
		return posts;
	}

	/**
	 * @param posts
	 *            the posts to set
	 */
	public void setPosts(Key<Post>[] posts) {
		this.posts = posts;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", name=" + name + ", managers="
				+ Arrays.toString(managers) + ", users="
				+ Arrays.toString(users) + ", posts=" + Arrays.toString(posts)
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		return (int) (prime * (id == null ? 0 : id));
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass()) {
			return false;
		}
		return (id == (((Group) obj).id));
	}

}

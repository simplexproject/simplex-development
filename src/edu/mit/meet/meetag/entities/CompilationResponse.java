/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.util.Map;

/**
 * @author George Khoury george@thesimplex.co
 * @version 0.1
 * @since 2012-08-28
 */

public class CompilationResponse {

	private boolean compiledSuccessfully; // IDECode Result = 12,13,15
	private boolean ranSuccessfully; // IDECode Result = 15
	private String programOutput; // The output returned after IDEOne runs the code
	private String programErrors; // The error returned after IDEOne runs the
									// code and encounters an error
	private int memory; // The amount of memory the program took to run

	/**
	 * @param compiledSuccessfully
	 * @param ranSuccessfully
	 */
	public CompilationResponse(boolean compiledSuccessfully,
			boolean ranSuccessfully) {
		super();
		this.compiledSuccessfully = compiledSuccessfully;
		this.ranSuccessfully = ranSuccessfully;
	}

	/**
	 * @return the compiledSuccessfully
	 */
	public boolean isCompiledSuccessfully() {
		return compiledSuccessfully;
	}

	/**
	 * @param compiledSuccessfully
	 *            the compiledSuccessfully to set
	 */
	public void setCompiledSuccessfully(boolean compiledSuccessfully) {
		this.compiledSuccessfully = compiledSuccessfully;
	}

	/**
	 * @return the ranSuccessfully
	 */
	public boolean isRanSuccessfully() {
		return ranSuccessfully;
	}

	/**
	 * @param ranSuccessfully
	 *            the ranSuccessfully to set
	 */
	public void setRanSuccessfully(boolean ranSuccessfully) {
		this.ranSuccessfully = ranSuccessfully;
	}

	/**
	 * @return the programOutput
	 */
	public String getProgramOutput() {
		return programOutput;
	}

	/**
	 * @param programOutput
	 *            the programOutput to set
	 */
	public void setProgramOutput(String programOutput) {
		this.programOutput = programOutput;
	}

	/**
	 * @return the programErrors
	 */
	public String getProgramErrors() {
		return programErrors;
	}

	/**
	 * @param programErrors
	 *            the programErrors to set
	 */
	public void setProgramErrors(String programErrors) {
		this.programErrors = programErrors;
	}

	/**
	 * @return the memory
	 */
	public int getMemory() {
		return memory;
	}

	/**
	 * @param memory
	 *            the memory to set
	 */
	public void setMemory(int memory) {
		this.memory = memory;
	}

	public static CompilationResponse IDEOneFactory(
			Map<String, String> IDEOneResponse) {

		boolean compiledSuccessfully = false;
		boolean ranSuccessfully = false;

		int statusCode = Integer.parseInt(IDEOneResponse.get("result"));
		
		// Defining status of compiledSuccessfully
		if (statusCode == 12 || statusCode == 13 || statusCode == 15) {
			compiledSuccessfully = true;
		}

		// Defining status of ranSuccessfully
		if (statusCode == 15) {
			ranSuccessfully = true;
		}

		CompilationResponse response = new CompilationResponse(
				compiledSuccessfully, ranSuccessfully);
		
		if(compiledSuccessfully){
			response.setProgramErrors(IDEOneResponse.get("stderr"));
			response.setProgramOutput(IDEOneResponse.get("output"));
			response.setMemory(Integer.parseInt(IDEOneResponse.get("memory")));
		}
		return response;
	}

}

/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import javax.persistence.Id;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Serialized;


@Entity(name = "Lesson")
public class Lesson {

	@Id
	private Long id;
	private String title;
	private String description;

	// private Key<User>[] managers;
	@Serialized
	private Component[] components;

	public Lesson() {
		super();
	}

	public Lesson(long id, String name, String description) {
		this.id = id;
		this.title = name;
		this.description = description;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param descirption
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		return (int) (prime * (id == null ? 0 : id));
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass()) {
			return false;
		}
		return (id == (((Lesson) obj).id));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Lesson [id=" + id + ", title=" + title + ", description="
				+ description + "]";
	}


	/**
	 * @return the components
	 */
	public Component[] getComponents() {

		return components;
	}

	/**
	 * @param components
	 *            the components to set
	 */
	public void setComponents(Component[] components) {
		this.components = components;
	}

}

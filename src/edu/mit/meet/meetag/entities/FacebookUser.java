/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import com.google.gson.Gson;

public class FacebookUser {

	private long id;
	private String first_name;
	private String last_name;
	private String gender;
	private String email;
	private String birthday;

	public FacebookUser() {
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * @param first_name
	 *            the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * @param last_name
	 *            the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	

	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FacebookUser [id=" + id + ", first_name=" + first_name
				+ ", last_name=" + last_name + ", gender=" + gender
				+ ", email=" + email + "]";
	}

	public static String getAccessToken(String code, String localName) {
		try
		{
			String url = "https://graph.facebook.com/oauth/access_token?client_id=";
			
			if(localName.equals("127.0.0.1"))
			{
				url += "458368827546920&redirect_uri=http://localhost:8888/login/facebook&client_secret=1795d6a3d1b9a9a1ccbfeef8817fd78c&code=" + code;
			}
			else
			{
				url += "323904147673897&redirect_uri=http://www.thesimplex.co/login/facebook&client_secret=64260fa9ef6afc1f294dd92b737b2ccc&code=" + code;
			}
			
			URL urlObject = new URL(url);
			BufferedReader reader = new BufferedReader(new InputStreamReader(urlObject.openStream()));
			String line;
			String total = "";

			while ((line = reader.readLine()) != null)
			{
				total += line + '\n';
			}

			return total;
		} catch (Exception e) {
			System.out.println("Facebook Error");
		}

		return null;
	}

	public static FacebookUser getFacebookUser(String accessToken)
	{
		try
		{
			URL url = new URL("https://graph.facebook.com/me?"+accessToken);
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			String line;
			String total = "";

			while ((line = reader.readLine()) != null) {
				total += line + '\n';
			}

			Gson gson = new Gson();
			FacebookUser user = gson.fromJson(total, FacebookUser.class);
			return user;
		}
		catch (Exception e)
		{
			System.out.println("Facebook Error");
		}

		return null;
	}
}

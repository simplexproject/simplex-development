/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Id;

import com.googlecode.objectify.annotation.Entity;

import edu.mit.meet.meetag.connectors.DAO;

/**
 * @author Ahmed Shawish
 * @version 0.1
 * @since 2012-08-15
 */
@Entity(name = "box")
public class MailBox {

	// The id of MailBox
	@Id
	private String id;
	// The ArrayList that holds the user's received messages
	@javax.persistence.Embedded
	private List<Message> inbox;
	// The ArrayList that holds the user's sent messages
	@javax.persistence.Embedded
	private List<Message> outbox;

	public MailBox(String id) {
		inbox = new ArrayList<Message>();
		outbox = new ArrayList<Message>();
		this.id = id;
	}
	// Empty constructor for the DAO
	public MailBox() {
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	public void sendMessage(Message msg) {
		ArrayList<String> listOfR = msg.getRecipients();

		System.out.println("listOfR " + listOfR.size());

		for (String userID : listOfR) {
			User user = DAO.getDAO().getUser(userID);
			MailBox userMailBox = user.getMailbox();

			System.out.println("Mailbox retrieved for: " + user.getEmail()
					+ " & value= " + userMailBox);
		}
	}

	/**
	 * @return the inbox
	 */
	public List<Message> getInbox() {
		return inbox;
	}

	/**
	 * @return the outbox
	 */
	public List<Message> getOutbox() {
		return outbox;
	}

	/**
	 * @param inbox
	 *            the inbox to set
	 */
	public void setInbox(ArrayList<Message> inbox) {
		this.inbox = inbox;
	}

	/**
	 * @param outbox
	 *            the outbox to set
	 */
	public void setOutbox(ArrayList<Message> outbox) {
		this.outbox = outbox;
	}

	/**
	 * @param args
	 */
}
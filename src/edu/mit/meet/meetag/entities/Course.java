/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

/**
 * @author Matan
 */
package edu.mit.meet.meetag.entities;

import java.util.ArrayList;

import javax.persistence.Embedded;
import javax.persistence.Id;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Serialized;

import edu.mit.meet.meetag.enumurations.Language;
import edu.mit.meet.meetag.enumurations.Privacy;

@Entity(name = "Course")
public class Course {

	@Id
	private Long id;
	private String name;
	private String description;
	private String avatar;
	private int popularity;
	private Privacy privacy;
	// Needs to be String
	private ArrayList<User> managers;
	@Embedded
	private ArrayList<Lesson> lessons;
	private ArrayList<Group> groups;
	private ArrayList<Post> posts;
	@Serialized
	private Language language;
	public Course() {
		this.managers = new ArrayList<User>();
		this.lessons = new ArrayList<Lesson>();
		this.groups = new ArrayList<Group>();
		this.posts = new ArrayList<Post>();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar
	 *            the avatar to set
	 */

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the privacy
	 */
	public Privacy getPrivacy() {
		return privacy;
	}

	/**
	 * @param privacy
	 *            the privacy to set
	 */
	public void setPrivacy(Privacy privacy) {
		this.privacy = privacy;
	}

	/**
	 * @return the managers
	 */
	public ArrayList<User> getManagers() {
		return managers;
	}

	/**
	 * @param managers
	 *            the managers to set
	 */
	public void setManagers(ArrayList<User> managers) {
		this.managers = managers;
	}

	/**
	 * @return the lessons
	 */
	public ArrayList<Lesson> getLessons() {
		return lessons;
	}

	/**
	 * @param lessons
	 *            the lessons to set
	 */
	public void setLessons(ArrayList<Lesson> lessons) {
		this.lessons = lessons;
	}	

	/**
	 * @return the groups
	 */
	public ArrayList<Group> getGroups() {
		return groups;
	}

	/**
	 * @param groups
	 *            the groups to set
	 */
	public void setGroups(ArrayList<Group> groups) {
		this.groups = groups;
	}

	/**
	 * @return the posts
	 */
	public ArrayList<Post> getPosts() {
		return posts;
	}

	/**
	 * @param posts
	 *            the posts to set
	 */
	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the popularity
	 */
	public int getPopularity() {
		return popularity;
	}

	/**
	 * @param popularity
	 *            the popularity to set
	 */
	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}

	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", privacy=" + privacy
				+ ", managers=" + managers.toString() + ", lessons="
				+ lessons.toString() + ", groups=" + groups.toString()
				+ ", posts=" + posts.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		return (int) (prime * (id == null ? 0 : id));
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass()) {
			return false;
		}
		return (id == (((Course) obj).id));
	}

}

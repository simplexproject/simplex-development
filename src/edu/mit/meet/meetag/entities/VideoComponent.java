/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

@SuppressWarnings("serial")
public class VideoComponent extends Component {

	/**
	 * 
	 */
	private String title;
	private String description;
	private String width;
	private String height;
	private String videoID;

	public VideoComponent(int id, String description, String width,
			String height, String videoID) {
		super(id, false, "/WEB-INF/views/videoComponent.jsp");
		setTitle("Video");
		setDescription(description);
		setWidth(width);
		setHeight(height);
		setVideoID(videoID);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		getParams().put("title", "Video");
	}

	/**
	 * @return the width
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * @return the height
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * @return the iD
	 */
	public String getVideoID() {
		return videoID;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
		getParams().put("width", width);
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
		getParams().put("height", height);
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setVideoID(String videoID) {
		this.videoID = videoID;
		getParams().put("videoID", videoID);
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
		getParams().put("description", description);
	}

}
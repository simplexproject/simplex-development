/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.sql.Date;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;

@Entity(name = "Post")
public class Post {
	@Id
	private Long id;
	private Key<Post> paretnPost;
	private Key<Group> parentGroup;
	private Key<Course> parentCourse;
	private Key<User> publisher;
	private String title;
	private String content;
	private Date date;

	public Post() {
		super();
	}

	/**
	 * @return the paretnPost
	 */
	public Key<Post> getParetnPost() {
		return paretnPost;
	}

	/**
	 * @param paretnPost
	 *            the paretnPost to set
	 */
	public void setParetnPost(Key<Post> paretnPost) {
		this.paretnPost = paretnPost;
	}

	/**
	 * @return the parentGroup
	 */
	public Key<Group> getParentGroup() {
		return parentGroup;
	}

	/**
	 * @param parentGroup
	 *            the parentGroup to set
	 */
	public void setParentGroup(Key<Group> parentGroup) {
		this.parentGroup = parentGroup;
	}

	/**
	 * @return the parentCourse
	 */
	public Key<Course> getParentCourse() {
		return parentCourse;
	}

	/**
	 * @param parentCourse
	 *            the parentCourse to set
	 */
	public void setParentCourse(Key<Course> parentCourse) {
		this.parentCourse = parentCourse;
	}

	/**
	 * @return the publisher
	 */
	public Key<User> getPublisher() {
		return publisher;
	}

	/**
	 * @param publisher
	 *            the publisher to set
	 */
	public void setPublisher(Key<User> publisher) {
		this.publisher = publisher;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", paretnPost=" + paretnPost
				+ ", parentGroup=" + parentGroup + ", parentCourse="
				+ parentCourse + ", publisher=" + publisher + ", title="
				+ title + ", content=" + content + ", date=" + date + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		return (int) (prime * (id == null ? 0 : id));
	}


	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass()) {
			return false;
		}
		return (id == (((Post) obj).id));
	}

}

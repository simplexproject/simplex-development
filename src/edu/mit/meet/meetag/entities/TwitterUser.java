/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.gson.Gson;

public class TwitterUser {

	private String id;
	private String email;
	private boolean verified_email;
	private String name;
	private String given_name;
	private String family_name;
	private String link;
	private String picture;
	private String gender;
	private String locale;

	public TwitterUser() {

	}

	public TwitterUser(String id, String email, boolean verified_email,
			String name, String given_name, String family_name, String link,
			String picture, String gender, String locale) {
		super();
		this.id = id;
		this.email = email;
		this.verified_email = verified_email;
		this.name = name;
		this.given_name = given_name;
		this.family_name = family_name;
		this.link = link;
		this.picture = picture;
		this.gender = gender;
		this.locale = locale;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the verified_email
	 */
	public boolean isVerified_email() {
		return verified_email;
	}

	/**
	 * @param verified_email
	 *            the verified_email to set
	 */
	public void setVerified_email(boolean verified_email) {
		this.verified_email = verified_email;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the given_name
	 */
	public String getGiven_name() {
		return given_name;
	}

	/**
	 * @param given_name
	 *            the given_name to set
	 */
	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	/**
	 * @return the family_name
	 */
	public String getFamily_name() {
		return family_name;
	}

	/**
	 * @param family_name
	 *            the family_name to set
	 */
	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the picture
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * @param picture
	 *            the picture to set
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 *            the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	public static String getAccessToken(Boolean isLocal)
	{
		String oauthCallback;
		String oauthConsumerKey;
		String oauthNonce = "298a8b0c010ddbd905fef2a2f3318ee2";
		String oauthSignature = "otyFZImkvLSdNeyUHX1wm6e1BiM%3D";
		String oauthSignatureMethod = "HMAC-SHA1";
		String oauthTimestamp = String.valueOf(System.currentTimeMillis());
		String oauthToken;
		String oauthVersion = "1.0";
		
		if(isLocal)
		{
			oauthCallback = "http://www.thesimplex.co/login/twitter";
			oauthConsumerKey = "67JhIrZGdWqIFbTv2r2MUg";
			oauthToken = "583763833-mJ7Xesng31fpqUwcWXY9fc09SRRa6etzpHoyGHOD";
		}
		else
		{
			oauthCallback = "http://127.0.0.1:8888/login/twitter";
			oauthConsumerKey = "tatX4i5XbE7A8LoNpVsg";
			oauthToken = "583763833-bGR4gTQgnWpnpaOwVgxFY8h036oaIBTpui0nr9vm";
		}
		
		String urlParameters = "oauth_callback=" + oauthCallback;
		String request = "https://api.twitter.com/oauth/request_token";
//		String authorizationHeader = "oauth_consumer_key=\"" + oauthConsumerKey + "\", oauth_nonce=\"" + oauthNonce + "\", oauth_signature=\"" + oauthSignature + "\", oauth_signature_method=\"" + oauthSignatureMethod + "\", oauth_timestamp=\"" + oauthTimestamp + "\", oauth_token=\"" + oauthToken + "\", oauth_version=\"" + oauthVersion + "\"";
		String authorizationHeader = "OAuth oauth_consumer_key=\"" + oauthConsumerKey + "\", oauth_nonce=\"" + oauthNonce + "\", oauth_signature=\"" + oauthSignature + "\", oauth_signature_method=\"" + oauthSignatureMethod + "\", oauth_timestamp=\"" + oauthTimestamp + "\", oauth_version=\"" + oauthVersion + "\"";
		System.out.println(authorizationHeader);
		

		try
		{
		HttpURLConnection connection = CreateHttpURLConnection(urlParameters, request, authorizationHeader);
		return ParseConnection(connection, urlParameters);
		}
		catch(Exception e)
		{
			System.out.println("Twitter Error");
			return null;
		}
	}

	public static TwitterUser getTwitterUser(String redirect)
	{
		try
		{
			URL url = new URL("https://api.twitter.com/oauth/request_token?oauth_callback=" + redirect);
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = reader.readLine();
			String total = "";
	
			while (line != null)
			{
				total += line + '\n';
				line = reader.readLine();
				System.out.println(line);
			}
	
			return new Gson().fromJson(total, TwitterUser.class);
		}
		catch(IOException e)
		{
			System.out.println("Error retreiving TwitterUser.");
			return null;
		}
	}
	
	public static HttpURLConnection CreateHttpURLConnection(String urlParameters, String request, String authorizationHeader) throws MalformedURLException, IOException
	{
		HttpURLConnection connection = (HttpURLConnection) new URL(request).openConnection();
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Authorization Header", authorizationHeader);
		connection.setRequestProperty("charset", "utf-8");
		connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
		connection.setUseCaches(false);
		
		return connection;
	}
	
	public static String ParseConnection(HttpURLConnection connection, String urlParameters) throws IOException, JSONException
	{
		if (connection != null)
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();

			String total = "";
			String line = reader.readLine();

			while (line != null)
			{
				total += line + '\n';
				line = reader.readLine();
				System.out.println(line);
			}

			wr.close();
			connection.disconnect();
			//total += new JSONObject(total).get("access_token").toString();
			return "as";
		}
		else
		{
			return null;
		}
	}
}
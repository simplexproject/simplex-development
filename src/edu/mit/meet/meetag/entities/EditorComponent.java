/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import com.googlecode.objectify.annotation.Subclass;

import edu.mit.meet.meetag.enumurations.Language;

/**
 * @author Ahmed Shawish ahmed@thesimplex.co
 * @version 0.1
 * @since 2012-08-12
 */

@Subclass
public class EditorComponent extends Component {

	/**
	 * 
	 */
	// Fields
	private int idTwo;
	private String title;
	private String description;
	private Language language;

	public EditorComponent(int id, boolean required, String title,
			String description, Language language) {
		super(id, required, null);
		setView("/WEB-INF/views/editorComponent.jsp");
		this.idTwo = id;
		super.getParams().put("id", id);
		this.title = title;
		super.getParams().put("title", title);
		this.description = description;
		super.getParams().put("description", description);
		this.language = language;
		super.getParams().put("language", language);

	}

	/**
	 * @param title
	 * @param question
	 * @param type
	 * @param answer
	 * @param correctAnswer
	 */

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
		super.getParams().put("title", title);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return idTwo;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
		super.getParams().put("description", description);
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
		super.getParams().put("language", language);
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.idTwo = id;
		super.getParams().put("id", idTwo);
	}

	// @Override
	// public View rednerComponent() {
	// return null;
	// }
}
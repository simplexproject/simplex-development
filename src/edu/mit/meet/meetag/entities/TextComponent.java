/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

@SuppressWarnings("serial")
public class TextComponent extends Component {

	private String title;
	private String text;

	public TextComponent(int id, String title, String text) {
		super(id, false, "/WEB-INF/views/textComponent.jsp");
		setTitle(title);
		setText(text);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		getParams().put("title", title);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		getParams().put("text", text);
	}

}
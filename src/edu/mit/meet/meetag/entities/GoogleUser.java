/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.google.gson.Gson;

public class GoogleUser {

	private String id;
	private String email;
	private boolean verified_email;
	private String name;
	private String given_name;
	private String family_name;
	private String link;
	private String picture;
	private String gender;
	private String locale;

	public GoogleUser() {}

	public GoogleUser(String id, String email, boolean verified_email,
			String name, String given_name, String family_name, String link,
			String picture, String gender, String locale) {
		super();
		this.id = id;
		this.email = email;
		this.verified_email = verified_email;
		this.name = name;
		this.given_name = given_name;
		this.family_name = family_name;
		this.link = link;
		this.picture = picture;
		this.gender = gender;
		this.locale = locale;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the verified_email
	 */
	public boolean isVerified_email() {
		return verified_email;
	}

	/**
	 * @param verified_email
	 *            the verified_email to set
	 */
	public void setVerified_email(boolean verified_email) {
		this.verified_email = verified_email;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the given_name
	 */
	public String getGiven_name() {
		return given_name;
	}

	/**
	 * @param given_name
	 *            the given_name to set
	 */
	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	/**
	 * @return the family_name
	 */
	public String getFamily_name() {
		return family_name;
	}

	/**
	 * @param family_name
	 *            the family_name to set
	 */
	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the picture
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * @param picture
	 *            the picture to set
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 *            the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	public static String getAccessToken(String code) {
		System.out.println("Google Code: " + code);

		String urlParameters = "code="
				+ code
				+ "&client_id=937715082662-h6eaeq2apf8eblhu6f7b01hpcb51diui.apps.googleusercontent.com&client_secret=z6YRIkRXIQPupl3FTFH80i3e&redirect_uri=http://localhost:8888/login/google&grant_type=authorization_code";
		String request = "https://accounts.google.com/o/oauth2/token";

		try {
			URL url = new URL(request);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length",
					"" + Integer.toString(urlParameters.getBytes().length));
			connection.setUseCaches(false);

			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			String total = "";

			while ((line = reader.readLine()) != null) {
				total += line + '\n';
			}

			wr.close();
			connection.disconnect();

			JSONObject jo = new JSONObject(total);
			String access_token = (String) jo.get("access_token");

			return access_token;

		} catch (Exception e) {
			System.out.println("Google Error");
		}
		return null;

	}

	public static GoogleUser getGoogleUser(String accessToken) {
		System.out.println(accessToken);
		try {
			URL url = new URL(
					"https://www.googleapis.com/oauth2/v1/userinfo?access_token="
							+ accessToken);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String line;
			String total = "";

			while ((line = reader.readLine()) != null) {
				total += line + '\n';
			}

			System.out.println(total);
			Gson gson = new Gson();
			GoogleUser user = gson.fromJson(total, GoogleUser.class);
			return user;
		} catch (Exception e) {
			System.out.println("Google Error");
		}

		return null;
	}
}

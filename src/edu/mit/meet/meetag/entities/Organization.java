/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.util.ArrayList;

import javax.persistence.Id;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Serialized;

@Entity(name = "Organization")
public class Organization {

	@Id
	private Long id;
	@Serialized
	private ArrayList<String> managers;
	// TODO: Shouldn't there be courses here?
	private String name;
	private boolean verified;
	@Serialized
	private ArrayList<String> users;

	public Organization()
	{}

	public Organization(ArrayList<String> manager, String name) {
		super();
		this.managers = manager;
		this.name = name;
	}

	/**
	 * @return	the manager
	 */
	public ArrayList<String> getManager() {
		return managers;
	}

	/**
	 * @param	manager		-	the manager to set
	 */
	public void setManager(ArrayList<String> manager) {
		this.managers = manager;
	}

	/**
	 * @return	the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param	name	-	the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return	the users
	 */
	public ArrayList<String> getUsers() {
		return users;
	}

	public boolean isVerified() {
		return verified;
	}

	/**
	 * @param	users	-	the users to set
	 */
	public void setUsers(ArrayList<String> users) {
		this.users = users;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @brief	Removes a manager from the managers' list
	 * @param	manager		-	the manager to remove
	 */
	public void removeManager(String manager) {
		if(managers.size() > 1)
		{
			managers.remove(manager);
		}
	}

	/**
	 * @brief	Adds a manager to the managers list
	 * @param	manager		-	the manager to add
	 */
	public void addManager(String manager) {
		if (managers == null) {
			managers = new ArrayList<String>();
		}
		if (!managers.contains(manager)) {
			managers.add(manager);
		}
	}

	/**
	 * @brief	Removes a user from the users' list
	 * 
	 * @param	user	-	the user to remove
	 */
	public void removeUser(String user) {
		users.remove(user);
	}

	/**
	 * @brief	Adds a user to the users' list
	 * @param	user	-	the user to add
	 */
	public void addUser(String user) {
		if (users == null) {
			users = new ArrayList<String>();
		}
		if (!users.contains(user)) {
			users.add(user);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Organization other = (Organization) obj;

		return (id == other.id);
	}

}
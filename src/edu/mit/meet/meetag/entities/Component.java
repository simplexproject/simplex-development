/*
 * 
 * SIMPLEx CONFIDENTIAL
 * __________________
 * 
 *  [2012] - [2013] SIMPLEx 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of SIMPLEx and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to SIMPLEx
 * and is protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from SIMPLEx.
 */

package edu.mit.meet.meetag.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.objectify.annotation.Serialized;

/**
 * @author George Khoury george@thesimplex.co
 * @version 0.1
 * @since 2012-07-25
 */

public class Component implements Serializable {

	/**
	 * 
	 */
	// Fields
	private int id;
	private boolean completed;
	private boolean required;
	private String view;
	@Serialized
	private Map<String, Object> params;

	// Empty constructor for Objectify
	public Component() {
	}

	// Default constructor for fields
	public Component(int id, boolean required, String view) {
		super();
		this.view = view;
		this.id = id;
		this.required = required;
		this.params = new HashMap<String, Object>();
		this.completed = false;
	}

	public Object getParam(String key) {
		return params.get(key);
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the completed
	 */
	public boolean isCompleted() {
		return completed;
	}

	/**
	 * @param completed
	 *            the completed to set
	 */
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * @param required
	 *            the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

}
